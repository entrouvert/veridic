'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


from sets import Set
import re


def is_constant(X):
    if X == 'True' or X == 'False':
        return True
    return False


def is_variable(X):
    if re.match("^\d+$", X):
        return True
    return False


def is_atom(X):
    return is_constant(X) or is_variable(X)


def is_proposition(X):
    if is_atom(X):
        return True
    if X[0] == '(':
        n = 1
        for i in range(1, len(X)):
            if X[i] == '(':
                n = n + 1
            if X[i] == ')':
                n = n - 1
                if n == 0:
                    return is_proposition("True" + X[i+1:])
        return False
    if X[0] == '-' and is_proposition(X[1:]):
        return True
    for i in range(0, len(X)):
        if X[i] == '&' and is_proposition(X[0:i]) and is_proposition(X[i+1:]):
            return True
    for i in range(0, len(X)):
        if X[i] == '|' and is_proposition(X[0:i]) and is_proposition(X[i+1:]):
            return True


def evaluation_constant(X):
    if X == 'True':
        return True
    return False


def evaluation_variable(X, Env):
    return Env[X]


def evaluation_atom(X, Env):
    if is_constant(X):
        return evaluation_constant(X)
    else:
        return evaluation_variable(X, Env)


def evaluation(X, Env):
    '''
        Provide something like:
        val = {'1': True, '2': True, '3': False, '4': True, '5': False}
        rule = "(1&2&3)|(4&5)"
        res = evaluation(rule, val)
    '''
    if is_atom(X):
        return evaluation_atom(X, Env)
    if X[0] == '(':
        n = 1
        for i in range(1, len(X)):
            if X[i]=='(':
                n = n + 1
            if X[i] == ')':
                n = n - 1
                if n == 0:
                    return evaluation(\
                        str(evaluation(X[1:i], Env)) + X[i+1:],
                        Env)
        return False

    if X[0] == '(' and X[len(X)-1] == ')':
        return evaluation(X[1:len(X)-1], Env)

    if X[0] == '-' and is_proposition(X[1:]):
        if not evaluation(X[1:], Env):
            return True
        if evaluation(X[1:], Env):
            return False

    for i in range(0, len(X)):
        if X[i] == '&' and is_proposition(X[0:i]) and is_proposition(X[i+1:]):
            if evaluation(X[0:i], Env) and evaluation(X[i+1:], Env):
                return True
            return False

    for i in range(0, len(X)):
        if X[i] == '|' and is_proposition(X[0:i]) and is_proposition(X[i+1:]):
            if evaluation(X[0:i], Env) or evaluation(X[i+1:], Env):
                return True
            return False


def extract_variables(F):
    if is_constant(F):
        return Set([])

    if is_variable(F):
        return Set([F])

    if F[0] == '(' and F[len(F)-1] == ')' and is_proposition(F[1:len(F)-1]):
        return extract_variables(F[1:len(F)-1])

    if F[0] == '-' and is_proposition(F[1:]):
        return extract_variables(F[1:])

    for i in range(0, len(F)):
        if F[i] == '&' and is_proposition(F[0:i]) and is_proposition(F[i+1:]):
            return extract_variables(F[0:i]) | extract_variables(F[i+1:])

    for i in range(0, len(F)):
        if F[i] == '|' and is_proposition(F[0:i]) and is_proposition(F[i+1:]):
            return extract_variables(F[0:i]) | extract_variables(F[i+1:])


def build_combinations(n, vals):
    c = [[x] for x in vals]
    for i in range(0, n-1):
        c = [y + [x] for y in c for x in vals]
    return c


def make_valuation(variables):
    result = []
    v_list = list(variables)
    combinaisons = build_combinations(len(variables), [False, True])
    for c in combinaisons:
        line = dict()
        for i in range(0, len(variables)):
            line[v_list[i]] = c[i]
        result.append(line)
    return result


def make_truth_table(rule):
    if not rule:
        return []
    variables = extract_variables(rule)
    if not variables:
        return []
    '''
        Valuation
    '''
    valuation = make_valuation(variables)
    if not valuation:
        return []
    return [[v, evaluation(rule, v)] for v in valuation]


def return_sorted_variables_to_truth(rule, valuation):
    if not rule or not valuation:
        return {}
    variables = extract_variables(rule)
    if not variables:
        return {}
    val = make_valuation(variables)
    if not val:
        return {}
    if evaluation(rule, valuation):
        return {}
    evas = {}
    for v in val:
        if evaluation(rule, v):
            l = []
            for k in valuation.keys():
                if k in v and v[k] != valuation[k]:
                    l.append((k, valuation[k], v[k]))
            if len(l) in evas:
                evas[len(l)].append(l)
            else:
                evas[len(l)] = [l]
    '''
        remove redundant possibilities
        i.e. remove bigger rules superset of smaller ones
    '''
    for i in evas.keys():
        for d1 in evas[i]:
            for j in evas.keys():
                if j > i:
                    '''
                        Use a temp list is mandatory, else each time
                        an element is removed, an element is skipped in the
                        loop
                    '''
                    tmp = []
                    for d2 in evas[j]:
                        if not is_l1_in_l2(d1, d2):
                            tmp.append(d2)
                        evas[j] = tmp
    return evas


def is_l1_in_l2(l1, l2):
    for i in l1:
        if not i in l2:
            return False
    return True
