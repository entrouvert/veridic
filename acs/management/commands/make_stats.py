'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import json
import datetime

from pprint import pprint

from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from acs import settings
from acs.models import *
from acs.abac.models import *
from acs.attribute_aggregator.models import *

try:
    import ldap
except ImportError:
    ldap = None

class Command(BaseCommand):

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    option_list = BaseCommand.option_list + (
        make_option('--view-and-log',
            action='store_true',
            dest='view-and-log',
            default=False,
            help='Make a new stats entry. Give a comment in argument if necessary.'),
        ) + (
        make_option('--view-now',
            action='store_true',
            dest='view-now',
            default=False,
            help='View current object counts.'),
        ) + (
        make_option('--view-last-changes',
            action='store_true',
            dest='view-last-changes',
            default=False,
            help='Compare current object counts and last count.'),
        ) + (
        make_option('--list-logs',
            action='store_true',
            dest='list-logs',
            default=False,
            help='Compare current object counts and last count.'),
        ) + (
        make_option('--list-logs-verbose',
            action='store_true',
            dest='list-logs-verbose',
            default=False,
            help='Compare current object counts and last count.'),
        )
    args = '<comment>'
    help = \
        'No options configured yet.'

    def handle(self, *args, **options):

        print '-- Bits of database statistics --'

        dic = {}

        if options['view-and-log'] or options['view-now']:

            n_User = len(User.objects.all())

            n_UserAlias = len(UserAlias.objects.all())
            n_Role = len(Role.objects.all())
            n_AcsObject = len(AcsObject.objects.all())
            n_View = len(View.objects.all())
            n_Action = len(Action.objects.all())
            n_Activity = len(Activity.objects.all())
            n_AcsPermission = len(AcsPermission.objects.all())
            n_AcsAbacPermission = len(AcsAbacPermission.objects.all())
            n_Namespace = len(Namespace.objects.all())
            n_Policy = len(Policy.objects.all())

            n_AttributeSource = len(AttributeSource.objects.all())
            if ldap:
                n_LdapSource = len(LdapSource.objects.all())
            n_UserAttributeProfile = len(UserAttributeProfile.objects.all())
            n_AbacRule = len(AbacRule.objects.all())

            print 'Current statistics:'
            for name, value in sorted(locals().items()):
                if name.startswith('n_'):
                    n = name.split('n_')[1]
                    print '%s\t%s' % (value, n)
                    dic[n] = value

        if options['view-and-log']:

            json_data = None
            try:
                json_data = open('acs/management/commands/db_stats.json', 'r')
            except:
                pass

            data = []
            try:
                data = json.load(json_data)
            except:
                pass

            last_dic = {}
            if data:
                last_dic = data[len(data)-1]['stats']
                print 'Last record:'
                for key, value in last_dic.items():
                    print '%s\t%s' % (value, key)

#            data = [{'id', xxx, 'date': xxx, 'comment': xxx, 'stats': {'ObjectName': value, }}]

            data.append({'id': len(data),
                'date': str(datetime.datetime.now()), 'stats': dic})

            if json_data:
                json_data.close()
            try:
                json_data = open('acs/management/commands/db_stats.json', 'w')
            except:
                raise CommandError('Unable to create or open the log file.')

            json_data.write(json.dumps(data))
            json_data.close()

            if last_dic:
                print 'Counts changed:'
                for key, value in dic.items():
                    if value - last_dic[key] > 0:
                        print '%s %s added' % (value - last_dic[key], key)

        if options['view-last-changes']:

            json_data = None
            try:
                json_data = open('acs/management/commands/db_stats.json', 'r')
            except:
                raise CommandError('No log file.')

            data = []
            try:
                data = json.load(json_data)
            except:
                raise CommandError('The log file is empty.')

            if data and len(data) > 1:
                last_dic = data[len(data)-1]['stats']
                print 'Last record:'
                for key, value in last_dic.items():
                    print '%s\t%s' % (value, key)
                blast_dic = data[len(data)-2]['stats']
                print 'Before last record:'
                for key, value in last_dic.items():
                    print '%s\t%s' % (value, key)

                if last_dic and blast_dic:
                    print 'Counts changed:'
                    for key, value in last_dic.items():
                        if value - blast_dic[key] > 0:
                            print '%s %s added' % (value - blast_dic[key], key)
                else:
                    raise CommandError('The log file does not contain enough material.')

            else:
                raise CommandError('The log file does not contain enough material.')

        if options['list-logs']:

            json_data = None
            try:
                json_data = open('acs/management/commands/db_stats.json', 'r')
            except:
                raise CommandError('No log file.')

            data = []
            try:
                data = json.load(json_data)
            except:
                raise CommandError('The log file is empty.')

            for it in data:
                print 'ID:\t%s\t%s' % (it['id'], it['date'])

        if options['list-logs-verbose']:

            json_data = None
            try:
                json_data = open('acs/management/commands/db_stats.json', 'r')
            except:
                raise CommandError('No log file.')

            data = []
            try:
                data = json.load(json_data)
            except:
                raise CommandError('The log file is empty.')

            for it in data:
                print '\nID:\t%s\t%s' % (it['id'], it['date'])
                for key, value in it['stats'].items():
                    print '%s\t%s' % (value, key)

        print '------------ END ------------'
