#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikaël Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from utils import get_soap_message, SOAPException, soap_call
from django.http import HttpResponseBadRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
import sys

'''
How to serve XACML with Lasso
Abstract:
<XACMLAuthzDecisionQuery> contenue dans
des <samlp:RequestAbstractType> en utilisant Lasso pour la partie SAML,
et aussi les reponses <XACMLAuthzDecisionStatement> ->
<saml:AttributeStatement> -> <XACMLAuthzDecisionStatement> ->
<samlp:Response>.

SAML 2.0 profile of XACML v2.0
OASIS Standard, 1 February 2005
Document identifier:
access_control-xacml-2.0-saml-profile-spec-os
Location:
http://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-saml-profile-spec-os.pdf

3.
<xacml-samlp:XACMLAuthzDecisionQuery> is a SAML Query that extends the SAML
Protocol Schema. It allows a PEP to submit an XACML Request Context in a SAML Request,
along with other information.

<xacml-saml:XACMLAuthzDecisionStatement> is a SAML Statement that extends the
SAML Assertion schema. It allows an XACML PDP to return an XACML Response Context in
the Response to an <XACMLAuthzDecisionStatement>, along with other information. It
also allows an XACML Response Context to be stored or transmitted in the form of a SAML
Assertion.

3.1
<xs:element name="XACMLAuthzDecisionQuery"
type="XACMLAuthzDecisionQueryType"/>
<xs:complexType name="XACMLAuthzDecisionQueryType">
<xs:complexContent>
<xs:extension base="samlp:RequestAbstractType">
<xs:sequence>
<xs:element ref="xacml-context:Request"/>
</xs:sequence>
<xs:attribute name="InputContextOnly"
type="boolean"
use="optional"
default="false"/>
<xs:attribute name="ReturnContext"
type="boolean"
use="optional"
</xs:extension>
</xs:complexContent>
</xs:complexType>
default="false"/>

3.2
<xs:element name="XACMLAuthzDecisionStatement"
type="xacml-saml:XACMLAuthzDecisionStatementType"/>
<xs:complexType name="XACMLAuthzDecisionStatementType">
<xs:complexContent>
<xs:extension base="saml:StatementAbstractType">
<xs:sequence>
<xs:element ref="xacml-context:Response"/>
<xs:element ref="xacml-context:Request"
MinOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>

5. Put decision in SAML2 a8n

An <XACMLAuthzDecisionStatement>, <XACMLPolicyStatement>, or SAML standard
<saml:AttributeStatement> SHALL be encapsulated in a <saml:Assertion>, which MAY
be signed.

Most components of a <saml:Assertion> are fully specified in the SAML 2.0 specification
[SAML]. The following elements and XML attributes are further specified here for use with the
SAML statement types defined and used in this Profile.

Except as specified here, this Profile imposes no requirements or restrictions on information in the
<saml:Assertion> element.

Use the SOAP binding

6.
An <XACMLAuthzDecisionQuery> or <XACMLPolicyQuery> SHALL be encapsulated in a
<samlp:RequestAbstractType> element, which MAY be signed.

Most components of a <samlp:RequestAbstractType> are fully specified in the SAML 2.0
specification [SAML]. The following elements and XML attributes are further specified here for use
with the SAML query types defined and used in this Profile. Except as specified here, this Profile
imposes no requirements or restrictions on information in the <samlp:RequestAbstractType>
element.

7.
An <XACMLAuthzDecisionStatement> or <XACMLPolicyStatement>
encapsulated in a <samlp:Response> element, which MAY be signed.

Most components of a <samlp:Response> are fully specified in the SAML 2.0 specification
[SAML]. The following elements and XML attributes are further specified here for use with the
SAML statement types defined and used in this Profile. Except as specified here, this Profile
imposes no requirements or restrictions on information in the <samlp:Response> element.
SHALL
be

'''

@csrf_exempt
def incoming(request):
    '''Play the role of an XACML2 PDP'''
    try:
        soap_message = get_soap_message(request)
    except:
        return HttpResponseBadRequest(_('Bad SOAP message'))

    print >> sys.stderr, 'soap_message ' + str(request.raw_post_data)

    '''
    1. Treat SOAP/SAML Request with Lasso
    2. Obtain XACML Request and transform it in a local request
    3. Build XACML Response
    4. Use Lasso to build SAML/SOAP Response
    '''

    django_response = HttpResponse()
    django_response.status_code = 200
    django_response.content_type = 'text/xml'
    django_response.content = request.raw_post_data
    return django_response


def outcoming():
    '''Play the decision requester: XACML2 PEP
    and Context Handler (converter from native to XAML request).
    Use console: export DJANGO_SETTINGS_MODULE=acs.settings && python
    >from acs.net_interface import *
    >outcoming()
    '''

    '''
    1. Build XACML Request
    2. Build SOAP/SAML Request with Lasso
    3. Make a soap call
    4. Treat SOAP/SAML Response with Lasso
    5. Analyse XACML Response
    '''

    # TODO: Client cert
    client_cert = None
    soap_answer = None
    try:
        soap_answer = soap_call("http://127.0.0.1:8000/incoming", "<soap/>", client_cert = client_cert)
    except SOAPException:
        print >> sys.stderr, 'Error: soap_answer ' + str(soap_answer)
    else:
        print >> sys.stderr, 'Success: soap_answer ' + str(soap_answer)
