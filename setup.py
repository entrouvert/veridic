#! /usr/bin/env python
#
'''
   Setup script for Veridic
'''
import acs

from ez_setup import use_setuptools
use_setuptools()

from setuptools import setup, find_packages

setup(name="veridic",
      version=acs.VERSION,
      license="AGPLv3+",
      description="Veridic - Towards a centralized access control system",
      url="http://dev.entrouvert.org/projects/veridic/",
      author="Entr'ouvert",
      author_email="mates@entrouvert.org",
      maintainer="Mikael Ates",
      maintainer_email="mates@entrouvert.org",
      packages=find_packages(),
      include_package_data=True,
      install_requires=['django >= 1.3',
        'django-debug-toolbar >= 0.9.0'],
      zip_safe=False,
      classifiers=[
          "Development Status :: 4 - Beta",
          "Environment :: Web Environment",
          "Framework :: Django",
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'Intended Audience :: Information Technology',
          'Intended Audience :: Legal Industry',
          'Intended Audience :: Science/Research',
          'Intended Audience :: Telecommunications Industry',
          "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
          "Operating System :: OS Independent",
          "Programming Language :: Python",
          "Topic :: System :: Systems Administration :: Authentication/Directory",
          "Topic :: Security",
      ],
)
