'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import time

from django.db import reset_queries
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from acs.core import is_authorized_on_alias_or_role, \
    is_authorized_on_object_or_view, \
    is_authorized_on_action_or_activity, \
    isAuthorizedRBAC2

from acs.models import Action, UserAlias, Policy, AcsObject


class Command(BaseCommand):
    '''
        Initialize Acs with a root user with all rights.
        Rerun with: python manage.py sqlclear acs | python manage.py dbshell \
        && python manage.py syncdb \
        && python manage.py initialize-acs <username> [--existing]

        Names hardly encoded:
            Namespace:Default,
            Action:administration,
            Role:root_admin_role,
            View: root_admin_view
    '''

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    help = \
    'Test performance running script without parameters and after load-db.'

    def handle(self, *args, **options):
        size = 10

        print '--> Test permissions'
        nb = size * 10
        i = 0
        m = 0
        n = 0
        requester = \
            User.objects.get(username='user_' + str((size/10)-1+i))
        policy = Policy.objects.get(name='policy_' + str(i))
        print 'Policy: %s' % policy
        sys.stdout.flush()
        k = 0
        total = 0
        maximum = 0

        while k < nb:

            user_name = 'user_' + str(k)
            who = UserAlias.objects.get(alias=user_name,
                namespace=policy.namespace)
            if is_authorized_on_alias_or_role(requester, who, policy) < 0:
                raise CommandError('Requester not authorized on %s' \
                    % who)

            print 'User: %s' % who

            l = 0
            d = 0
            maxi = 0
            while l < nb:

                object_name = 'object_' + policy.name + '_' + str(l)
                what = AcsObject.objects.get(name=object_name,
                    namespace=policy.namespace)
                action_name = 'action_' + policy.name + '_' + str(l)
                how = Action.objects.get(name=action_name,
                    namespace=policy.namespace)

                if is_authorized_on_object_or_view(requester, what,
                        policy) < 0:
                    raise CommandError('Requester not authorized on %s' \
                        % what)
                if is_authorized_on_action_or_activity(requester, how,
                        policy) < 0:
                    raise CommandError('Requester not authorized on %s' \
                        % how)

                t0 = time.time()
                p = isAuthorizedRBAC2(who, what, how)
                t1 = time.time()
                delta = t1 - t0
                maxi = max(maxi, delta)
                d = d + delta
                authz = False
                if p:
                    authz = True
                    sys.stdout.write('+')
                else:
                    sys.stdout.write('-')

                if k <= l:
                    if authz:
                        sys.stdout.write('+')
                        m = m + 1
                    else:
                        sys.stdout.write('-')
                        n = n + 1
                else:
                    if not authz:
                        sys.stdout.write('+')
                        m = m + 1
                    else:
                        sys.stdout.write('-')
                        n = n + 1

                sys.stdout.flush()

                l = l + 1

            print '\nNumber of requests: %s' % str(l)
            print 'Average request time: %s' % str(d/l)
            print 'Requests per seconds: %s' % str(1/(d/l))
            print 'Slower request: %s' % str(maxi)
            '''Test 1/20 user'''
            k = k + 20
            total = total + d
            maximum = max(maxi, maximum)

            '''
                Due to memory leakage, else Debug=False
                https://docs.djangoproject.com/en/dev/faq/models/
                #why-is-django-leaking-memory
            '''
            reset_queries()

        req = m + n
        print '\nTotal number of requests: %s' % str(req)
        print 'Average request time: %s' % str(total/req)
        print 'Requests per seconds: %s' % str(1/(total/req))
        print 'Slower request: %s' % str(maximum)
        print 'Successful permissions tested: %s' % str(m)
        print 'Error testing permissions: %s' % str(n)
        print '<--\n'


        print '-------- DONE --------'
