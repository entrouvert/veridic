=====================================================
Veridic - Towards a centralized access control system
=====================================================

Veridic is a project containing for now a single application called A.C.S. or
ACS for Access Control System.

ACS is an application providing an access control decision point.
An access control policy configuration is done through a Web interface.
For now, request decisions can only be requested through the Web interface.
Until request can be made by PEP though another interface, ACS can onlt be
used to design policies and verify their conformance with the RBAC model.

This application aims at being a policy administration point (PAP) and a
policy decision point (PDP).


Features
--------

Policy administration:
- Define policies based on Identity-Based Access Control (IBAC), Role-BAC
  (RBAC) and Attributes-BAC (ABAC) permissions

ACS administration:
- The access control system possesses its access control system that governs
ACS and all the policies

Web interface:
- Web interface to define policies
- Web interface to administrate the system

Other:
- Define objects with regexp
- Permission delegations

The ROADMAP is coming soon.


Dependencies
------------

(Optional) For graph display of policies: networkx and matplotlib.


Quick Start
-----------

** Before running the ACS application, you must execute the initialization
script **

    Initialization script:
        - Initialize ACS.
        - Create a root user with all rights.
        - Inject the attribute definitions in the database.

    Run with:
        python manage.py initialize-acs <username> [--existing] \
        [--without-abac] [--reload-abac]

        --existing
            The user already exists in the database.
            Else, the user is created.

        --reload-abac
            Skip the initial configuration and update the
            attribute definitions.

            *** Be sure to use --reload-abac option if it is not the first
            time the script is run and it is not expected to reinitialize
            ACS ***

        --without-abac
            Only the initial configuration.

    Reinitialization:
        python manage.py sqlclear acs | python manage.py dbshell \
        && python manage.py syncdb \
        && python manage.py initialize-acs <username> [--existing] \
        [--without-abac] [--reload-abac]

    If you want to clear all the attribute definitions use
        python manage.py sqlclear abac

    WARNING: The reinitialization or rerunning the initialization script
    assume that you want to reset ACS and you will loose all policies.

To run the application with the django embedded server, launch the following
command:

    python manage.py runserver

You should see the following output:

  Validating models...
  0 errors found

  Django version 1.2, using settings 'authentic.settings'
  Development server is running at http://127.0.0.1:8000/
  Quit the server with CONTROL-C.

  You can access the running application on http://127.0.0.1:8000/


Overall explanations
--------------------

In ACS, a permission define an access control rule with the form
permission(who, what, how), with 'who' a user or a role, 'what' an object or a
view and 'how' an action or an activity.

Roles contain users, views contain objects, activities contain actions.
Users, roles, objects, views, actions, activities are entities.

A policy is a set of entities and permissions.

ACS is an application to design access control policies. Then, ACS can be
requested to deliver access control decisions on the access control policies
defined.

In ACS you can create as many policies as you want. Each policy define a
namespace where entities are unique. E.g. two actions with the same name can
be cerated in two different policies and are considered as completly different
entities.

One important point is that permissions are not set on the User objects but on
aliases of users. Then, a single Django user must have an alias declared in
each policy it is expected to grant it permissions. It is thus possible to
declare an alias in a policy without having a corresponding existing Django
user.

Helper functions on the Web interface help in declaring all the known django
users in a policy. These functions can be used each time new django users are
added.

Managing a user and aliases is an easy way to have a global representation of
a user in the whole policies. It is fast to see for a same user which are its
different identifiers in the different policies.

ACS is an application where an access control applies. It means that the
application contains a policy to define the access control permissions to
adminstrate the ACS application. This access control policy is named "Default"
with a namespace "Default". Every entity in that namespace is thus an entity
employed in the access control on adminstration of ACS. The management of this
policy is done through the web interface functions, in other words, the
default policy is not directly managed adding actions, users or objects.

In other words, the administration of the access control system is an
application by itself, the application, for the purpose of administration, is
also a PAP of the access control policy on the access control system, a PDP on
the access to modify the access control policy and a PEP to enforce these
decisions when modifying the access control policy.

The functions used are the following:

- manage root administrators : a root administrator can manage the whole
system. - manage user administrators : a user administrators can add and
remove users, can declare users in all policies (give a user an alias in the
policy), can create-modify-remove roles in any policies, can declare policy
user administrators, can add any user or role in any role inside any policy,
can set a permission on any role or user in any policy.

- manage policy root administrators : a policyroot administrator can manage a
whole policy.
- manage policy user administrators : a policy user administrators can declare
any users in the policy, can create-modify-remove roles in this policy, can
add any user or role in any role inside the policy, can set a permission on
any role or user in the policy.

- manage object and view creators : a object and view creator can
create-modify-remove objects and views in the policy, can add any object or
view in any view inside the policy, can set a permission on any object or view
in the policy.

- manage action and activity creators : a action and activity creator can
create-modify-remove actions and activities in the policy, can add any action
and activity in any view inside the policy, can set a permission on any action
and activity in the policy.

In comparison with a user with the role policy root administrator, a user that
has the three roles (policy user administrator, object and view creator,
action and activity creator) can administrate the whole policy but cannot add
users to these roles or to the policy root administrator role.

Moreover, inside each policy, it is feasible to define fined grained
permissions to administrate a subpart of the policy using administration
roles, wiews and permissions. Any user with at least an administration
permission is considered as a policy administrator.


Access control model
--------------------

All that is not authorized is denied.

A permission(WHO,WHAT,HOW) is the right of WHO on WHAT to perform HOW.

The access control model is role-based. With an RBAC system, WHO may be a user
or a role:
is_authorized(WHO,WHAT,HOW) :- permission(WHO,WHAT,HOW).
is_authorized(WHO,WHAT,HOW) :- permission(X,WHAT,HOW), has_role(X,WHO).

The role hierarchy is used for permission heritance.

Consider r1 'is a' r2 and 'is a' r3 (r2 and r3 are juniors of r1)
assigned_permissions(r1) :- assigned_permissions(r2).
assigned_permissions(r1) :- assigned_permissions(r3).
(All permissions assigned to r2 and r3 are also assigned to r1)

assigned_users(r2) :- assigned_users(r1).
assigned_users(r2) :- assigned_users(r1).
(All users assigned to r1 are also assigned to r2 and r3)

The heritage tree of r1 is then:
assigned_permissions(r1) = direct_permissions(r1) U_i assigned_permissions(ri)
ri being all the junior roles of r1.

Then, if the user has the role r1, it has the rights given by all the
permissions direclty assigned to r1 and all the permissions assigned to roles
below in the hirarchy.

We do not handle interdictions. Mainly due to conflict with permission
heritance.

We do not handle obligation for now. It is just a question of semantic and
what information can be retreived from the policy. An obligation is a
permission when a usage control is assumed to verify that some permissions are
really used, potentially with somme constraints.

We do not handle sessions and role activation, as we do not handle separation
of duty (SoD) nor information flow control.

The access control model also relies on abstractions of objects (WHAT) and
actions (HOW).

The abstraction for object is the view concept. A view may contain objects and
other views. It means that a view may contain any entity of the model.

A permission on a view gives the right of WHO to do HOW on all the objects
contained in this view, or in any view below in the hirarchy of views.

The abstraction for action is activity. An activity may contain actions and
other activities.

A permission on an activity gives the right of WHO on WHAT to do all the
actions contained in this activity or any activity below in the hirarchy of
activities.

The abstract entities and the heritance make often talk about the range of a
permision.


Copyright
---------

VERIDIC is copyrighted by Entr'ouvert and is licensed through the GNU Affero
General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version. A copy of the
whole license text is available in the COPYING file.
