'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from core import get_from_pk, is_entity_who, is_entity_what, is_entity_how

'''
''''''
    View Helper functions
''''''
'''


def get_who_from_one_post_field(request, field):
    pk, type_entity = request.POST[field].split('_')
    who = get_from_pk(type_entity, pk)
    if not who:
        raise Exception('Unable to find who from %s' %field)
    if not is_entity_who(who):
        raise Exception('This entity is not a User or a Role %s' %who)
    return who


def get_what_admin_from_one_post_field(request, field):
    pk, type_entity = request.POST[field].split('_')
    what = get_from_pk(type_entity, pk)
    if not what:
        raise Exception('Unable to find what from %s' %field)
    return what


def get_what_from_one_post_field(request, field):
    pk, type_entity = request.POST[field].split('_')
    what = get_from_pk(type_entity, pk)
    if not what:
        raise Exception('Unable to find what from %s' %field)
    if not is_entity_what(what):
        raise Exception('This entity is not an AcsObject or a View %s' %what)
    return what


def get_how_from_one_post_field(request, field):
    pk, type_entity = request.POST[field].split('_')
    how = get_from_pk(type_entity, pk)
    if not how:
        raise Exception('Unable to find how from %s' %field)
    if not is_entity_how(how):
        raise Exception('This entity is not an action or an Activity %s' %how)
    return how


def get_policy_from_session(request):
    if 'policy' in request.session:
        return request.session['policy']
    return None
