import lasso
import urllib

__all__ = [ 'saml_authorization_query', 'SamlAuthorizationQueryProcessor',
    'DENY', 'PERMIT', 'INDETERMINATE', 'PERMISSIONS' ]

def saml_authorization_query_async(sp_metadata_file, private_key,
        pdp_metadata_file, name_id, action, resource, action_ns=None):
    '''
       Build a generator to request a remote policy decision point (PDP) about
       permission of a subject to do some action on a resource.

       sp_metadata_file - the SAML 2.0 metadata description of the current
       provider
       private_key - the PEM encoded private key of the current provider
       pdp_metadata_file - the SAML 2.0 metadata description of the policy
       decision point
       name_id - the NameID of the subject doing an action
       action - a string describing the action being made by the subject
       resource - the resource being affected by the subject
       action_ns - an eventual namespace for the action string

       The generator first return a pair, (soap_request_msg,
       soap_endpoint_url), you must POST this message to the given URL. The
       HTTP response body must be passed to the generator using the send()
       method. The send() method will return the final result of the request
       among the constants PERMIT, DENY or INDETERMINATE.
    '''

    server = lasso.Server.newFromBuffers(sp_metadata_file, private_key)
    server.addProviderFromBuffer(lasso.PROVIDER_ROLE_AUTHZ_AUTHORITY,
            metadata_file)
    assertion_query = lasso.AssertionQuery(server)
    assertion_query.initRequest(None, lasso.HTTP_METHOD_SOAP,
            lasso.ASSERTION_QUERY_REQUEST_TYPE_AUTHZ_DECISION)
    assertion_query.request.subject = lasso.Saml2Subject()
    assertion_query.request.subject.nameId = name_id
    assertion_query.request.action = lasso.Saml2Action.newWithString(action)
    assertion_query.request.action.namespace = action_ns
    assertion_query.request.resource = resource
    assertion_query.buildRequestMsg()
    soap_response = yield assertion_query.msgBody, assertion_query.msgUrl
    assertion_query.processResponseMsg(soap_response)
    assertion = assertion_query.response.assertion[0]
    authz_decision_statement = assertion.authzDecisionStatement[0]
    # XXX: assert authz_decision_statement match the request
    yield authz_decision_statement.decision

def saml_authorization_query_sync(sp_metadata_file, private_key,
        pdp_metadata_file, name_id, action, resource, action_ns=None):
    generator = saml_authorization_query_async(sp_metadata_file, private_key,
            pdp_metadata_file, name_id, action, resource, action_ns)
    soap_request_msg, soap_endpoint_url = next(generator)
    soap_response = urllib.urlopen(soap_endpoint_url, soap_request_msg).read()
    return generator.send(soap_response)

PERMIT = 'Permit'
DENY = 'Deny'
INDETERMINATE = 'Indeterminate'
PERMISSIONS = [ 'Permit', 'Deny', 'Indeterminate' ]

class SamlAuthorizationQueryProcessor:
    ''' Simple SAML 2.0 AuthorizationDecisionQuery processor.

        You must passe the received SOAP request to the process() method and it
        will return the SOAP response.

        To implement the decision process you must overload the decide() method.
        It must return one of the constant PERMIT, DENY or INDETERMINATE.

        By default it always return DENY.
    '''
    def __init__(self, pdp_metadata_file, private_key, sp_metadata_file):
        server = lasso.Server.newFromBuffers(pdp_metadata_file, private_key)
        server.addProviderFromBuffer(lasso.PROVIDER_ROLE_SP, sp_metadata_file)
        self.server = server

    def process(self, soap_message, force_decision=None):
        assertion_query = lasso.AssertionQuery(self.server)
        assertion_query.processRequestMsg(soap_message)
        assertion_query.validateRequest()
        name_id = assertion_query.request.subject.nameId
        action = assertion_query.request.action.content
        action_ns = assertion_query.request.action.namespace or \
                lasso.SAML2_ACTION_NAMESPACE_RWEDC_NEGATION
        resource = assertion_query.request.resource
        if force_decision:
            decision = force_decision
        else:
            decision = self.decide(name_id, action, action_ns, resource)
        assert decision in PERMISSIONS
        authz_decision_statement = lasso.Saml2AuthzDecisionStatement()
        authz_decision_statement.action = assertion_query.request.action
        authz_decision_statement.resource = resource
        authz_decision_statement.decision = decision
        assertion = lasso.Saml2Assertion()
        assertion.subject = assertion_query.request.subject
        assertion.authzDecisionStatement = [ authz_decision_statement ]
        assertion_query.response.assertion = [ assertion ]
        assertion_query.buildResponseMsg()
        return assertion_query.msgBody

    def decide(self, name_id, action, action_ns, resource):
        return DENY

if __name__ == '__main__':
    sp_metadata = '''<?xml version="1.0"?>
<EntityDescriptor xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
    entityID="http://sp6/metadata">
<SPSSODescriptor
    AuthnRequestsSigned="true"
    protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">

<KeyDescriptor use="signing">
    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:X509Data>
        <ds:X509Certificate>-----BEGIN CERTIFICATE-----
MIIDnjCCAoagAwIBAgIBATANBgkqhkiG9w0BAQUFADBUMQswCQYDVQQGEwJGUjEP
MA0GA1UECBMGRnJhbmNlMQ4wDAYDVQQHEwVQYXJpczETMBEGA1UEChMKRW50cm91
dmVydDEPMA0GA1UEAxMGRGFtaWVuMB4XDTA2MTAyNzA5MDc1NFoXDTExMTAyNjA5
MDc1NFowVDELMAkGA1UEBhMCRlIxDzANBgNVBAgTBkZyYW5jZTEOMAwGA1UEBxMF
UGFyaXMxEzARBgNVBAoTCkVudHJvdXZlcnQxDzANBgNVBAMTBkRhbWllbjCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM06Hx6VgHYR9wUf/tZVVTRkVWNq
h9x+PvHA2qH4OYMuqGs4Af6lU2YsZvnrmRdcFWv0+UkdAgXhReCWAZgtB1pd/W9m
6qDRldCCyysow6xPPKRz/pOTwRXm/fM0QGPeXzwzj34BXOIOuFu+n764vKn18d+u
uVAEzk1576pxTp4pQPzJfdNLrLeQ8vyCshoFU+MYJtp1UA+h2JoO0Y8oGvywbUxH
ioHN5PvnzObfAM4XaDQohmfxM9Uc7Wp4xKAc1nUq5hwBrHpjFMRSz6UCfMoJSGIi
+3xJMkNCjL0XEw5NKVc5jRKkzSkN5j8KTM/k1jPPsDHPRYzbWWhnNtd6JlkCAwEA
AaN7MHkwCQYDVR0TBAIwADAsBglghkgBhvhCAQ0EHxYdT3BlblNTTCBHZW5lcmF0
ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFP2WWMDShux3iF74+SoO1xf6qhqaMB8G
A1UdIwQYMBaAFGjl6TRXbQDHzSlZu+e8VeBaZMB5MA0GCSqGSIb3DQEBBQUAA4IB
AQAZ/imK7UMognXbs5RfSB8cMW6iNAI+JZqe9XWjvtmLfIIPbHM96o953SiFvrvQ
BZjGmmPMK3UH29cjzDx1R/RQaYTyMrHyTePLh3BMd5mpJ/9eeJCSxPzE2ECqWRUa
pkjukecFXqmRItwgTxSIUE9QkpzvuQRb268PwmgroE0mwtiREADnvTFkLkdiEMew
fiYxZfJJLPBqwlkw/7f1SyzXoPXnz5QbNwDmrHelga6rKSprYKb3pueqaIe8j/AP
NC1/bzp8cGOcJ88BD5+Ny6qgPVCrMLE5twQumJ12V3SvjGNtzFBvg2c/9S5OmVqR
LlTxKnCrWAXftSm1rNtewTsF
-----END CERTIFICATE-----</ds:X509Certificate>
      </ds:X509Data>
    </ds:KeyInfo>
  </KeyDescriptor>

<KeyDescriptor use="encryption">
    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:X509Data>
        <ds:X509Certificate>-----BEGIN CERTIFICATE-----
MIIDnjCCAoagAwIBAgIBATANBgkqhkiG9w0BAQUFADBUMQswCQYDVQQGEwJGUjEP
MA0GA1UECBMGRnJhbmNlMQ4wDAYDVQQHEwVQYXJpczETMBEGA1UEChMKRW50cm91
dmVydDEPMA0GA1UEAxMGRGFtaWVuMB4XDTA2MTAyNzA5MDc1NFoXDTExMTAyNjA5
MDc1NFowVDELMAkGA1UEBhMCRlIxDzANBgNVBAgTBkZyYW5jZTEOMAwGA1UEBxMF
UGFyaXMxEzARBgNVBAoTCkVudHJvdXZlcnQxDzANBgNVBAMTBkRhbWllbjCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM06Hx6VgHYR9wUf/tZVVTRkVWNq
h9x+PvHA2qH4OYMuqGs4Af6lU2YsZvnrmRdcFWv0+UkdAgXhReCWAZgtB1pd/W9m
6qDRldCCyysow6xPPKRz/pOTwRXm/fM0QGPeXzwzj34BXOIOuFu+n764vKn18d+u
uVAEzk1576pxTp4pQPzJfdNLrLeQ8vyCshoFU+MYJtp1UA+h2JoO0Y8oGvywbUxH
ioHN5PvnzObfAM4XaDQohmfxM9Uc7Wp4xKAc1nUq5hwBrHpjFMRSz6UCfMoJSGIi
+3xJMkNCjL0XEw5NKVc5jRKkzSkN5j8KTM/k1jPPsDHPRYzbWWhnNtd6JlkCAwEA
AaN7MHkwCQYDVR0TBAIwADAsBglghkgBhvhCAQ0EHxYdT3BlblNTTCBHZW5lcmF0
ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFP2WWMDShux3iF74+SoO1xf6qhqaMB8G
A1UdIwQYMBaAFGjl6TRXbQDHzSlZu+e8VeBaZMB5MA0GCSqGSIb3DQEBBQUAA4IB
AQAZ/imK7UMognXbs5RfSB8cMW6iNAI+JZqe9XWjvtmLfIIPbHM96o953SiFvrvQ
BZjGmmPMK3UH29cjzDx1R/RQaYTyMrHyTePLh3BMd5mpJ/9eeJCSxPzE2ECqWRUa
pkjukecFXqmRItwgTxSIUE9QkpzvuQRb268PwmgroE0mwtiREADnvTFkLkdiEMew
fiYxZfJJLPBqwlkw/7f1SyzXoPXnz5QbNwDmrHelga6rKSprYKb3pueqaIe8j/AP
NC1/bzp8cGOcJ88BD5+Ny6qgPVCrMLE5twQumJ12V3SvjGNtzFBvg2c/9S5OmVqR
LlTxKnCrWAXftSm1rNtewTsF
-----END CERTIFICATE-----</ds:X509Certificate>
      </ds:X509Data>
    </ds:KeyInfo>
  </KeyDescriptor>
  <SingleLogoutService
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:SOAP"
    Location="http://sp6/singleLogoutSOAP" />
  <SingleLogoutService
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
    Location="http://sp6/singleLogout"
    ResponseLocation="http://sp6/singleLogoutReturn" />
  <ManageNameIDService
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:SOAP"
    Location="http://sp6/manageNameIdSOAP" />
  <ManageNameIDService
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
    Location="http://sp6/manageNameId"
    ResponseLocation="http://sp6/manageNameIdReturn" />
  <AssertionConsumerService isDefault="true" index="0"
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact"
    Location="http://sp6/singleSignOnArtifact" />
  <AssertionConsumerService index="1"
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
    Location="http://sp6/singleSignOnPost" />
  <AssertionConsumerService index="2"
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:PAOS"
    Location="http://sp6/singleSignOnSOAP" />

</SPSSODescriptor>
<Organization>
   <OrganizationName xml:lang="en">WCS Entrouvert</OrganizationName>
</Organization>
</EntityDescriptor>'''
    pdp_metadata = '''<?xml version="1.0"?>
<EntityDescriptor xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
    entityID="http://idp5/metadata">
<PDPDescriptor
    protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">

<KeyDescriptor use="signing">
    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:X509Data>
        <ds:X509Certificate>-----BEGIN CERTIFICATE-----
MIIDnjCCAoagAwIBAgIBATANBgkqhkiG9w0BAQUFADBUMQswCQYDVQQGEwJGUjEP
MA0GA1UECBMGRnJhbmNlMQ4wDAYDVQQHEwVQYXJpczETMBEGA1UEChMKRW50cm91
dmVydDEPMA0GA1UEAxMGRGFtaWVuMB4XDTA2MTAyNzA5MDc1NFoXDTExMTAyNjA5
MDc1NFowVDELMAkGA1UEBhMCRlIxDzANBgNVBAgTBkZyYW5jZTEOMAwGA1UEBxMF
UGFyaXMxEzARBgNVBAoTCkVudHJvdXZlcnQxDzANBgNVBAMTBkRhbWllbjCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM06Hx6VgHYR9wUf/tZVVTRkVWNq
h9x+PvHA2qH4OYMuqGs4Af6lU2YsZvnrmRdcFWv0+UkdAgXhReCWAZgtB1pd/W9m
6qDRldCCyysow6xPPKRz/pOTwRXm/fM0QGPeXzwzj34BXOIOuFu+n764vKn18d+u
uVAEzk1576pxTp4pQPzJfdNLrLeQ8vyCshoFU+MYJtp1UA+h2JoO0Y8oGvywbUxH
ioHN5PvnzObfAM4XaDQohmfxM9Uc7Wp4xKAc1nUq5hwBrHpjFMRSz6UCfMoJSGIi
+3xJMkNCjL0XEw5NKVc5jRKkzSkN5j8KTM/k1jPPsDHPRYzbWWhnNtd6JlkCAwEA
AaN7MHkwCQYDVR0TBAIwADAsBglghkgBhvhCAQ0EHxYdT3BlblNTTCBHZW5lcmF0
ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFP2WWMDShux3iF74+SoO1xf6qhqaMB8G
A1UdIwQYMBaAFGjl6TRXbQDHzSlZu+e8VeBaZMB5MA0GCSqGSIb3DQEBBQUAA4IB
AQAZ/imK7UMognXbs5RfSB8cMW6iNAI+JZqe9XWjvtmLfIIPbHM96o953SiFvrvQ
BZjGmmPMK3UH29cjzDx1R/RQaYTyMrHyTePLh3BMd5mpJ/9eeJCSxPzE2ECqWRUa
pkjukecFXqmRItwgTxSIUE9QkpzvuQRb268PwmgroE0mwtiREADnvTFkLkdiEMew
fiYxZfJJLPBqwlkw/7f1SyzXoPXnz5QbNwDmrHelga6rKSprYKb3pueqaIe8j/AP
NC1/bzp8cGOcJ88BD5+Ny6qgPVCrMLE5twQumJ12V3SvjGNtzFBvg2c/9S5OmVqR
LlTxKnCrWAXftSm1rNtewTsF
-----END CERTIFICATE-----</ds:X509Certificate>
      </ds:X509Data>
    </ds:KeyInfo>
  </KeyDescriptor>

<KeyDescriptor use="encryption">
    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:X509Data>
        <ds:X509Certificate>-----BEGIN CERTIFICATE-----
MIIDnjCCAoagAwIBAgIBATANBgkqhkiG9w0BAQUFADBUMQswCQYDVQQGEwJGUjEP
MA0GA1UECBMGRnJhbmNlMQ4wDAYDVQQHEwVQYXJpczETMBEGA1UEChMKRW50cm91
dmVydDEPMA0GA1UEAxMGRGFtaWVuMB4XDTA2MTAyNzA5MDc1NFoXDTExMTAyNjA5
MDc1NFowVDELMAkGA1UEBhMCRlIxDzANBgNVBAgTBkZyYW5jZTEOMAwGA1UEBxMF
UGFyaXMxEzARBgNVBAoTCkVudHJvdXZlcnQxDzANBgNVBAMTBkRhbWllbjCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM06Hx6VgHYR9wUf/tZVVTRkVWNq
h9x+PvHA2qH4OYMuqGs4Af6lU2YsZvnrmRdcFWv0+UkdAgXhReCWAZgtB1pd/W9m
6qDRldCCyysow6xPPKRz/pOTwRXm/fM0QGPeXzwzj34BXOIOuFu+n764vKn18d+u
uVAEzk1576pxTp4pQPzJfdNLrLeQ8vyCshoFU+MYJtp1UA+h2JoO0Y8oGvywbUxH
ioHN5PvnzObfAM4XaDQohmfxM9Uc7Wp4xKAc1nUq5hwBrHpjFMRSz6UCfMoJSGIi
+3xJMkNCjL0XEw5NKVc5jRKkzSkN5j8KTM/k1jPPsDHPRYzbWWhnNtd6JlkCAwEA
AaN7MHkwCQYDVR0TBAIwADAsBglghkgBhvhCAQ0EHxYdT3BlblNTTCBHZW5lcmF0
ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFP2WWMDShux3iF74+SoO1xf6qhqaMB8G
A1UdIwQYMBaAFGjl6TRXbQDHzSlZu+e8VeBaZMB5MA0GCSqGSIb3DQEBBQUAA4IB
AQAZ/imK7UMognXbs5RfSB8cMW6iNAI+JZqe9XWjvtmLfIIPbHM96o953SiFvrvQ
BZjGmmPMK3UH29cjzDx1R/RQaYTyMrHyTePLh3BMd5mpJ/9eeJCSxPzE2ECqWRUa
pkjukecFXqmRItwgTxSIUE9QkpzvuQRb268PwmgroE0mwtiREADnvTFkLkdiEMew
fiYxZfJJLPBqwlkw/7f1SyzXoPXnz5QbNwDmrHelga6rKSprYKb3pueqaIe8j/AP
NC1/bzp8cGOcJ88BD5+Ny6qgPVCrMLE5twQumJ12V3SvjGNtzFBvg2c/9S5OmVqR
LlTxKnCrWAXftSm1rNtewTsF
-----END CERTIFICATE-----</ds:X509Certificate>
      </ds:X509Data>
    </ds:KeyInfo>
  </KeyDescriptor>
	<AuthzService Binding="urn:oasis:names:tc:SAML:2.0:bindings:SOAP" Location="http://idp6/authzService"/>
	<AssertionIDRequestService Binding="urn:oasis:names:tc:SAML:2.0:bindings:URI" Location="http://idp6/PDPAuthAssertionIDRequestService"/>
	<NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:kerberos</NameIDFormat>
</PDPDescriptor>
<Organization>
   <OrganizationName xml:lang="en">Entr'ouvert</OrganizationName>
</Organization>

</EntityDescriptor>'''
    private_key = '''-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzTofHpWAdhH3BR/+1lVVNGRVY2qH3H4+8cDaofg5gy6oazgB
/qVTZixm+euZF1wVa/T5SR0CBeFF4JYBmC0HWl39b2bqoNGV0ILLKyjDrE88pHP+
k5PBFeb98zRAY95fPDOPfgFc4g64W76fvri8qfXx3665UATOTXnvqnFOnilA/Ml9
00ust5Dy/IKyGgVT4xgm2nVQD6HYmg7Rjyga/LBtTEeKgc3k++fM5t8AzhdoNCiG
Z/Ez1RztanjEoBzWdSrmHAGsemMUxFLPpQJ8yglIYiL7fEkyQ0KMvRcTDk0pVzmN
EqTNKQ3mPwpMz+TWM8+wMc9FjNtZaGc213omWQIDAQABAoIBAEPj5keHzWdBqiXX
38WnlPgv+M9afndCjDANTEYoh14OIUjWzlIe/ufd6HLkrVA89hkwgQbewbyQOT2C
YiSlQLl0PlKMCTIKIzVHD07HvXNTAwykEqNfTZChSYEa1/Ixre+MXvugF8nwdKxk
8xN0qXTQF6OXeVYvQNAAdng743YON4ubqKlEezIwnfG/jcoZrGkiTpx+k1JXJsZN
4dHKFP12RRhUTGjaOkBo41w8GNKQLFpy1vqAOYMyi1SJcrwpAu3H0iQug9SylQaM
bFjt8j/m13gu3zXIJbi8xbyg3nqpxl9dxcZG/cDA9z2tLu/h3G3nPq7CXvkZxmjl
ePvOCwECgYEA9zbwYMtd8tT3PHtrCtjwkfxV0dvMmfNw/rRT4ShWtKLmgX+K9nz/
T4qpbehz4z7OvsLjQ6Bt6wjMNMw9SEBeEMyDVTpmzSD2PowARegmeLX4CsilqHHl
/AMYUtywEQ2f65/CWPiMIt8mLnEyJ/dsyVLpuzGUNNt34Yaqpu2qXnUCgYEA1IUy
PObmTh3I8ZyESyGhbu2TYs0A8Zy6eTIAv0ijOIpmUykzjE5pR9sB3nYEd4GTHPEv
hF6SWfNIDDr83TqThJYzkFyXMCxiVLH55U42wlsvwp4jTnOI3K/7Y7U/lEmBlgcl
JbIIv1t9okg3+Kuu4i7iB6JR89cSO/Wfcdu/c9UCgYAHE5eF7cxeqyH4pT/HK7aX
NzXtr/EHZySQ5fCQvWrd+NvIUTJVI/ba/AklkEXg92dLpqCCyxDabYIK8N3AN7d5
m6EWy3kt3geueqt3VNHlGrBi/qNfUwNWV3BWzuJrWox9XjFeAp9gUCrzoWHiKv7+
NFVkemLXsICaABTaemsqEQKBgQDJJ4n1u1gieG7Kwqs1sg9rP9RRoFlUWFTogjvS
0p4r1lQkQstX8qAUM2gBeROhSjRFIMUpNZqxKWT4rpzJibg3tzP3YKx6HIi2Qf+W
3AFY1ZbPT397sj/JI4l/Rv93DFxr9TdkBq/g8GhqQpE3/sj5rgaj0zBe7SOFPWg+
DRGaQQKBgEEcSF5KmpIHnhi3WlfGiEtx3kcD63orKME0YYA5BM6wnmRT4QiSw+qj
i7ljrKGSbmdMFC3ArM42/k2lXYpVLsYWmyaRYSgbdowxLM1XxDJMFIPR2uG6N+vi
HzWkRxi2SXKU42vfs5eA0itHvQP2DfUx8VuvtwVbOxDGgntYia70
-----END RSA PRIVATE KEY-----'''
    request = saml_authorization_query(sp_metadata, private_key, pdp_metadata,
            name_id=lasso.Saml2NameID.newWithString('bob'),
            action=lasso.SAML2_ACTION_GHPP_GET, 
            action_ns=lasso.SAML2_ACTION_NAMESPACE_GHPP,
            resource='http://example.com/data/1')
    request_msg, url = next(request)
    pdp = SamlAuthorizationQueryProcessor(pdp_metadata, private_key, sp_metadata)
    print request.send(pdp.process(request_msg, force_decision=INDETERMINATE))
