'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import logging

from django.contrib import messages
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.conf import settings

from core import set_default_alias, \
    get_user_administrator_role, get_abac_administrator_role, \
    is_policy_action_creator, is_policy_object_creator, \
    is_policy_user_administrator, is_policy_abac_administrator, \
    is_user_administrator, is_abac_administrator, \
    is_policy_root_administrator, \
    set_default_alias, \
    is_root_administrator, \
    set_root_administrator, remove_root_administrator, \
    set_policy_root_administrator, remove_policy_root_administrator

from decorators import prevent_access_to_normal_users,\
    check_policy_in_session, \
    prevent_access_to_not_root_administrators, \
    prevent_access_to_not_policy_root_administrators

from views import return_add_any

from utils_views import get_policy_from_session

logger = logging.getLogger('acs')

login_url = settings.LOGIN_URL
root_url = settings.ROOT_URL


'''
''''''
    User Management
''''''
'''


@csrf_exempt
@prevent_access_to_normal_users
def add_user(request):
    if not is_user_administrator(request.user):
        messages.add_message(request, messages.ERROR,
            _('You are not authorized to add a user'))
        return HttpResponseRedirect(root_url)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect(root_url)
        form = UserCreationForm(request.POST)
        if form.is_valid():
            '''Add user'''
            user = form.save()
            messages.add_message(request, messages.INFO,
                _('User %s added') % user)
            return HttpResponseRedirect(root_url)
    else:
        form = UserCreationForm()
    title = _('Add a new user')
    return return_add_any(request, form, title)


@csrf_exempt
@prevent_access_to_not_root_administrators
def manage_root_administrators(request):
    if request.method == 'GET':
        return return_manage_root_administrators(request)

    if request.method == 'POST':
        if (not 'empower' in request.POST \
                    or not 'downgrade' in request.POST) \
                and not 'id' in request.POST:
            logger.error('manage_root_administrators: \
                unknown form action by %s' % request.user)
            messages.add_message(request, messages.ERROR, _('Invalid action'))
            return return_manage_root_administrators(request)
        if 'empower' in request.POST:
            logger.debug('manage_root_administrators: the id is %s' \
                % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_root_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_root_administrators(request)
            r = set_root_administrator(request.user, user)
            if r == -3:
                messages.add_message(request, messages.ERROR,
                    _('%s has not been empowered') % user)
            elif r == -4:
                messages.add_message(request, messages.ERROR,
                    _('%s is already a root user') % user)
            elif r == 0:
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been empowered') % user)
            else:
                messages.add_message(request, messages.ERROR,
                    _('%s has not been empowered') % user)
            return return_manage_root_administrators(request)

        if 'downgrade' in request.POST:
            logger.debug('manage_root_administrators: \
                the id is %s' % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_root_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_root_administrators(request)
            r = remove_root_administrator(request.user, user)
            if r == -3:
                messages.add_message(request, messages.ERROR,
                    _('%s cannot downgrade itself') % request.user)
            elif r == -4:
                messages.add_message(request, messages.ERROR,
                    _('%s is not a root user') % user)
            elif r == 0:
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been downgraded') % user)
            else:
                messages.add_message(request, messages.ERROR,
                    _('%s has not been downgraded') % user)
            return return_manage_root_administrators(request)

    logger.critical('manage_root_administrators: \
        not GET or POST, methos is %s' % request.method)
    messages.add_message(request, messages.ERROR,
        _('Unable to perform request'))
    return return_manage_root_administrators(request)


@prevent_access_to_not_root_administrators
def return_manage_root_administrators(request,
        template_name='manage_root_administrators.html'):
    root_users = []
    not_root_users = []
    for user in User.objects.all():
        if user.id != request.user.id:
            if is_root_administrator(user):
                root_users.append(user)
            else:
                not_root_users.append(user)
    tpl_parameters = {'backlink': root_url,
                    'root_users': root_users,
                    'not_root_users': not_root_users}
    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))


@csrf_exempt
@prevent_access_to_not_root_administrators
def manage_user_administrators(request):
    if request.method == 'GET':
        return return_manage_user_administrators(request)

    if request.method == 'POST':
        if (not 'empower' in request.POST \
                    or not 'downgrade' in request.POST) \
                and not 'id' in request.POST:
            logger.error('manage_user_administrators: \
                unknown form action by %s' % request.user)
            messages.add_message(request, messages.ERROR,
                _('Invalid action'))
            return return_manage_user_administrators(request)
        if 'empower' in request.POST:
            logger.debug('manage_user_administrators: \
                the id is %s' % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_user_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_user_administrators(request)
            if is_user_administrator(user):
                logger.error('manage_user_administrators: \
                    Ask to empower %s that is already a user administrator' \
                    % user)
                messages.add_message(request, messages.ERROR,
                    _('%s is already a user administrator') % user)
                return return_manage_user_administrators(request)
            if not set_default_alias(user):
                    logger.critical('manage_root_administrators: \
                        %s can not be initialized for administration' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            '''Empower user'''
            user_admin_role = get_user_administrator_role()
            if not user_admin_role:
                    logger.critical('manage_root_administrators: \
                        No existing user admin role')
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            try:
                user_admin_role.users.add(set_default_alias(user))
                logger.info('manage_user_administrators: \
                    %s added to the user administrators role' % user)
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been empowered') % user)
            except:
                logger.info('manage_user_administrators: \
                    failed to add %s to the user administrators role' % user)
                messages.add_message(request, messages.ERROR,
                    _('%s has not been empowered') % user)
            return return_manage_user_administrators(request)

        if 'downgrade' in request.POST:
            logger.debug('manage_user_administrators: the id is %s' \
                % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_user_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_user_administrators(request)
            logger.debug('manage_user_administrators: the user is %s' % user)
            if is_root_administrator(user):
                logger.error('manage_root_administrators: Ask to downgrade \
                    %s that is a root user that can not be done.' % user)
                messages.add_message(request, messages.ERROR,
                    _('Ask to downgrade %s that is a root user that can not \
                        be done.') % user)
                return return_manage_root_administrators(request)
            if not is_user_administrator(user):
                logger.error('manage_user_administrators: \
                    Ask to downgrade %s that is not a user administrator' \
                        % user)
                messages.add_message(request, messages.ERROR,
                    _('%s is not a user administrator') % user)
                return return_manage_user_administrators(request)
            '''Downgrade user'''
            user_admin_role = get_user_administrator_role()
            if not user_admin_role:
                    logger.critical('manage_root_administrators: \
                        No existing user admin role')
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            try:
                user_admin_role.users.remove(set_default_alias(user))
                logger.info('manage_user_administrators: \
                    %s deleted of the root admin role' % user)
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been downgraded') % user)
            except:
                logger.info('manage_user_administrators: \
                    failed to delete %s of the root admin role' % user)
                messages.add_message(request, messages.ERROR,
                    _('%s has not been downgraded') % user)
            return return_manage_user_administrators(request)

    logger.critical('manage_user_administrators: not GET or POST, \
        methos is %s' % request.method)
    messages.add_message(request, messages.ERROR,
        _('Unable to perform request'))
    return return_manage_user_administrators(request)


@csrf_exempt
@prevent_access_to_not_root_administrators
def manage_abac_administrators(request):
    if request.method == 'GET':
        return return_manage_abac_administrators(request)

    if request.method == 'POST':
        if (not 'empower' in request.POST \
                    or not 'downgrade' in request.POST) \
                and not 'id' in request.POST:
            logger.error('manage_abac_administrators: \
                unknown form action by %s' % request.user)
            messages.add_message(request, messages.ERROR,
                _('Invalid action'))
            return return_manage_abac_administrators(request)
        if 'empower' in request.POST:
            logger.debug('manage_abac_administrators: \
                the id is %s' % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_abac_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_abac_administrators(request)
            if is_abac_administrator(user):
                logger.error('manage_abac_administrators: \
                    Ask to empower %s that is already an abac administrator' \
                    % user)
                messages.add_message(request, messages.ERROR,
                    _('%s is already an abac administrator') % user)
                return return_manage_abac_administrators(request)
            if not set_default_alias(user):
                    logger.critical('manage_root_administrators: \
                        %s can not be initialized for administration' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            '''Empower user'''
            abac_admin_role = get_abac_administrator_role()
            if not abac_admin_role:
                    logger.critical('manage_root_administrators: \
                        No existing abac admin role')
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            try:
                abac_admin_role.users.add(set_default_alias(user))
                logger.info('manage_abac_administrators: \
                    %s added to the abac administrators role' % user)
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been empowered') % user)
            except:
                logger.info('manage_abac_administrators: \
                    failed to add %s to the abac administrators role' % user)
                messages.add_message(request, messages.ERROR,
                    _('%s has not been empowered') % user)
            return return_manage_abac_administrators(request)

        if 'downgrade' in request.POST:
            logger.debug('manage_abac_administrators: the id is %s' \
                % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_abac_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_abac_administrators(request)
            logger.debug('manage_abac_administrators: the user is %s' % user)
            if is_root_administrator(user):
                logger.error('manage_root_administrators: Ask to downgrade \
                    %s that is a root user that can not be done.' % user)
                messages.add_message(request, messages.ERROR,
                    _('Ask to downgrade %s that is a root user that can not \
                        be done.') % user)
                return return_manage_root_administrators(request)
            if not is_abac_administrator(user):
                logger.error('manage_abac_administrators: \
                    Ask to downgrade %s that is not an abac administrator' \
                        % user)
                messages.add_message(request, messages.ERROR,
                    _('%s is not an abac administrator') % user)
                return return_manage_abac_administrators(request)
            '''Downgrade user'''
            abac_admin_role = get_abac_administrator_role()
            if not abac_admin_role:
                    logger.critical('manage_root_administrators: \
                        No existing abac admin role')
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_root_administrators(request)
            try:
                abac_admin_role.users.remove(set_default_alias(user))
                logger.info('manage_abac_administrators: \
                    %s deleted of the root admin role' % user)
                messages.add_message(request, messages.INFO,
                    _('%s has successfully been downgraded') % user)
            except:
                logger.info('manage_abac_administrators: \
                    failed to delete %s of the root admin role' % user)
                messages.add_message(request, messages.ERROR,
                    _('%s has not been downgraded') % user)
            return return_manage_abac_administrators(request)

    logger.critical('manage_abac_administrators: not GET or POST, \
        methos is %s' % request.method)
    messages.add_message(request, messages.ERROR,
        _('Unable to perform request'))
    return return_manage_abac_administrators(request)


@prevent_access_to_not_root_administrators
def return_manage_user_administrators(request,
        template_name='empower_or_downgrade.html'):
    empowered = []
    not_empowered = []
    not_manageable = []
    for user in User.objects.all():
        if user.id != request.user.id:
            if is_root_administrator(user):
                not_manageable.append(user)
            elif is_user_administrator(user):
                empowered.append(user)
            else:
                not_empowered.append(user)
    tpl_parameters = \
        {'backlink': root_url,
        'empowered': empowered,
        'not_empowered': not_empowered,
        'not_manageable': not_manageable,
        'explanation': \
    _('The following users are root administrators and cannot be managed.'),
        'title': \
            _('Empower or downgrade user administrators'),
        'subtitle': \
    _('Here you can give or remove the capability of a user to manage users \
        and aliases in all policies.')}
    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))


@prevent_access_to_not_root_administrators
def return_manage_abac_administrators(request,
        template_name='empower_or_downgrade.html'):
    empowered = []
    not_empowered = []
    not_manageable = []
    for user in User.objects.all():
        if user.id != request.user.id:
            if is_root_administrator(user):
                not_manageable.append(user)
            elif is_abac_administrator(user):
                empowered.append(user)
            else:
                not_empowered.append(user)
    tpl_parameters = \
        {'backlink': root_url,
        'empowered': empowered,
        'not_empowered': not_empowered,
        'not_manageable': not_manageable,
        'explanation': \
    _('The following users are root administrators and cannot be managed.'),
        'title': \
            _('Empower or downgrade user administrators'),
        'subtitle': \
    _('Here you can give or remove the capability of a user to manage ABAC \
        rules and permissions.')}
    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))


'''
''''''
    Root policy roles management views
''''''
'''


@csrf_exempt
@prevent_access_to_not_policy_root_administrators
def manage_policy_administrators(request):
    policy = get_policy_from_session(request)
    policy_admin_types = ('root', 'user', 'abac', 'object', 'action')
    if request.method == 'GET':
        if 'type' in request.GET \
                and request.GET['type'] in policy_admin_types:
            return return_manage_policy_administrators(request,
                request.GET['type'])
        messages.add_message(request, messages.ERROR, _('Invalid action'))
        HttpResponseRedirect('mod_policy?id=' + str(policy.id))

    user = None
    if request.method == 'POST':
        if (not 'empower' in request.POST \
                    or not 'downgrade' in request.POST) \
                and not ('id' in request.POST or 'type' in request.POST):
            logger.error('manage_policy_administrators: missing field by %s' \
                % request.user)
            messages.add_message(request, messages.ERROR, _('Invalid action'))
            HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        if request.GET['type'] not in policy_admin_types:
            logger.error('manage_policy_administrators: \
                unknown type of admin by %s' % request.user)
            messages.add_message(request, messages.ERROR, _('Invalid action'))
            HttpResponseRedirect('mod_policy?id=' + str(policy.id))

        type_admin = request.POST['type']

        if 'empower' in request.POST:
            logger.debug('manage_policy_administrators: the id is %s'
                % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_policy_administrators: \
                    unknown user with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_policy_administrators(request,
                    type_admin)
            if not set_default_alias(user):
                    logger.critical('manage_policy_administrators: \
                        %s can not be initialized for administration' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
            if type_admin == 'root':
                r = set_policy_root_administrator(request.user, user, policy)
                '''
                    Already tested:
                    -1: Missing argument
                    -2: requester not authorized
                    -3: test default alias
                '''
                if r == -4:
                    messages.add_message(request, messages.ERROR,
                        _('%s is already a policy root admin') % user)
                elif r == 0:
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been empowered') % user)
                else:
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            elif type_admin == 'user':
                if is_policy_user_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        empower %s that is already a user administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s already has this capacity') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    if not set_default_alias(user):
                        logger.critical('manage_policy_administrators: \
                            %s can not be initialized for administration' \
                            % user)
                        messages.add_message(request, messages.ERROR,
                            _('%s has not been empowered') % user)
                        return return_manage_policy_administrators(request)
                    policy.user_admin_role.users.add(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s added to %s' % (user, policy.user_admin_role))
                    messages.add_message(request, messages.INFO,
                    _('%s has successfully been empowered') % user)
                except:
                    logger.info('manage_policy_administrators: failed to add \
                        %s to %s' % (user, policy.user_admin_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            elif type_admin == 'abac':
                if is_policy_abac_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        empower %s that is already an abac administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s already has this capacity') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    if not set_default_alias(user):
                        logger.critical('manage_policy_administrators: \
                            %s can not be initialized for administration' \
                            % user)
                        messages.add_message(request, messages.ERROR,
                            _('%s has not been empowered') % user)
                        return return_manage_policy_administrators(request)
                    policy.abac_admin_role.users.add(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s added to %s' % (user, policy.abac_admin_role))
                    messages.add_message(request, messages.INFO,
                    _('%s has successfully been empowered') % user)
                except:
                    logger.info('manage_policy_administrators: failed to add \
                        %s to %s' % (user, policy.abac_admin_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            elif type_admin == 'object':
                if is_policy_object_creator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        empower %s that is already an object creator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s already has this capacity') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    if not set_default_alias(user):
                        logger.critical('manage_policy_administrators: %s \
                            can not be initialized for administration' % user)
                        messages.add_message(request, messages.ERROR,
                            _('%s has not been empowered') % user)
                        return return_manage_policy_administrators(request)
                    policy.\
                        object_creator_role.users.add(set_default_alias(user))
                    logger.info('manage_policy_administrators: %s added to \
                        %s' % (user, policy.object_creator_role))
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been empowered') % user)
                except:
                    logger.info('manage_policy_administrators: failed to add \
                        %s to %s' % (user, policy.object_creator_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            elif type_admin == 'action':
                if is_policy_action_creator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        empower %s that is already an action creator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s already has this capacity') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    if not set_default_alias(user):
                        logger.critical('manage_policy_administrators: %s \
                            can not be initialized for administration' % user)
                        messages.add_message(request, messages.ERROR,
                            _('%s has not been empowered') % user)
                        return return_manage_policy_administrators(request)
                    policy.\
                        action_creator_role.users.add(set_default_alias(user))
                    logger.info('manage_policy_administrators: %s added to \
                        %s' % (user, policy.action_creator_role))
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been empowered') % user)
                except:
                    logger.info('manage_policy_administrators: failed to add \
                        %s to %s' % (user, policy.action_creator_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            return return_manage_policy_administrators(request, type_admin)

        if 'downgrade' in request.POST:
            logger.debug('manage_policy_administrators: the id is %s'
                % request.POST['id'])
            try:
                user = User.objects.get(id=request.POST['id'])
            except:
                logger.error('manage_policy_administrators: unknown user \
                    with id %s' % request.POST['id'])
                messages.add_message(request, messages.ERROR,
                    _('Unkown user'))
                return return_manage_policy_administrators(request,
                    type_admin)
            logger.debug('manage_policy_administrators: the user is %s'
                % user)

            if is_root_administrator(user):
                logger.error('manage_policy_administrators: Ask to downgrade \
                    %s that is a root administrator' % user)
                messages.add_message(request, messages.ERROR,
                    _('%s is a root administrator and cannot be downgraded')
                    % user)
                return return_manage_policy_administrators(request,
                    type_admin)

            if type_admin == 'root':
                r = remove_policy_root_administrator(request.user, user,
                    policy)
                '''
                    Already tested:
                    -1: Missing argument
                    -2: requester not authorized
                    -3: test default alias
                '''
                if r == -4:
                    messages.add_message(request, messages.ERROR,
                        _('%s is not a policy root admin') % user)
                elif r == 0:
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been empowered') % user)
                else:
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been empowered') % user)
            elif type_admin == 'user':
                if is_policy_root_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root policy administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is a root policy administrator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if is_user_administrator(user):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root user administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR, _('%s is a \
                        root user administrator and cannot be downgraded')
                        % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if not is_policy_user_administrator(user, policy):
                    logger.error('manage_policy_administrators: \
                        Ask to downgrade %s that is not a policy user \
                        administrator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is not a policy user administrator and cannot \
                        be downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    policy.\
                        user_admin_role.users.remove(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s deleted of the policy admin role' % user)
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been downgraded') % user)
                except:
                    logger.info('manage_policy_administrators: \
                        failed to delete %s of %s' \
                        % (user, policy.user_admin_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been downgraded') % user)
            elif type_admin == 'abac':
                if is_policy_root_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root policy administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is a root policy administrator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if is_abac_administrator(user):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root abac administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR, _('%s is a \
                        root abac administrator and cannot be downgraded')
                        % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if not is_policy_abac_administrator(user, policy):
                    logger.error('manage_policy_administrators: \
                        Ask to downgrade %s that is not a policy abac \
                        administrator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is not a policy abac administrator and cannot \
                        be downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    policy.\
                        abac_admin_role.users.remove(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s deleted of the policy admin role' % user)
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been downgraded') % user)
                except:
                    logger.info('manage_policy_administrators: \
                        failed to delete %s of %s' \
                        % (user, policy.abac_admin_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been downgraded') % user)
            elif type_admin == 'object':
                if is_policy_root_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root policy administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is a root policy administrator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if not is_policy_object_creator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is not an object creator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is not an object creator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    policy.object_creator_role.\
                        users.remove(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s deleted of the policy admin role' % user)
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been downgraded') % user)
                except:
                    logger.info('manage_policy_administrators: failed to \
                        delete %s of %s' % (user, policy.object_creator_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been downgraded') % user)
            elif type_admin == 'action':
                if is_policy_root_administrator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is a root policy administrator' \
                        % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is a root policy administrator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                if not is_policy_action_creator(user, policy):
                    logger.error('manage_policy_administrators: Ask to \
                        downgrade %s that is not an action creator' % user)
                    messages.add_message(request, messages.ERROR,
                        _('%s is not an action creator and cannot be \
                        downgraded') % user)
                    return return_manage_policy_administrators(request,
                        type_admin)
                try:
                    policy.action_creator_role.\
                        users.remove(set_default_alias(user))
                    logger.info('manage_policy_administrators: \
                        %s deleted of the policy admin role' % user)
                    messages.add_message(request, messages.INFO,
                        _('%s has successfully been downgraded') % user)
                except:
                    logger.info('manage_policy_administrators: \
                        failed to delete %s of %s' \
                        % (user, policy.action_creator_role))
                    messages.add_message(request, messages.ERROR,
                        _('%s has not been downgraded') % user)
            return return_manage_policy_administrators(request, type_admin)

    logger.critical('manage_policy_administrators: \
        not GET or POST, methos is %s' % request.method)
    messages.add_message(request, messages.ERROR,
        _('Unable to perform request'))
    return return_manage_policy_administrators(request, type_admin)


@check_policy_in_session
@prevent_access_to_not_policy_root_administrators
def return_manage_policy_administrators(request, type_admin=None,
        template_name='empower_or_downgrade.html'):
    policy = get_policy_from_session(request)
    empowered = []
    not_empowered = []
    not_manageable = []
    tpl_parameters = {}
    for user in User.objects.all():
        if type_admin == 'root':
            if user.id != request.user.id:
                if is_root_administrator(user):
                    not_manageable.append(user)
                elif is_policy_root_administrator(user, policy):
                    empowered.append(user)
                else:
                    not_empowered.append(user)
            tpl_parameters = \
                {'explanation': \
                    _('The following users are root administrators and \
                    cannot be managed.'),
                'title': _('Empower or downgrade root policy administrators'),
                'subtitle': \
                    _('Here you can give or remove the capability of a user \
                    to be an administrator of the whole policy.')}
        elif type_admin == 'user':
            if user.id != request.user.id:
                if is_user_administrator(user) \
                        or is_policy_root_administrator(user, policy):
                    not_manageable.append(user)
                elif is_policy_user_administrator(user, policy):
                    empowered.append(user)
                else:
                    not_empowered.append(user)
            tpl_parameters = \
                {'explanation': \
                    _('The following users are root user administrators or \
                    root policy administrator and cannot be managed.'),
                'title': _('Empower or downgrade user policy administrators'),
                'subtitle': \
                    _('Here you can give or remove the capability of a user \
                    to be an user administrator for this policy.')}
        elif type_admin == 'abac':
            if user.id != request.user.id:
                if is_abac_administrator(user) \
                        or is_policy_abac_administrator(user, policy):
                    not_manageable.append(user)
                elif is_policy_abac_administrator(user, policy):
                    empowered.append(user)
                else:
                    not_empowered.append(user)
            tpl_parameters = \
                {'explanation': \
                    _('The following users are root abac administrators or \
                    root policy administrator and cannot be managed.'),
                'title': _('Empower or downgrade abac policy administrators'),
                'subtitle': \
                    _('Here you can give or remove the capability of a user \
                    to be an abac administrator for this policy.')}
        elif type_admin == 'object':
            if user.id != request.user.id:
                if is_policy_root_administrator(user, policy):
                    not_manageable.append(user)
                elif is_policy_object_creator(user, policy):
                    empowered.append(user)
                else:
                    not_empowered.append(user)
            tpl_parameters = \
                {'explanation': \
                    _('The following users are root policy administrators \
                    and cannot be managed.'),
                'title': _('Empower or downgrade objects and views creators'),
                'subtitle': \
                    _('Here you can give or remove the capability of a user \
                    to be a creator of objects and views.')}
        elif type_admin == 'action':
            if user.id != request.user.id:
                if is_policy_root_administrator(user, policy):
                    not_manageable.append(user)
                elif is_policy_action_creator(user, policy):
                    empowered.append(user)
                else:
                    not_empowered.append(user)
            tpl_parameters = \
                {'explanation': \
                    _('The following users are root policy administrators \
                    and cannot be managed.'),
                'title': \
                    _('Empower or downgrade actions and activities creators'),
                'subtitle': \
                    _('Here you can give or remove the capability of a user \
                    to be a creator of actions and activities.')}

    tpl_parameters['backlink'] = 'mod_policy?id=' + str(policy.id)
    tpl_parameters['empowered'] = empowered
    tpl_parameters['not_empowered'] = not_empowered
    tpl_parameters['not_manageable'] = not_manageable
    tpl_parameters['type_admin'] = type_admin

    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))
