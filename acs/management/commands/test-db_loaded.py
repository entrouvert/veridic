'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import time
import random
import string

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db import reset_queries

from acs.models import Policy, Role, UserAlias, AcsObject, Action

from acs.core import set_root_administrator, create_policy, \
    set_policy_root_administrator, \
    add_role, mod_role, add_object, add_action, add_permission, \
    is_authorized_on_object_or_view, remove_policy, \
    is_authorized_on_action_or_activity, \
    isAuthorizedRBAC2, is_authorized_on_alias_or_role


class Command(BaseCommand):
    '''
        Load the database
    '''

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    option_list = BaseCommand.option_list
    args = '<username>'
    help = \
        'Execute giving the username of a root administrator.'

    def handle(self, *args, **options):

        if not args:
            raise CommandError('No username on the command line')

        root_user = args[0]

        rdm_str = ''.join(random.choice(string.ascii_uppercase + \
            string.digits) for x in range(8))
        print 'All objects of this tests begin or contain the string %s' \
            % rdm_str

        size = 3
        print "Creation of:"
        print "\t- %s users" % str(size*10)
        print "\t- %s policies" % str(size)
        print "\t- %s users in each policy, total of %s" \
            % (str(size*10), str(size*size*10))
        print "\t- %s roles in each policy, total of %s" \
            % (str(size*10), str(size*size*10))
        print "\t- %s actions in each policy, total of %s" \
            % (str(size*10), str(size*size*10))
        print "\t- %s objects in each policy, total of %s" \
            % (str(size*10), str(size*size*10))
        print "\t- %s permissions in each policy, total of %s" \
            % (str(size*10), str(size*size*10))
        print "Total objects without systems objects: %s" \
            % str((size*size*10*5) + size + size *10)


        '''Create 100 users'''
        print '--> Set some users'
        users = []
        nb = size * 10
        i = 0
        j = 0
        while i < nb:
            username = 'user_' + str(i)
            user, created = User.objects.get_or_create(username=username)
            if not user:
                raise CommandError('Error with user %s' %user)
            if created:
                j = j +1
            sys.stdout.write('.')
            sys.stdout.flush()
            user.set_password(username)
            user.save()
            users.append(user)
            i = i + 1
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Users created: %s' % str(j)
        print 'already existing users: %s' % str(i-j)
        print '<--\n'

        '''Set 100 root administrators'''
        print '--> Set root administrators [user_0; user_size['
        requester = User.objects.get(username=root_user)
        nb = size
        i = 0
        j = 0
        while i < nb:
            user = User.objects.get(username='user_' + str(i))
            r = set_root_administrator(requester, user)
            if r == -4:
                j = j + 1
            elif  r == 0:
                pass
            else:
                raise \
                CommandError('Error setting user %s root admin due to %s' \
                % (user, str(r)))
            requester = user
            sys.stdout.write('.')
            sys.stdout.flush()
            i = i + 1
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Users set root admin: %s' % str(i-j)
        print 'Users already root admin: %s' % str(j)
        print '<--\n'

        '''Create 100 policy'''
        print '--> Create some policy'
        policies = []
        nb = size
        i = 0
        j = 0
        while i < nb:
            name = rdm_str + '_policy_' + str(i)
            namespace = name + '_namespace'
            '''user creator: a root administrator'''
            user = User.objects.get(username='user_' + str(i))
            policy = None
            try:
                policy, created = create_policy(name, user,
                    namespace=namespace)
                if created:
                    j = j + 1
            except Exception, err:
                raise CommandError('Unable to create policy %s due to %s' \
                    % (name, err))
            policies.append(policy)
            sys.stdout.write('.')
            sys.stdout.flush()
            i = i + 1
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Policies created: %s' % str(j)
        print 'Existing policies: %s' % str(i-j)
        print '<--\n'

        '''For each policy, define a root policy administrator'''
        print '--> Set root policy administrators [user_size; user_size*2['
        nb = size
        i = 0
        j = 0
        while i < nb:
            username = 'user_' + str(i)
            requester = \
                User.objects.get(username=username)
            target = User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            r = set_policy_root_administrator(requester, target, policy)
            if r != 0:
                if r == -4:
                    j = j + 1
                else:
                    raise CommandError('Unable to set policy root admin \
                        %s by %s in %s due to %s' \
                        % (target, requester, policy, str(r)))
            sys.stdout.write('.')
            sys.stdout.flush()
            i = i + 1
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Users set policy root administrator: %s' % str(i-j)
        print 'Users already policy administrators: %s' % str(j)
        print '<--\n'

        print '--> Set users in policy'
        nb = size
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            for u in users:
                alias, created = \
                    UserAlias.objects.get_or_create(alias=u.username,
                    namespace=policy.namespace)
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Policy synchronized without errors: %s' % str(i)
        print 'Policy synchronized with errors: %s' % str(j)
        print '<--\n'

        print '--> Create roles in each policy'
        nb = size
        nb2 = size * 10
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            k = 0
            while k < nb2:
                role_name = 'role_' + policy.name + '_' + str(k)
                if not add_role(requester, role_name, policy):
                    raise CommandError('Unable to handle a role due to %s' \
                        % str(r))
                k = k + 1
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Roles in policy created without errors: %s' % str(i)
        print 'Roles in policy created with errors: %s' % str(j)
        print '<--\n'

        print '--> Create a role hierarchy in each policy'
        nb = size
        nb2 = size * 10
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            k = 0
            while k < nb2-1:
                role_name_senior = 'role_' + policy.name + '_' + str(k)
                role_senior = Role.objects.get(name=role_name_senior,
                    namespace=policy.namespace)
                role_name_junior = 'role_' + policy.name + '_' + str(k+1)
                role_junior = Role.objects.get(name=role_name_junior,
                    namespace=policy.namespace)
                user = UserAlias.objects.get(alias='user_' + str(k),
                    namespace=policy.namespace)
                r = mod_role(requester, role_senior, policy,
                    users_added=[user], roles_added=[role_junior])
                if r < 0:
                    raise CommandError('Unable to handle a role due to %s' \
                        % str(r))
                k = k + 1
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Role hierarchy created without errors: %s' % str(i)
        print 'Role hierarchy created with errors: %s' % str(j)
        print '<--\n'

        print '--> Create objects in each policy'
        nb = size
        nb2 = size * 10
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            k = 0
            while k < nb2:
                object_name = 'object_' + policy.name + '_' + str(k)
                if not add_object(requester, object_name, policy):
                    raise CommandError('Unable to handle an object due to \
                        %s' % str(r))
                k = k + 1
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Objects in policy created without errors: %s' % str(i)
        print 'Objects in policy created with errors: %s' % str(j)
        print '<--\n'

        print '--> Create actions in each policy'
        nb = size
        nb2 = size * 10
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            k = 0
            while k < nb2:
                action_name = 'action_' + policy.name + '_' + str(k)
                if not add_action(requester, action_name, policy):
                    raise CommandError('Unable to handle an action due to \
                        %s' % str(r))
                k = k + 1
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Actions in policy created without errors: %s' % str(i)
        print 'Actions in policy created with errors: %s' % str(j)
        print '<--\n'

        print '--> Create permissions in each policy'
        nb = size
        nb2 = size * 10
        i = 0
        j = 0
        while i < nb:
            requester = \
                User.objects.get(username='user_' + str(size+i))
            policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
            k = 0
            while k < nb2:
                role_name = 'role_' + policy.name + '_' + str(k)
                who = Role.objects.get(name=role_name,
                    namespace=policy.namespace)
                object_name = 'object_' + policy.name + '_' + str(k)
                what = AcsObject.objects.get(name=object_name,
                    namespace=policy.namespace)
                action_name = 'action_' + policy.name + '_' + str(k)
                how = Action.objects.get(name=action_name,
                    namespace=policy.namespace)
                p = add_permission(requester, policy, who, what, how,
                    delegable=False)
                if not p:
                    raise CommandError('Unable to handle a permission due to \
                        %s' % str(r))
                k = k + 1
            i = i + 1
            sys.stdout.write('.')
            sys.stdout.flush()
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Permissions in policy created without errors: %s' % str(i)
        print 'Permissions in policy created with errors: %s' % str(j)
        print '<--\n'

        print '--> Test permissions'
        nb = size * 10
        i = 0
        m = 0
        n = 0
        requester = \
            User.objects.get(username='user_' + str(size+i))
        policy = Policy.objects.get(name=rdm_str + '_policy_' + str(i))
        print 'Policy: %s' % policy
        sys.stdout.flush()
        k = 0
        total = 0
        maximum = 0

        while k < nb:

            user_name = 'user_' + str(k)
            who = UserAlias.objects.get(alias=user_name,
                namespace=policy.namespace)
            if is_authorized_on_alias_or_role(requester, who, policy) < 0:
                raise CommandError('Requester not authorized on %s' \
                    % who)

            print 'User: %s' % who

            l = 0
            d = 0
            maxi = 0
            while l < nb:

                object_name = 'object_' + policy.name + '_' + str(l)
                what = AcsObject.objects.get(name=object_name,
                    namespace=policy.namespace)
                action_name = 'action_' + policy.name + '_' + str(l)
                how = Action.objects.get(name=action_name,
                    namespace=policy.namespace)

                if is_authorized_on_object_or_view(requester, what,
                        policy) < 0:
                    raise CommandError('Requester not authorized on %s' \
                        % what)
                if is_authorized_on_action_or_activity(requester, how,
                        policy) < 0:
                    raise CommandError('Requester not authorized on %s' \
                        % how)

                t0 = time.time()
                p = isAuthorizedRBAC2(who, what, how)
                t1 = time.time()
                delta = t1 - t0
                maxi = max(maxi, delta)
                d = d + delta
                authz = False
                if p:
                    authz = True
                    sys.stdout.write('+')
                else:
                    sys.stdout.write('-')

                if k <= l:
                    if authz:
                        sys.stdout.write('+')
                        m = m + 1
                    else:
                        sys.stdout.write('-')
                        n = n + 1
                else:
                    if not authz:
                        sys.stdout.write('+')
                        m = m + 1
                    else:
                        sys.stdout.write('-')
                        n = n + 1

                sys.stdout.flush()

                l = l + 1

            print '\nNumber of requests: %s' % str(l)
            print 'Average request time: %s' % str(d/l)
            print 'Requests per seconds: %s' % str(1/(d/l))
            print 'Slower request: %s' % str(maxi)
            '''Test 1/20 user'''
            k = k + 20
            total = total + d
            maximum = max(maxi, maximum)

            '''
                Due to memory leakage, else Debug=False
                https://docs.djangoproject.com/en/dev/faq/models/
                #why-is-django-leaking-memory
            '''
            reset_queries()

        req = m + n
        print '\nTotal number of requests: %s' % str(req)
        print 'Average request time: %s' % str(total/req)
        print 'Requests per seconds: %s' % str(1/(total/req))
        print 'Slower request: %s' % str(maximum)
        print 'Successful permissions tested: %s' % str(m)
        print 'Error testing permissions: %s' % str(n)
        print '<--\n'

        print '-------- CLEANING --------'
        requester = User.objects.get(username=root_user)
        for u in users:
            u.delete()
        for p in policies:
            remove_policy(p.name, requester)

        print '-------- DONE --------'
