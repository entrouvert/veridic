'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import CheckboxSelectMultiple
from django.contrib.auth.models import User
from registration.forms import RegistrationForm

from models import Action, Activity, AcsObject, Role, View, Namespace

from attribute_aggregator.models import AttributeSource, LdapSource


logger = logging.getLogger('acs')

attrs_dict = {'class': 'required'}


class AuthenticRegistrationForm(RegistrationForm):
    username = forms.RegexField(regex=r'^\w+$',
        max_length=30,
        widget=forms.TextInput(attrs=attrs_dict),
        label=_(u'username'),
        error_messages = {'invalid': \
        _(u'your username must contain only letters, numbers and no spaces')})

#class AcsUserCreationForm(UserCreationForm):
#    make_it_admin = forms.BooleanField(required=False, \
#    label=_("Tick to make this user able \
#    to administrate the access control policy."))

'''
def mk_authz_who_list(user):
    ret = []

    administration = Action.objects.get(name='administration')

    users = User.objects.all()
    for it in users:
        if isAuthorizedRBAC2(user, it, administration):
            ret.append((it.username, it.username))
    roles = Role.objects.all()
    for it in roles:
        if isAuthorizedRBAC2(user, it, administration):
            ret.append((it.name, it.name))
    return ret
'''


'''I was unable to modify query sets to only contain the authorized objects as
computed in mk_authz_who_list for instance.
We restrict at display giving an another list to the template.
class AddPermissionForm(forms.Form):
#    def __init__(self, *args, **kwargs):
#        super(AddPermissionForm, self).__init__(*args, **kwargs)
#        self.fields["who"].widget = CheckboxSelectMultiple()
#        self.fields["who"].help_text = None
#        self.fields["what"].widget = CheckboxSelectMultiple()
#        self.fields["what"].help_text = None
#        self.fields["how"].widget = CheckboxSelectMultiple()
#        self.fields["how"].help_text = None

#    class Meta:
#        model = AcsPermissions2

    who_matches = forms.ChoiceField(
        choices = mk_who_list()
    )
    what_matches = forms.ChoiceField(
        choices = mk_what_list()
    )
    how_matches = forms.ChoiceField(
        choices = mk_how_list()
    )

#    def __init__(self, *args, **kwargs):
#        self.user = kwargs.pop('user', None)
#        import sys
#        print >> sys.stderr, str(self.user)
#        self.who_matches.choices = mk_authz_who_list(self.user)
#        self.what_matches.choices = mk_authz_what_list(self.user)
#        self.how_matches.choices = mk_authz_how_list(self.user)
#        forms.Form.__init__(self, *args, **kwargs)
'''


class AddRoleForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    class Meta:
        model = Role
        fields = ("name",)

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)

    def save(self, commit=True):
        role = super(AddRoleForm, self).save(commit=False)
        if commit:
            role.save()
        return role


class AddObjectForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})
    regex = forms.CharField(label=_("Regular expression"), required=False)

    class Meta:
        model = AcsObject
        fields = ("name", "regex",)

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)

    def save(self, commit=True):
        acs_object = super(AddObjectForm, self).save(commit=False)
        if commit:
            acs_object.save()
        return acs_object


class AddViewForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    class Meta:
        model = View
        fields = ("name",)

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


    def save(self, commit=True):
        view = super(AddViewForm, self).save(commit=False)
        if commit:
            view.save()
        return view


class AddActionForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    class Meta:
        model = Action
        fields = ("name",)

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


    def save(self, commit=True):
        action = super(AddActionForm, self).save(commit=False)
        if commit:
            action.save()
        return action


class AddActivityForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    class Meta:
        model = Activity
        fields = ("name",)

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


    def save(self, commit=True):
        activity = super(AddActivityForm, self).save(commit=False)
        if commit:
            activity.save()
        return activity


class RoleChangeForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    def __init__(self, *args, **kwargs):
        super(RoleChangeForm, self).__init__(*args, **kwargs)
        self.fields["users"].widget = CheckboxSelectMultiple()
        self.fields["users"].help_text = None
        self.fields["roles"].widget = CheckboxSelectMultiple()
        self.fields["roles"].help_text = None

    class Meta:
        model = Role
        fields = ("name", "users", "roles")

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


class ViewChangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ViewChangeForm, self).__init__(*args, **kwargs)
        self.fields["acs_objects"].widget = CheckboxSelectMultiple()
        self.fields["acs_objects"].help_text = None
        self.fields["views"].widget = CheckboxSelectMultiple()
        self.fields["views"].help_text = None

    class Meta:
        model = View
        fields = ("name", "acs_objects", "views")

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


class AdminViewChangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdminViewChangeForm, self).__init__(*args, **kwargs)
        self.fields["acs_objects"].widget = CheckboxSelectMultiple()
        self.fields["acs_objects"].help_text = None
        self.fields["views"].widget = CheckboxSelectMultiple()
        self.fields["views"].help_text = None
        self.fields["users"].widget = CheckboxSelectMultiple()
        self.fields["users"].help_text = None
        self.fields["roles"].widget = CheckboxSelectMultiple()
        self.fields["roles"].help_text = None
        self.fields["actions"].widget = CheckboxSelectMultiple()
        self.fields["actions"].help_text = None
        self.fields["activities"].widget = CheckboxSelectMultiple()
        self.fields["activities"].help_text = None

    class Meta:
        model = View
        fields = ("name", "acs_objects", "views", "users", "roles",
            "actions", "activities")

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)

    def save(self, *args, **kwargs):
        super(AdminViewChangeForm, self).save(*args, **kwargs)
        self.instance.users = self.cleaned_data.get('users')
        if len(args) > 0 and isinstance(args[0], User):
            self.instance.users.add(args[0])
        self.instance.save()


class ActivityChangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ActivityChangeForm, self).__init__(*args, **kwargs)
        self.fields["actions"].widget = CheckboxSelectMultiple()
        self.fields["actions"].help_text = None
        self.fields["activities"].widget = CheckboxSelectMultiple()
        self.fields["activities"].help_text = None

    class Meta:
        model = Activity
        fields = ("name", "actions", "activities")

    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        exclude.remove('namespace') # allow checking against the missing attribute

        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError, e:
            self._update_errors(e.message_dict)


class AddSourceForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    class Meta:
        model = AttributeSource
        fields = ("name",)

    def save(self, commit=True):
        source = super(AddSourceForm, self).save(commit=False)
        if commit:
            source.save()
        return source


class AddLdapSourceForm(forms.ModelForm):
    name = forms.RegexField(label=_("name"),
    max_length=30, regex=r'^[\w.@+-]+$',
    help_text = \
    _("30 characters or fewer. Letters, digits and @/./+/-/_ only."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    server = forms.RegexField(label=_("LDAP host"),
    max_length=100, regex=r'^[\w.@+-]+$',
    help_text = \
    _("Provide a hostname or an IP address."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    user = forms.RegexField(label=_("Username"), required=False,
    max_length=100, regex=r'^[\w.@+-]+$',
    help_text = \
_("Provide a user account if it is necessary to authenticate for binding."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    password = forms.RegexField(label=_("Password"), required=False,
    max_length=100, regex=r'^[\w.@+-]+$',
    help_text = \
_("Provide a user account if it is necessary to authenticate for binding."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    base = forms.RegexField(label=_("Base DN"),
    max_length=100, regex=r'^[\w.@+-,=]+$',
    help_text = \
    _("Provide the base DN for searching, e.g. dc=organization,dc=org."),
    error_messages = \
    {'invalid': _("This value may contain only letters, \
    numbers and @/./+/-/_ characters.")})

    is_auth_backend = forms.BooleanField(required=False,
    help_text = \
    _("Check if this LDAP is also used as an authentication backend"))

    class Meta:
        model = LdapSource
        fields = ("name", "server", "user", "password", "base", "is_auth_backend")

    def save(self, commit=True):
        source = super(AddLdapSourceForm, self).save(commit=False)
        if commit:
            source.save()
        return source
