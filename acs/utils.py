'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import urllib
import httplib
import logging

from django.http import Http404


def get_soap_message(request, on_error_raise = True):
    '''Verify that POST content looks like a SOAP message and returns it'''
    if request.method != 'POST' or \
            request.META['CONTENT_TYPE'] != 'text/xml':
        if on_error_raise:
            raise Http404(_('Only SOAP messages here'))
        else:
            return None
    return request.raw_post_data


class SOAPException(Exception):
    url = None

    def __init__(self, url):
        self.url = url


def soap_call(url, msg, client_cert = None):
    if url.startswith('http://'):
        host, query = urllib.splithost(url[5:])
        conn = httplib.HTTPConnection(host)
    else:
        host, query = urllib.splithost(url[6:])
        conn = httplib.HTTPSConnection(host,
                key_file = client_cert, cert_file = client_cert)
    try:
        conn.request('POST', query, msg, {'Content-Type': 'text/xml'})
        response = conn.getresponse()
    except Exception, err:
        logging.error('SOAP error (on %s): %s' % (url, err))
        raise SOAPException(url)
    try:
        data = response.read()
    except Exception, err:
        logging.error('SOAP error (on %s): %s' % (url, err))
        raise SOAPException(url)
    conn.close()
    if response.status not in (200, 204): # 204 ok for federation termination
        logging.warning('SOAP error (%s) (on %s)' % (response.status, url))
        raise SOAPException(url)
    return data
