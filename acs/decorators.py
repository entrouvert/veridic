'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from functools import wraps
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib import messages

from utils_views import get_policy_from_session

from core import check_acs_initialized, \
    is_administrator, \
    is_user_administrator, \
    is_root_administrator, \
    is_policy_root_administrator, \
    is_policy_user_administrator, \
    is_policy_abac_administrator, \
    is_policy_object_creator, \
    is_policy_action_creator, \
    is_self_admin, get_alias_in_policy

logger = logging.getLogger('acs')

root_url = settings.ROOT_URL


'''
''''''
    Decorators to protect views
''''''
'''


def prevent_access_to_normal_users(view_func):

    def _wrapped_view(request, *args, **kwargs):
        '''Test that ACS is initialized'''
        if not check_acs_initialized():
            messages.add_message(request, messages.ERROR, \
                _('The ACS application is not initialized. \
                Contact your administrator.'))
            return HttpResponseRedirect(root_url)
        '''Check if the user is an ACS admin'''
        if not is_administrator(request.user):
            messages.add_message(request, messages.ERROR, \
                _('You are not an admin user. \
                Log in with a user empowered for administration.'))
            return HttpResponseRedirect(root_url)
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def check_policy_in_session(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not policy:
            messages.add_message(request, messages.ERROR,
                _('No policy selected'))
            return HttpResponseRedirect(root_url)
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def prevent_access_to_not_user_administrators(view_func):

    def _wrapped_view(request, *args, **kwargs):
        if not is_user_administrator(request.user):
            logger.critical('prevent_access_to_not_user_administrators: \
                %s is not a user administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a user administrator. \
                    Log in with a user empowered for user administration.'))
            return HttpResponseRedirect(root_url)
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def prevent_access_to_not_root_administrators(view_func):

    def _wrapped_view(request, *args, **kwargs):
        if not is_root_administrator(request.user):
            logger.critical('prevent_access_to_not_root_administrators: \
                %s is not a root administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a root administrator. \
                    Log in with a user empowered for root administration.'))
            return HttpResponseRedirect(root_url)
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def prevent_access_to_not_policy_root_administrators(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not policy \
                or not is_policy_root_administrator(request.user, policy):
            logger.critical('prevent_access_to_not_root_administrators: \
                %s is not a root administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a root administrator. \
                    Log in with a user empowered for root administration.'))
            return HttpResponseRedirect(root_url)
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def check_authorized_on_users_and_roles(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not is_policy_user_administrator(request.user, policy):
            logger.critical('check_authorized_on_users_and_roles: \
                %s is not able to manage user or roles' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a policy user administrator. \
                Log in with a user empowered for root administration.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def check_authorized_for_abac(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not is_policy_abac_administrator(request.user, policy):
            logger.critical('check_authorized_for_abac: \
                %s is not able to manage ABAC' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a policy abac administrator. \
                Log in with a user empowered for root administration.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def check_authorized_on_objects_and_views(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not is_policy_object_creator(request.user, policy):
            logger.critical('check_authorized_on_objects_and_views: \
                %s is not able to manage objects and views' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not an object or view creator. \
                    Log in with a user empowered for root administration.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def check_authorized_on_actions_and_activities(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not is_policy_action_creator(request.user, policy):
            logger.critical('check_authorized_on_actions_and_activities: \
                %s is not able to manage actions and activities' \
                %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not an actions and activities. \
                Log in with a user empowered for root administration.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def prevent_access_to_not_self_administrators(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not policy:
            logger.critical('prevent_access_to_not_self_administrators: \
                %s is not a self administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a self administrator.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        alias = get_alias_in_policy(request.user, policy)
        if not alias or not is_self_admin(alias):
            logger.critical('prevent_access_to_not_self_administrators: \
                %s is not a self administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a self administrator.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))


def prevent_access_to_not_self_administrators_or_normal_users(view_func):

    def _wrapped_view(request, *args, **kwargs):
        policy = get_policy_from_session(request)
        if not policy:
            logger.critical('prevent_access_to_not_self_administrators: \
                %s is not a self administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a self administrator.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        alias = get_alias_in_policy(request.user, policy)
        if not is_administrator(request.user) and \
                (not alias or not is_self_admin(alias)):
            logger.critical('prevent_access_to_not_self_administrators: \
                %s is not a self administrator' %request.user)
            messages.add_message(request, messages.ERROR, \
                _('You are not a self administrator.'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        return view_func(request, *args, **kwargs)
    return login_required(wraps(view_func)(_wrapped_view))
