'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import re
import random
import string

from cPickle import loads, dumps

from django.db import models

from acs.attribute_aggregator.xacml_constants import *
from acs.attribute_aggregator.models import AttributeSource


class AssertionData:
    def __init__(self, attribute_data=None):
        self.attribute_data = attribute_data

    def get_attribute_data(self):
        return self.attribute_data

    def set_attribute_data(self, attribute_data):
        self.attribute_data = attribute_data

    def __unicode__(self):
        data = self.get_attribute_data()
        return data.__unicode__()


class AssertionDefinition:
    def __init__(self, definition=None, sources=[]):
        self.definition = definition
        self.sources = []
        for source in sources:
            self.add_source(source)

    def add_source(self, source):
        if not source.id in self.sources:
            self.sources.append(source.id)

    def get_definition(self):
        return self.definition

    def remove_source(self, source):
        if source.id in self.sources:
            self.sources.pop(source.id)

    def get_sources(self):
        result = []
        for key in self.sources:
            try:
                source_ = AttributeSource.objects.get(id=key)
                result.append(source_)
            except:
                pass
        return result

    def __unicode__(self):
        s = self.definition
        if self.sources:
            s += ' from '
            for source in self.get_sources():
                s = s + source.name + ', '
            if len(s) > 6:
                s = s[:-2]
            else:
                s = self.definition
        return s


'''
    To say that an attribute is required
'''


class PredicateRequired:
    def __init__(self, assertion_definition=None, single_value=False, sources=[]):
        self.assertion_definition = assertion_definition
        self.single_value = single_value
        self.sources = []
        for source in sources:
            self.add_source(source)

    def get_assertion_definition(self):
        return self.assertion_definition

    def set_assertion_definition(self, assertion_definition=None):
        self.assertion_definition = assertion_definition

    def get_definition(self):
        if self.assertion_definition:
            return self.assertion_definition.definition
        return None

    def get_sources(self):
        definition = self.assertion_definition
        if definition:
            return definition.get_sources()
        return None

    def __unicode__(self):
        if self.single_value:
            return "Predicate required: %s - \
                The attribute must be single-valued" \
                    % str(self.get_definition())
        return "Predicate required: %s" % str(self.get_definition())


class PredicateRole:
    '''
        Role are only handled from the ACS role tree

        Else, use attributes equality with roles provided as attributes from
        sources.
    '''
    def __init__(self, role=None):
        self.role = None
        if role and role.id:
            self.role = role.id

    def get_role(self):
        try:
            from acs.models import Role
            return Role.objects.get(id=self.role)
        except:
            return None

    def __unicode__(self):
        return "Predicate role on %s" % str(self.get_role())


class PredicateComparison:
    def __init__(self, operand1=None, operand2=None,
            operand1_single_value=False, operand2_single_value=False,
            comparison_type=ACS_XACML_COMPARISON_EQUALITY_STRING,
            multivalues='NO_MULTIVALUES',
            multivalues_explanation=None):
        self.operand1 = operand1
        self.operand2 = operand2
        self.operand1_single_value = operand1_single_value
        self.operand2_single_value = operand2_single_value
        self.comparison_type = comparison_type
        self.multivalues = multivalues
        self.multivalues_explanation = multivalues_explanation

    def get_operand1(self):
        return self.operand1

    def set_operand1(self, operand1=None):
        self.operand1 = operand1

    def get_operand2(self):
        return self.operand2

    def set_operand2(self, operand2=None):
        self.operand2 = operand2

    def __unicode__(self):
        operator = ''
        if self.comparison_type in XACML_COMPARISON_EQUALITY:
            operator = '='
        elif self.comparison_type in ACS_XACML_COMPARISON_LT:
            operator = '<'
        elif self.comparison_type in ACS_XACML_COMPARISON_LT_OE:
            operator = '<='
        elif self.comparison_type in ACS_XACML_COMPARISON_GRT:
            operator = '>'
        elif self.comparison_type in ACS_XACML_COMPARISON_GRT_OE:
            operator = '>='
        s = 'Predicate comparison: %s %s %s (' \
            % (self.get_operand1().__unicode__(),
                operator, self.get_operand2().__unicode__())
        if self.operand1_single_value:
            s += 'operand one requires a single-valued attribute - '
        if self.operand2_single_value:
            s += 'operand two requires a single-valued attribute - '
        if not self.operand1_single_value or not self.operand2_single_value:
            if not self.multivalues_explanation:
                s += 'multivalues management is %s' \
                    % self.multivalues
            else:
                s += 'The multivalues management is as follows: %s' \
                    % self.multivalues_explanation
        s += ')'
        return s


'''
    An ABAC rule is a string containing logical statements (and, or, not) and
    the identifiers of predicates.
'''


class AbacRule(models.Model):
    expression = models.CharField(max_length = 2048, null=True, blank=True)
    predicates = models.CharField(max_length = 4096, null=True, blank=True)

    def get_predicates(self):
        if not self.predicates:
            return []
        return loads(str(self.predicates))

    def add_predicate(self, predicate=None):
        if predicate:
            predicates = self.get_predicates()
            predicates.append(predicate)
            self.predicates = dumps(predicates)

    def __unicode__(self):
        count = 1
        expression = self.expression
        for predicate in self.get_predicates():
            expression = re.sub(str(count), predicate.__unicode__(), expression)
            count = count + 1
        return expression
