'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import logging

from django.contrib import messages
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.conf import settings

from attribute_aggregator.models import AttributeSource

from forms import AddRoleForm, AddObjectForm, AddViewForm, AddActionForm, \
    AddActivityForm, RoleChangeForm, \
    ViewChangeForm, ActivityChangeForm, \
    AddSourceForm, AddLdapSourceForm

from core import is_policy_action_creator, is_policy_object_creator, \
    is_policy_user_administrator, is_in_policy, is_valid_regex, \
    is_admin_permission_in_policy, is_authorized_by_names, \
    isAuthorizedRBAC2, is_authorized_with_object_in_regexp, \
    set_default_alias, \
    is_policy_root_administrator, \
    filter_list_in_namespace, \
    return_list_users_authorized_for_admin, \
    return_list_roles_authorized_for_admin, \
    return_list_actions_authorized_for_admin, \
    return_list_objects_authorized_for_admin, \
    return_list_views_authorized_for_admin, \
    return_list_activities_authorized_for_admin, \
    check_permission, is_permission_delegated_by_user, \
    return_all_derived_delegated_permissions, \
    get_alias_in_policy

from models import UserAlias, Role, AcsObject, View, Action, Activity, \
    AcsPermission, Namespace

from decorators import prevent_access_to_normal_users,\
    check_policy_in_session, \
    prevent_access_to_not_user_administrators, \
    check_authorized_on_users_and_roles, \
    check_authorized_on_objects_and_views, \
    check_authorized_on_actions_and_activities, \
    prevent_access_to_not_self_administrators_or_normal_users

from utils_views import get_policy_from_session, \
    get_who_from_one_post_field, \
    get_what_from_one_post_field, \
    get_how_from_one_post_field

from build_policy_display import draw_graph


logger = logging.getLogger('acs')

login_url = settings.LOGIN_URL
root_url = settings.ROOT_URL


'''
''''''
    Policy entities management
''''''
'''


@csrf_exempt
@prevent_access_to_not_user_administrators
def add_abac_source(request):
    title = _('Add a new ABAC source')
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect(root_url)
        form = AddSourceForm(request.POST)
        if form.is_valid():
            source = form.save()
            logger.debug('add_abac_source: Source %s created' %source)
            messages.add_message(request, messages.INFO,
                _('The source %s has been correctly created.') % source)
            return HttpResponseRedirect(root_url)
    else:
        form = AddSourceForm()
    return return_add_any(request, form, title)


@csrf_exempt
@prevent_access_to_not_user_administrators
def add_abac_ldap_source(request):
    title = _('Add a new ABAC LDAP source')
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect(root_url)
        logger.debug('add_abac_ldap_source: request.POST %s ' % request.POST)
        form = AddLdapSourceForm(request.POST)
        logger.debug('add_abac_ldap_source: form %s ' % form)
        if form.is_valid():
            source = form.save()
            Namespace.objects.get_or_create(name=source.name)
            logger.debug('add_abac_ldap_source: Ldap source %s created' \
                % source)
            messages.add_message(request, messages.INFO,
                _('The LDAP source %s has been correctly created.') % source)
            return HttpResponseRedirect(root_url)
    else:
        form = AddLdapSourceForm()
    return return_add_any(request, form, title)


@csrf_exempt
@prevent_access_to_not_user_administrators
def list_abac_sources(request):
    title = _('Modify or delete an attribute source')
    type_entity = 'source'
    objects = AttributeSource.objects.all()
    return return_list_any(request, objects, title, type_entity,
        backlink=root_url, back_url='list_abac_sources')


@csrf_exempt
@prevent_access_to_not_user_administrators
def mod_source(request):
    form = None
    source = None
    if request.method == 'GET':
        if 'source' in request.GET and request.GET['source']:
            try:
                source = AttributeSource.objects.get(id=request.GET['source'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown source'))
                return HttpResponseRedirect('/list_abac_sources')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown source'))
            return HttpResponseRedirect('/list_abac_sources')

        is_ldap = source.get_source_instance()
        if is_ldap:
            form = AddLdapSourceForm(instance=is_ldap)
        else:
            form = AddSourceForm(instance=source)

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_abac_sources')
        if 'id' in request.POST and request.POST['id']:
            try:
                source = AttributeSource.objects.get(id=request.POST['id'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown source'))
                return HttpResponseRedirect('/list_abac_sources')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown source'))
            return HttpResponseRedirect('/list_abac_sources')

        is_ldap = source.get_source_instance()
        if is_ldap:
            form = AddLdapSourceForm(request.POST, instance=is_ldap)
        else:
            form = AddSourceForm(request.POST, instance=source)

        if form.is_valid():
            s = form.save()
            s.save()
            messages.add_message(request, messages.INFO,
                _('Source %s modified') % form.cleaned_data['name'])
        else:
            logger.error('mod_source: Error validating form %s' % form)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_abac_sources')

    title = _('Modify the source %s') % source
    tpl_p = {'item': source, 'backlink': 'list_abac_sources'}
    return return_mod_any(request, form, title, tpl_p=tpl_p,
        template_name='mod_any_new.html')


@csrf_exempt
@prevent_access_to_not_user_administrators
def list_users(request):
    title = _('Modify or delete a user')
    type_entity = 'user'
    users = User.objects.all()
    return return_list_any(request, users, title, type_entity,
        template_name='list_user.html')


@csrf_exempt
@check_policy_in_session
@check_authorized_on_users_and_roles
def add_role(request):
    policy = get_policy_from_session(request)
    if not is_policy_user_administrator(request.user, policy):
            messages.add_message(request, messages.INFO,
                _('You are not authorized to add a role'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        form = AddRoleForm(request.POST)
        form.instance.namespace = policy.namespace
        if form.is_valid():
            what_target = form.save()
            logger.debug('add_role: Role %s created' %what_target)
            policy.admin_view.roles.add(what_target)
            logger.debug('add_role: role added to %s' %policy.admin_view)
            messages.add_message(request, messages.INFO,
                _('Role %s added') %what_target)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    else:
        form = AddRoleForm()
    title = _('Add a new role in %s' %policy)
    return return_add_any(request, form, title)


@csrf_exempt
@check_policy_in_session
@check_authorized_on_objects_and_views
def add_object(request):
    policy = get_policy_from_session(request)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        form = AddObjectForm(request.POST)
        form.instance.namespace = policy.namespace
        if form.is_valid():
            '''if regex, test that it is valid'''
            if form.cleaned_data['regex']:
                logger.debug('add_object: with regexp %s' \
                    % form.cleaned_data['regex'])
                if not is_valid_regex(form.cleaned_data['regex']):
                    messages.add_message(request, messages.ERROR,
                        _('Invalid regular expression. Object not created.'))
                    return HttpResponseRedirect('mod_policy?id=' + \
                        str(policy.id))
            what_target = form.save()
            logger.debug('add_object: Object %s created' %what_target)
            policy.admin_view.acs_objects.add(what_target)
            logger.debug('add_object: Object added to %s' %policy.admin_view)
            messages.add_message(request, messages.INFO,
                _('Object %s added') %what_target)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    else:
        form = AddObjectForm()
    title = _('Add a new object in %s' %policy)
    return return_add_any(request, form, title,
        template_name='add_object.html')


@csrf_exempt
@check_policy_in_session
@check_authorized_on_objects_and_views
def add_view(request):
    policy = get_policy_from_session(request)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        form = AddViewForm(request.POST)
        form.instance.namespace = policy.namespace
        if form.is_valid():
            what_target = form.save()
            logger.debug('add_view: View %s created' %what_target)
            policy.admin_view.views.add(what_target)
            logger.debug('add_view: View added to %s' %policy.admin_view)
            messages.add_message(request, messages.INFO,
                _('View %s added') %what_target)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    else:
        form = AddViewForm()
    title = _('Add a new view in %s' %policy)
    return return_add_any(request, form, title)


@csrf_exempt
@check_policy_in_session
@check_authorized_on_actions_and_activities
def add_action(request):
    policy = get_policy_from_session(request)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        form = AddActionForm(request.POST)
        form.instance.namespace = policy.namespace
        if form.is_valid():
            what_target = form.save()
            logger.debug('add_action: Action %s created' %what_target)
            policy.admin_view.actions.add(what_target)
            logger.debug('add_action: Action added to %s' %policy.admin_view)
            messages.add_message(request, messages.INFO,
                _('Action %s added') %what_target)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    else:
        form = AddActionForm()
    title = _('Add a new action in %s' %policy)
    return return_add_any(request, form, title)


@csrf_exempt
@check_policy_in_session
@check_authorized_on_actions_and_activities
def add_activity(request):
    policy = get_policy_from_session(request)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
        form = AddActivityForm(request.POST)
        form.instance.namespace = policy.namespace
        if form.is_valid():
            what_target = form.save()
            logger.debug('add_activity: Activity %s created' %what_target)
            policy.admin_view.activities.add(what_target)
            logger.debug('add_activity: Activity added to %s' \
                %policy.admin_view)
            messages.add_message(request, messages.INFO,
                _('Activity %s added') %what_target)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    else:
        form = AddActivityForm()
    title = _('Add a new activity in %s' %policy)
    return return_add_any(request, form, title)


def return_add_any(request, form, title, template_name='add_any.html'):
    policy = get_policy_from_session(request)
    tpl_p = {'form': form,
            'title': title, }
    if policy:
        tpl_p['policy'] = policy
    return render_to_response(template_name, tpl_p,
           context_instance=RequestContext(request))


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_roles(request):
    policy = get_policy_from_session(request)
    title = _('Modify or delete a role')
    type_entity = 'role'
    roles = \
    return_list_roles_authorized_for_admin(set_default_alias(request.user))
    roles = filter_list_in_namespace(roles, policy.namespace)
    if is_policy_user_administrator(request.user, policy):
        for r in Role.objects.filter(namespace=policy.namespace):
            if not r in roles:
                roles.append(r)
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, roles, title, type_entity,
        backlink=backlink, back_url='list_roles')


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_objects(request):
    policy = get_policy_from_session(request)
    title = _('Modify or delete an object')
    type_entity = 'object'
    objects = \
    return_list_objects_authorized_for_admin(set_default_alias(request.user))
    objects = filter_list_in_namespace(objects, policy.namespace)
    if is_policy_object_creator(request.user, policy):
        for o in AcsObject.objects.filter(namespace=policy.namespace):
            if not o in objects:
                objects.append(o)
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, objects, title, type_entity,
        backlink=backlink, back_url='list_objects')


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_views(request):
    policy = get_policy_from_session(request)
    title = _('Modify or delete a view')
    type_entity = 'view'
    views = \
    return_list_views_authorized_for_admin(set_default_alias(request.user))
    views = filter_list_in_namespace(views, policy.namespace)
    if is_policy_object_creator(request.user, policy):
        for v in View.objects.filter(namespace=policy.namespace):
            if not v in views:
                views.append(v)
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, views, title, type_entity,
        backlink=backlink, back_url='list_views')


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_actions(request):
    policy = get_policy_from_session(request)
    title = _('Modify or delete an action')
    type_entity = 'action'
    actions = \
    return_list_actions_authorized_for_admin(set_default_alias(request.user))
    actions = filter_list_in_namespace(actions, policy.namespace)
    if is_policy_action_creator(request.user, policy):
        for a in Action.objects.filter(namespace=policy.namespace):
            if not a in actions:
                actions.append(a)
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, actions, title, type_entity,
        backlink=backlink, back_url='list_actions')


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_activities(request):
    policy = get_policy_from_session(request)
    title = _('Modify or delete an activity')
    type_entity = 'activity'
    activities = return_list_activities_authorized_for_admin(
        set_default_alias(request.user))
    activities = filter_list_in_namespace(activities, policy.namespace)
    if is_policy_action_creator(request.user, policy):
        for a in Activity.objects.filter(namespace=policy.namespace):
            if not a in activities:
                activities.append(a)
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, activities, title, type_entity,
        backlink=backlink, back_url='list_activities')


def return_list_any(request, list_any, title, type_entity,
        template_name='list_any.html', backlink=root_url, back_url=root_url):
    policy = get_policy_from_session(request)
    tpl_parameters = {'list_any': list_any,
                    'title': title,
                    'type_entity': type_entity,
                    'back_url': back_url,
                    'backlink': backlink}
    if policy:
        tpl_parameters['policy'] = policy
    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))


def list_any(request, type_entity):
    if 'back_url' in request.POST:
        return HttpResponseRedirect(request.POST['back_url'])
    if type_entity == 'user':
        return list_users(request)
    elif type_entity == 'role':
        return list_roles(request)
    elif type_entity == 'object':
        return list_objects(request)
    elif type_entity == 'view':
        return list_views(request)
    elif type_entity == 'action':
        return list_actions(request)
    elif type_entity == 'activity':
        return list_activities(request)
    elif type_entity == 'source':
        return list_abac_sources(request)
    return HttpResponseRedirect(root_url)


@csrf_exempt
@prevent_access_to_normal_users
@prevent_access_to_not_user_administrators
def mod_user(request):
    logger.info('mod_user')
    form = None
    user = None
    if request.method == 'GET':
        if 'user' in request.GET:
            try:
                user = User.objects.get(username=request.GET['user'])
            except:
                messages.add_message(request, messages.INFO,
                    _('Unknown user'))
                return HttpResponseRedirect('/list_users')
            form = UserChangeForm(instance=user)
        else:
            return HttpResponseRedirect('/list_users')
    if request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_users')
        if not 'username' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Error submitting form'))
            return HttpResponseRedirect('/list_users')

        try:
            user = User.objects.get(username=request.POST['username'])
        except:
            messages.add_message(request, messages.INFO,
                _('Unknown user'))
            return HttpResponseRedirect('/list_users')

        form = UserChangeForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO,
                _('User %s modified') %form.cleaned_data['username'])
    tpl_parameters = {'item': user,
            'backlink': 'list_users',
            'title': _('Modify a user'),
            'form': form}
    return render_to_response('mod_user.html', tpl_parameters,
           context_instance=RequestContext(request))


@csrf_exempt
@check_policy_in_session
@prevent_access_to_normal_users
def mod_role(request):
    policy = get_policy_from_session(request)

    role = None
    form = None

    if request.method == 'GET':
        if 'role' in request.GET and request.GET['role']:
            try:
                role = Role.objects.get(id=request.GET['role'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown role'))
                return HttpResponseRedirect('/list_roles')
            if not check_user_or_role(request, role):
                HttpResponseRedirect('/list_roles')
            form = RoleChangeForm(instance=role)
            form.fields["users"].queryset = \
                UserAlias.objects.filter(namespace=policy.namespace)
            form.fields["roles"].queryset = \
                Role.objects.filter(namespace=policy.namespace)
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown role'))
            return HttpResponseRedirect('/list_roles')

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_roles')

        if 'role' in request.POST and request.POST['role']:
            try:
                role = Role.objects.get(id=request.POST['role'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown role'))
                return HttpResponseRedirect('/list_roles')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown role'))
            return HttpResponseRedirect('/list_roles')

        if not check_user_or_role(request, role):
            HttpResponseRedirect('/list_roles')

        form = RoleChangeForm(request.POST, instance=role)
        form.fields["users"].queryset = \
            UserAlias.objects.filter(namespace=policy.namespace)
        form.fields["roles"].queryset = \
            Role.objects.filter(namespace=policy.namespace)

        if form.is_valid():
            '''Processing users modifications'''
            users_registered = []
            users_new = []
            users_removed = []
            users_added = []
            for user in role.users.all():
                if is_in_policy(user, policy):
                    users_registered.append(user)
            for name, value in request.POST.iterlists():
                if name == 'users':
                    for v in value:
                        user = UserAlias.objects.get(id=v)
                        if is_in_policy(user, policy):
                            users_new.append(user)
            users_removed = list(set(users_registered)-set(users_new))
            users_added = list(set(users_new)-set(users_registered))
            for u in users_removed:
                if not check_user_or_role(request, u):
                    return return_mod_role(request, role, form)
            for u in users_added:
                if not check_user_or_role(request, u):
                    return return_mod_role(request, role, form)
            '''Processing roles modifications'''
            roles_registered = []
            roles_new = []
            roles_removed = []
            roles_added = []
            for r in role.roles.all():
                if is_in_policy(r, policy):
                    roles_registered.append(r)
            for name, value in request.POST.iterlists():
                if name == 'roles':
                    for v in value:
                        if v != role.name:
                            r = Role.objects.get(id=v)
                            if is_in_policy(r, policy):
                                roles_new.append(r)
            roles_removed = list(set(roles_registered)-set(roles_new))
            roles_added = list(set(roles_new)-set(roles_registered))
            for u in roles_removed:
                if not check_user_or_role(request, u):
                    return return_mod_role(request, role, form)
            for u in roles_added:
                if not check_user_or_role(request, u):
                    return return_mod_role(request, role, form)
            '''Modification authorized, save role'''
            form.save()
            messages.add_message(request, messages.INFO,
                _('Role %s modified') %role)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_roles')

    return return_mod_role(request, role, form)


@check_policy_in_session
@prevent_access_to_normal_users
def return_mod_role(request, role, form):
    title = _('Modify a role')
    tpl_p = display_for_mod_role(request, role)
    return return_mod_any(request, form, title, tpl_p, 'mod_role.html')


@check_policy_in_session
@prevent_access_to_normal_users
def check_user_or_role(request, item):
    policy = get_policy_from_session(request)
    administration = Action.objects.get(name='administration')
    if not is_in_policy(item, policy):
        messages.add_message(request, messages.ERROR,
        _('%s is not in %s') %(item, policy))
        return False
    p = isAuthorizedRBAC2(set_default_alias(request.user),
        item, administration)
    if not is_policy_user_administrator(request.user, policy) and not p:
        messages.add_message(request, messages.ERROR,
        _('You are not authorized to modify %s') %item)
        return False
    return True


@check_policy_in_session
@prevent_access_to_normal_users
def display_for_mod_role(request, role=None):
    policy = get_policy_from_session(request)
    tpl_p = {}
    users_to_display = \
    return_list_users_authorized_for_admin(set_default_alias(request.user))
    users_to_display = filter_list_in_namespace(users_to_display,
        policy.namespace)
    if is_policy_user_administrator(request.user, policy):
        for u in UserAlias.objects.filter(namespace=policy.namespace):
            if not u in users_to_display:
                users_to_display.append(u)
    tpl_p['users_to_display'] = []
    for u in users_to_display:
        if u in role.users.all():
            tpl_p['users_to_display'].append((u, 'checked'))
        else:
            tpl_p['users_to_display'].append((u, ''))
    roles_to_display = \
    return_list_roles_authorized_for_admin(set_default_alias(request.user))
    roles_to_display = \
        filter_list_in_namespace(roles_to_display, policy.namespace)
    if is_policy_user_administrator(request.user, policy):
        for r in Role.objects.filter(namespace=policy.namespace):
            if not r in roles_to_display:
                roles_to_display.append(r)
    tpl_p['roles_to_display'] = []
    for r in roles_to_display:
        if r.id != role.id:
            if r in role.roles.all():
                tpl_p['roles_to_display'].append((r, 'checked'))
            else:
                tpl_p['roles_to_display'].append((r, ''))
    tpl_p['role'] = role
    tpl_p['backlink'] = '/list_roles'
    return tpl_p


@csrf_exempt
@check_policy_in_session
@prevent_access_to_normal_users
def mod_object(request):
    form = None
    acs_object = None
    if request.method == 'GET':
        if 'object' in request.GET and request.GET['object']:
            try:
                acs_object = AcsObject.objects.get(id=request.GET['object'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown object'))
                return HttpResponseRedirect('/list_objects')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown object'))
            return HttpResponseRedirect('/list_objects')

        if not check_object_or_view(request, acs_object):
            return HttpResponseRedirect('/list_objects')

        form = AddObjectForm(instance=acs_object)

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_objects')
        if 'id' in request.POST and request.POST['id']:
            try:
                acs_object = AcsObject.objects.get(id=request.POST['id'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown object'))
                return HttpResponseRedirect('/list_objects')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown object'))
            return HttpResponseRedirect('/list_objects')

        if not check_object_or_view(request, acs_object):
            return HttpResponseRedirect('/list_objects')

        form = AddObjectForm(request.POST, instance=acs_object)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO,
                _('Object %s modified') %form.cleaned_data['name'])
        else:
            logger.error('mod_object: Error validating form %s' %form)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_objects')

    title = _('Rename the object %s') %acs_object
    tpl_p={'item': acs_object, 'backlink': 'list_objects'}
    return return_mod_any(request, form, title, tpl_p=tpl_p)


@csrf_exempt
@check_policy_in_session
@prevent_access_to_normal_users
def mod_view(request):
    policy = get_policy_from_session(request)

    view = None
    form = None

    if request.method == 'GET':
        if 'view' in request.GET and request.GET['view']:
            try:
                view = View.objects.get(id=request.GET['view'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown view'))
                return HttpResponseRedirect('/list_views')
            if not check_object_or_view(request, view):
                HttpResponseRedirect('/list_views')
            form = ViewChangeForm(instance=view)
            form.fields["acs_objects"].queryset = \
                AcsObject.objects.filter(namespace=policy.namespace)
            form.fields["views"].queryset = \
                View.objects.filter(namespace=policy.namespace)
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown view'))
            return HttpResponseRedirect('/list_views')

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_views')

        if 'view' in request.POST and request.POST['view']:
            try:
                view = View.objects.get(id=request.POST['view'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown view'))
                return HttpResponseRedirect('/list_views')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown view'))
            return HttpResponseRedirect('/list_views')

        if not check_object_or_view(request, view):
            HttpResponseRedirect('/list_views')

        form = ViewChangeForm(request.POST, instance=view)
        form.fields["acs_objects"].queryset = \
            AcsObject.objects.filter(namespace=policy.namespace)
        form.fields["views"].queryset = \
            View.objects.filter(namespace=policy.namespace)

        if form.is_valid():
            '''Processing objects modifications'''
            objects_registered = []
            objects_new = []
            objects_removed = []
            objects_added = []
            for object in view.acs_objects.all():
                if is_in_policy(object, policy):
                    objects_registered.append(object)
            for name, value in request.POST.iterlists():
                if name == 'acs_objects':
                    for v in value:
                        object = AcsObject.objects.get(id=v)
                        if is_in_policy(object, policy):
                            objects_new.append(object)
            objects_removed = list(set(objects_registered)-set(objects_new))
            objects_added = list(set(objects_new)-set(objects_registered))
            for u in objects_removed:
                if not check_object_or_view(request, u):
                    return return_mod_view(request, view, form)
            for u in objects_added:
                if not check_object_or_view(request, u):
                    return return_mod_view(request, view, form)
            '''Processing views modifications'''
            views_registered = []
            views_new = []
            views_removed = []
            views_added = []
            for r in view.views.all():
                if is_in_policy(r, policy):
                    views_registered.append(r)
            for name, value in request.POST.iterlists():
                if name == 'views':
                    for v in value:
                        if v != view.name:
                            r = View.objects.get(id=v)
                            if is_in_policy(r, policy):
                                views_new.append(r)
            views_removed = list(set(views_registered)-set(views_new))
            views_added = list(set(views_new)-set(views_registered))
            for u in views_removed:
                if not check_object_or_view(request, u):
                    return return_mod_view(request, view, form)
            for u in views_added:
                if not check_object_or_view(request, u):
                    return return_mod_view(request, view, form)
            '''Modification authorized, save view'''
            form.save()
            messages.add_message(request, messages.INFO,
                _('View %s modified') %view)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_views')

    return return_mod_view(request, view, form)


@check_policy_in_session
@prevent_access_to_normal_users
def return_mod_view(request, view, form):
    title = _('Modify a view')
    tpl_p = display_for_mod_view(request, view)
    return return_mod_any(request, form, title, tpl_p, 'mod_view.html')


@check_policy_in_session
@prevent_access_to_normal_users
def check_object_or_view(request, item):
    policy = get_policy_from_session(request)
    administration = Action.objects.get(name='administration')
    if not is_in_policy(item, policy):
        messages.add_message(request, messages.ERROR,
        _('%s is not in %s') %(item, policy))
        return False
    p = isAuthorizedRBAC2(set_default_alias(request.user),
        item, administration)
    if not is_policy_object_creator(request.user, policy) and not p:
        messages.add_message(request, messages.ERROR,
        _('You are not authorized to modify %s') %item)
        return False
    return True


@check_policy_in_session
@prevent_access_to_normal_users
def display_for_mod_view(request, view):
    policy = get_policy_from_session(request)
    tpl_p = {}
    objects_to_display = \
    return_list_objects_authorized_for_admin(set_default_alias(request.user))
    objects_to_display = \
        filter_list_in_namespace(objects_to_display, policy.namespace)
    if is_policy_object_creator(request.user, policy):
        for u in AcsObject.objects.filter(namespace=policy.namespace):
            if not u in objects_to_display:
                objects_to_display.append(u)
    tpl_p['objects_to_display'] = []
    for u in objects_to_display:
        if u in view.acs_objects.all():
            tpl_p['objects_to_display'].append((u, 'checked'))
        else:
            tpl_p['objects_to_display'].append((u, ''))
    views_to_display = \
    return_list_views_authorized_for_admin(set_default_alias(request.user))
    views_to_display = \
        filter_list_in_namespace(views_to_display, policy.namespace)
    if is_policy_object_creator(request.user, policy):
        for r in View.objects.filter(namespace=policy.namespace):
            if not r in views_to_display:
                views_to_display.append(r)
    tpl_p['views_to_display'] = []
    for r in views_to_display:
        if r.id != view.id:
            if r in view.views.all():
                tpl_p['views_to_display'].append((r, 'checked'))
            else:
                tpl_p['views_to_display'].append((r, ''))
    tpl_p['view'] = view
    tpl_p['backlink'] = '/list_views'
    return tpl_p


@csrf_exempt
@check_policy_in_session
@prevent_access_to_normal_users
def mod_action(request):
    form = None
    action = None
    if request.method == 'GET':
        if 'action' in request.GET and request.GET['action']:
            try:
                action = Action.objects.get(id=request.GET['action'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown action'))
                return HttpResponseRedirect('/list_actions')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown action'))
            return HttpResponseRedirect('/list_actions')

        if not check_action_or_activity(request, action):
            return HttpResponseRedirect('/list_actions')

        form = AddActionForm(instance=action)

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_actions')
        if 'id' in request.POST and request.POST['id']:
            try:
                action = Action.objects.get(id=request.POST['id'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown action'))
                return HttpResponseRedirect('/list_actions')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown action'))
            return HttpResponseRedirect('/list_actions')

        if not check_action_or_activity(request, action):
            return HttpResponseRedirect('/list_actions')

        form = AddActionForm(request.POST, instance=action)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO,
                _('Object %s modified') %form.cleaned_data['name'])
        else:
            logger.error('mod_action: Error validating form %s' %form)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_actions')

    title = _('Rename the action %s') %action
    tpl_p={'item': action, 'backlink': 'list_actions'}
    return return_mod_any(request, form, title, tpl_p=tpl_p)


@csrf_exempt
@check_policy_in_session
@prevent_access_to_normal_users
def mod_activity(request):
    policy = get_policy_from_session(request)

    activity = None
    form = None

    if request.method == 'GET':
        if 'activity' in request.GET and request.GET['activity']:
            try:
                activity = Activity.objects.get(id=request.GET['activity'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown activity'))
                return HttpResponseRedirect('/list_activities')
            if not check_action_or_activity(request, activity):
                HttpResponseRedirect('/list_activities')
            form = ActivityChangeForm(instance=activity)
            form.fields["actions"].queryset = \
                Action.objects.filter(namespace=policy.namespace)
            form.fields["activities"].queryset = \
                Activity.objects.filter(namespace=policy.namespace)
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown activity'))
            return HttpResponseRedirect('/list_activities')

    elif request.method == 'POST':
        if 'cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect('/list_activities')

        if 'activity' in request.POST and request.POST['activity']:
            try:
                activity = Activity.objects.get(id=request.POST['activity'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown activity'))
                return HttpResponseRedirect('/list_activities')
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown activity'))
            return HttpResponseRedirect('/list_activities')

        if not check_action_or_activity(request, activity):
            HttpResponseRedirect('/list_activities')

        form = ActivityChangeForm(request.POST, instance=activity)
        form.fields["actions"].queryset = \
            Action.objects.filter(namespace=policy.namespace)
        form.fields["activities"].queryset = \
            Activity.objects.filter(namespace=policy.namespace)

        if form.is_valid():
            '''Processing actions modifications'''
            actions_registered = []
            actions_new = []
            actions_removed = []
            actions_added = []
            for action in activity.actions.all():
                if is_in_policy(action, policy):
                    actions_registered.append(action)
            for name, value in request.POST.iterlists():
                if name == 'actions':
                    for v in value:
                        action = Action.objects.get(id=v)
                        if is_in_policy(action, policy):
                            actions_new.append(action)
            actions_removed = list(set(actions_registered)-set(actions_new))
            actions_added = list(set(actions_new)-set(actions_registered))
            for u in actions_removed:
                if not check_action_or_activity(request, u):
                    return return_mod_activity(request, activity, form)
            for u in actions_added:
                if not check_action_or_activity(request, u):
                    return return_mod_activity(request, activity, form)
            '''Processing activities modifications'''
            activities_registered = []
            activities_new = []
            activities_removed = []
            activities_added = []
            for r in activity.activities.all():
                if is_in_policy(r, policy):
                    activities_registered.append(r)
            for name, value in request.POST.iterlists():
                if name == 'activities':
                    for v in value:
                        if v != activity.name:
                            r = Activity.objects.get(id=v)
                            if is_in_policy(r, policy):
                                activities_new.append(r)
            activities_removed = \
                list(set(activities_registered)-set(activities_new))
            activities_added = \
                list(set(activities_new)-set(activities_registered))
            for u in activities_removed:
                if not check_action_or_activity(request, u):
                    return return_mod_activity(request, activity, form)
            for u in activities_added:
                if not check_action_or_activity(request, u):
                    return return_mod_activity(request, activity, form)
            '''Modification authorized, save activity'''
            form.save()
            messages.add_message(request, messages.INFO,
                _('Activity %s modified') %activity)

    else:
        messages.add_message(request, messages.ERROR,
        _('Unknown HTTP method %s') %request.method)
        return HttpResponseRedirect('/list_activities')

    return return_mod_activity(request, activity, form)


@check_policy_in_session
@prevent_access_to_normal_users
def return_mod_activity(request, activity, form):
    title = _('Modify a activity')
    tpl_p = display_for_mod_activity(request, activity)
    return return_mod_any(request, form, title, tpl_p, 'mod_activity.html')


@check_policy_in_session
@prevent_access_to_normal_users
def check_action_or_activity(request, item):
    policy = get_policy_from_session(request)
    administration = Action.objects.get(name='administration')
    if not is_in_policy(item, policy):
        messages.add_message(request, messages.ERROR,
        _('%s is not in %s') %(item, policy))
        return False
    p = isAuthorizedRBAC2(set_default_alias(request.user),
        item, administration)
    if not is_policy_action_creator(request.user, policy) and not p:
        messages.add_message(request, messages.ERROR,
        _('You are not authorized to modify %s') %item)
        return False
    return True


@check_policy_in_session
@prevent_access_to_normal_users
def display_for_mod_activity(request, activity):
    policy = get_policy_from_session(request)
    tpl_p = {}
    actions_to_display = \
    return_list_actions_authorized_for_admin(set_default_alias(request.user))
    actions_to_display = \
        filter_list_in_namespace(actions_to_display, policy.namespace)
    if is_policy_action_creator(request.user, policy):
        for u in Action.objects.filter(namespace=policy.namespace):
            if not u in actions_to_display:
                actions_to_display.append(u)
    tpl_p['actions_to_display'] = []
    for u in actions_to_display:
        if u in activity.actions.all():
            tpl_p['actions_to_display'].append((u, 'checked'))
        else:
            tpl_p['actions_to_display'].append((u, ''))
    activities_to_display = return_list_activities_authorized_for_admin(
        set_default_alias(request.user))
    activities_to_display = \
        filter_list_in_namespace(activities_to_display, policy.namespace)
    if is_policy_action_creator(request.user, policy):
        for r in Activity.objects.filter(namespace=policy.namespace):
            if not r in activities_to_display:
                activities_to_display.append(r)
    tpl_p['activities_to_display'] = []
    for r in activities_to_display:
        if r.id != activity.id:
            if r in activity.activities.all():
                tpl_p['activities_to_display'].append((r, 'checked'))
            else:
                tpl_p['activities_to_display'].append((r, ''))
    tpl_p['activity'] = activity
    tpl_p['backlink'] = '/list_activities'
    return tpl_p


def return_mod_any(request, form, title, tpl_p={},
                                    template_name='mod_any.html'):
    tpl_parameters = {}
    tpl_parameters['form'] = form
    tpl_parameters['title'] = title
    if tpl_p:
        tpl_parameters = dict(tpl_parameters.items() + tpl_p.items())
    return render_to_response(template_name, tpl_parameters,
           context_instance=RequestContext(request))


'''
''''''
    Permission management views
''''''
'''


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def add_permission_any(request):
    policy = get_policy_from_session(request)
    return render_to_response('select_permission_type.html',
        {'backlink': 'mod_policy?id=' + str(policy.id)},
        context_instance=RequestContext(request))


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def add_permission(request):
    '''
        Add permissions into policies
    '''

    policy = get_policy_from_session(request)
    if request.method == 'POST':
        if not 'who_matches' in request.POST \
                or not request.POST['who_matches']:
            messages.add_message(request, messages.ERROR,
                _('No object for Who provided'))
            return return_add_permission_form(request)
        if not 'what_matches' in request.POST \
                or not request.POST['what_matches']:
            messages.add_message(request, messages.ERROR,
                _('No object for What provided'))
            return return_add_permission_form(request)
        if not 'how_matches' in request.POST \
                or not request.POST['how_matches']:
            messages.add_message(request, messages.ERROR,
                _('No object for How provided'))
            return return_add_permission_form(request)
        try:
            who = get_who_from_one_post_field(request, 'who_matches')
            what = get_what_from_one_post_field(request, 'what_matches')
            how = get_how_from_one_post_field(request, 'how_matches')
        except Exception, err:
            logger.error('add_permission: \
                Fail to find an object due to %s' % err)
            messages.add_message(request, messages.ERROR,
                _('Fail to find an object due to %s') % err)
            return return_add_permission_form(request)
        if not check_user_or_role(request, who) \
                or not check_object_or_view(request, what) \
                or not check_action_or_activity(request, how):
            return return_add_permission_form(request)

        delegable = False
        if 'delegable' in request.POST:
            delegable = True

        p = AcsPermission(who=who, what=what, how=how, delegable=delegable)
        if not p:
            logger.error('add_permission: Fail to create permission')
            messages.add_message(request, messages.ERROR,
                    _('Fail to create permission'))
            return return_add_permission_form(request)
        try:
            p.save()
            logger.info('add_permission: Permission added: %s' % str(p))
            messages.add_message(request, messages.INFO,
                    _('Permission %s added') % str(p))
            return HttpResponseRedirect('add_permission_any')
        except Exception, err:
            logger.error('add_permission: \
                Fail to save permission with error: %s' %err)
            messages.add_message(request, messages.ERROR,
                    _('Fail to save permission with error: %s') %err)
            return HttpResponseRedirect('mod_policy?id=' + str(policy.id))

    return return_add_permission_form(request)


@prevent_access_to_normal_users
@check_policy_in_session
def return_add_permission_form(request, template_name='add_permission.html'):
    tpl_p = {}
    policy = get_policy_from_session(request)

    tpl_p['who_to_display'] = \
        return_list_users_authorized_for_admin(
            set_default_alias(request.user)) + \
        return_list_roles_authorized_for_admin(
            set_default_alias(request.user))
    tpl_p['who_to_display'] = \
        filter_list_in_namespace(tpl_p['who_to_display'], policy.namespace)
    if is_policy_user_administrator(request.user, policy):
        for a in UserAlias.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['who_to_display']:
                tpl_p['who_to_display'].append(a)
        for a in Role.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['who_to_display']:
                tpl_p['who_to_display'].append(a)

    tpl_p['what_to_display'] = \
        return_list_objects_authorized_for_admin(
            set_default_alias(request.user)) + \
        return_list_views_authorized_for_admin(
            set_default_alias(request.user))
    tpl_p['what_to_display'] = \
        filter_list_in_namespace(tpl_p['what_to_display'], policy.namespace)
    if is_policy_object_creator(request.user, policy):
        for a in AcsObject.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['what_to_display']:
                tpl_p['what_to_display'].append(a)
        for a in View.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['what_to_display']:
                tpl_p['what_to_display'].append(a)

    tpl_p['how_to_display'] = \
        return_list_actions_authorized_for_admin(
            set_default_alias(request.user)) + \
        return_list_activities_authorized_for_admin(
            set_default_alias(request.user))
    tpl_p['how_to_display'] = \
        filter_list_in_namespace(tpl_p['how_to_display'], policy.namespace)
    if is_policy_action_creator(request.user, policy):
        for a in Action.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['how_to_display']:
                tpl_p['how_to_display'].append(a)
        for a in Activity.objects.filter(namespace=policy.namespace):
            if not a in tpl_p['how_to_display']:
                tpl_p['how_to_display'].append(a)

    tpl_p['title'] = _('Add a permission in %s') %policy
    tpl_p['backlink'] = 'mod_policy?id=' + str(policy.id)
    return render_to_response(template_name, tpl_p,
           context_instance=RequestContext(request))


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def list_permissions(request):
    '''
        Permissions can only be set on objects all being in the same policy
    '''
    policy = get_policy_from_session(request)
    permissions = AcsPermission.objects.all()
    list_any = []
    for p in permissions:
        if check_permission(request.user, policy, p):
            list_any.append(p)
    title = _('Delete a permission')
    type_entity = 'permission'
    backlink = 'mod_policy?id=' + str(policy.id)
    return return_list_any(request, list_any, title, type_entity,
        template_name='list_permissions.html',
        backlink=backlink, back_url='list_permissions')


@csrf_exempt
@check_policy_in_session
@prevent_access_to_not_self_administrators_or_normal_users
def del_permission(request):
    policy = get_policy_from_session(request)
    administration = Action.objects.get(name='administration')
    back = '/list_permissions'
    if request.method == 'POST':
        p = None
        delegateds = []
        type_p = 'permission'
        if 'type_entity' in request.POST and request.POST['type_entity']:
            if request.POST['type_entity'] == 'admin_permission':
                back = '/list_admin_permissions'
                type_p = 'admin_permission'
        if 'back_url' in request.POST and request.POST['back_url']:
            back = request.POST['back_url']
        if 'Cancel' in request.POST:
            messages.add_message(request, messages.INFO,
                _('Operation canceled'))
            return HttpResponseRedirect(back)
        if 'id' in request.POST and request.POST['id']:
            try:
                p = AcsPermission.objects.get(id=request.POST['id'])
            except:
                messages.add_message(request, messages.ERROR,
                _('Unknown permission'))
                return HttpResponseRedirect(back)
        else:
            messages.add_message(request, messages.ERROR,
            _('Unknown permission'))
            return HttpResponseRedirect(back)

        if type_p == 'admin_permission':
            if not is_policy_root_administrator(request.user, policy):
                messages.add_message(request, messages.ERROR,
                    _('You have not enough rights to perform this deletion'))
                return HttpResponseRedirect(back)
            administration = Action.objects.get(name='administration')
            if not isinstance(p.how, Action) \
                    and p.how.id != administration.id:
                messages.add_message(request, messages.ERROR,
                        _('This is not an administration permission'))
                return HttpResponseRedirect(back)
            if not is_admin_permission_in_policy(p, policy):
                messages.add_message(request, messages.ERROR,
                    _('You have not enough rights to perform this deletion'))
                return HttpResponseRedirect(back)
        else:
            if not check_permission(request.user, policy, p) and \
                    not is_permission_delegated_by_user(\
                        get_alias_in_policy(request.user, policy), p):
                messages.add_message(request, messages.ERROR,
                    _('You have not enough rights to perform this deletion'))
                return HttpResponseRedirect(back)
            delegateds = return_all_derived_delegated_permissions(p)
            if len(delegateds) > 1:
                logger.debug('del_permission: delegated permissions %s' \
                    % delegateds)
            else:
                logger.debug('del_permission: no delegated permissions')
                delegateds = []

        if not 'consent' in request.POST:
            tpl_parameters = {'permission': p, 'delegateds': delegateds,
                'type_entity': type_p, 'back_url': back}
            template_name = 'deletion_permission_confirmation.html'
            return render_to_response(template_name, tpl_parameters,
               context_instance=RequestContext(request))
        if not delegateds:
            logger.debug('del_permission: deletion of %s' % p)
            p.delete()
        else:
            for p in delegateds:
                logger.debug('del_permission: deletion of %s' % p)
                p.delete()
        messages.add_message(request, messages.INFO,
                _('Permission deleted'))
    return HttpResponseRedirect(back)


'''
''''''
    View to request a policy
''''''
'''


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def ask_decision(request):
    if request.method == 'POST':
        logger.debug('ask_decision: POST received %s' %request.POST)

        logger.debug('ask_decision: valid form')
        if not 'who_matches' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No object for Who provided'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')
        if not 'what_matches' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No object for What provided'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')
        if not 'how_matches' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No object for How provided'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')

        try:
            who = get_who_from_one_post_field(request, 'who_matches')
            what = get_what_from_one_post_field(request, 'what_matches')
            how = get_how_from_one_post_field(request, 'how_matches')
        except Exception, err:
            logger.error('ask_decision: \
                Fail to find an object with error %s' %err)
            messages.add_message(request, messages.ERROR, err)
            return return_add_permission_form(request,
                template_name='ask_decision.html')

        policy = get_policy_from_session(request)

        r = 0
        if isinstance(who, UserAlias) and isinstance(what, AcsObject) and \
                isinstance(how, Action):
            r = is_authorized_by_names(request.user.username,
                who.alias, what.name, how.name,
                policy.namespace.name)
        elif isinstance(who, Role) and isinstance(what, AcsObject) and \
                isinstance(how, Action):
            r = is_authorized_by_names(request.user.username,
                who.name, what.name, how.name,
                policy.namespace.name,
                role=True)
        elif isinstance(who, UserAlias) and isinstance(what, View) and \
                isinstance(how, Action):
            r = is_authorized_by_names(request.user.username,
                who.alias, what.name, how.name,
                policy.namespace.name,
                view=True)
        elif isinstance(who, Role) and isinstance(what, View) and \
                isinstance(how, Action):
            r = is_authorized_by_names(request.user.username,
                who.name, what.name, how.name,
                policy.namespace.name,
                role=True, view=True)
        elif isinstance(who, UserAlias) and isinstance(what, AcsObject) and \
                isinstance(how, Activity):
            r = is_authorized_by_names(request.user.username,
                who.alias, what.name, how.name,
                policy.namespace.name,
                activity=True)
        elif isinstance(who, Role) and isinstance(what, AcsObject) and \
                isinstance(how, Activity):
            r = is_authorized_by_names(request.user.username,
                who.name, what.name, how.name,
                policy.namespace.name,
                role=True, activity=True)
        elif isinstance(who, UserAlias) and isinstance(what, View) and \
                isinstance(how, Activity):
            r = is_authorized_by_names(request.user.username,
                who.alias, what.name, how.name,
                policy.namespace.name,
                view=True, activity=True)
        elif isinstance(who, Role) and isinstance(what, View) and \
                isinstance(how, Activity):
            r = is_authorized_by_names(request.user.username,
                who.name, what.name, how.name,
                policy.namespace.name,
                role=True, view=True, activity=True)

        logger.debug('ask_decision: result is %s' % str(r))

        if r == -5:
            messages.add_message(request, messages.ERROR,
                _('Result: error processing request'))
        if r == -7:
            messages.add_message(request, messages.ERROR,
            _('You are not authorized to request on %s') %who)
        if r == -8:
            messages.add_message(request, messages.ERROR,
            _('You are not authorized to request on %s') %what)
        if r == -9:
            messages.add_message(request, messages.ERROR,
            _('You are not authorized to request on %s') %how)

        if r == 1:
            messages.add_message(request, messages.INFO,
                _('Result: %s is authorized on %s to perform %s')
                %(who, what, how))
        if r == 0:
            messages.add_message(request, messages.INFO,
                _('Result: %s is not authorized') %who)

    return return_add_permission_form(request,
        template_name='ask_decision.html')


@csrf_exempt
@prevent_access_to_normal_users
@check_policy_in_session
def ask_decision_regex(request):
    if request.method == 'POST':
        logger.debug('ask_decision_regex: POST received %s' % request.POST)

        logger.debug('ask_decision_regex: valid form')
        if not 'who_matches' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No object for Who provided'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')
        if not 'what' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No string provided for what'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')
        if not 'how_matches' in request.POST:
            messages.add_message(request, messages.ERROR,
                _('No object for How provided'))
            return return_add_permission_form(request,
                template_name='ask_decision.html')

        try:
            who = get_who_from_one_post_field(request, 'who_matches')
            how = get_how_from_one_post_field(request, 'how_matches')
        except Exception, err:
            logger.error('ask_decision_regex: \
                Fail to find an object with error %s' % err)
            messages.add_message(request, messages.ERROR, err)
            return return_add_permission_form(request,
                template_name='ask_decision.html')

        what = request.POST['what']
        logger.error('ask_decision_regex: \
            what is %s' % what)

        policy = get_policy_from_session(request)

        r = 0
        if isinstance(who, UserAlias) and isinstance(how, Action):
            r = is_authorized_with_object_in_regexp(request.user.username,
                who.alias, what, how.name,
                policy.namespace.name)
        elif isinstance(who, Role) and isinstance(how, Action):
            r = is_authorized_with_object_in_regexp(request.user.username,
                who.name, what, how.name,
                policy.namespace.name,
                role=True)
        elif isinstance(who, UserAlias) and isinstance(how, Activity):
            r = is_authorized_with_object_in_regexp(request.user.username,
                who.alias, what, how.name,
                policy.namespace.name,
                activity=True)
        elif isinstance(who, Role) and isinstance(how, Activity):
            r = is_authorized_with_object_in_regexp(request.user.username,
                who.name, what, how.name,
                policy.namespace.name,
                role=True, activity=True)

        logger.debug('ask_decision_regex: result is %s' % str(r))

        if r == -5:
            messages.add_message(request, messages.ERROR,
                _('Result: error processing request'))
        if r == -7:
            messages.add_message(request, messages.ERROR,
            _('You are not authorized to request on %s') %who)
        if r == -9:
            messages.add_message(request, messages.ERROR,
            _('You are not authorized to request on %s') %how)

        if r == -2:
            messages.add_message(request, messages.ERROR,
            _('No matching object with %s') %what)

        if r == 1:
            messages.add_message(request, messages.INFO,
                _('Result: %s is authorized on %s to perform %s')
                %(who, what, how))
        if r == 0:
            messages.add_message(request, messages.INFO,
                _('Result: %s is not authorized') %who)

    return return_add_permission_form(request,
        template_name='ask_decision_regex.html')


'''
''''''
    Display of policies with graphs
''''''
'''


@login_required
def graph(request):
    policy = get_policy_from_session(request)
    if 'type_graph' in request.GET \
        and request.GET['type_graph'] == 'whole_policy':
        r = False
        try:
            r = draw_graph(request, "acs/media/images/graph.png",
                type_graph='whole_policy', policy=policy)
        except Exception, err:
            messages.add_message(request, messages.ERROR,
                    _('Function not supported due to: %s') %err)
            return HttpResponseRedirect(root_url)
        if not r:
            messages.add_message(request, messages.ERROR,
                    _('Unable to draw graph'))
            if policy:
                return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
            return HttpResponseRedirect(root_url)
        return render_to_response('graph.html',
               {'graphs': ['graph.png'],
               'backlink': 'mod_policy?id=' + str(policy.id)},
               context_instance=RequestContext(request))
    messages.add_message(request, messages.ERROR,
            _('Unknown graph display'))
    if policy:
        return HttpResponseRedirect('mod_policy?id=' + str(policy.id))
    return HttpResponseRedirect(root_url)
