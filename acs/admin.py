'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from django.contrib import admin

from models import Role, Action, Activity, View, AcsObject, AcsPermission, \
    Namespace


class RoleAdmin(admin.ModelAdmin):
    model = Role
    fieldsets = (
            (None, {'fields': (
                    'name',
                    'namespace',
                    'users',
                    'roles', )}), )
    filter_horizontal = ('users', 'roles', )


class ActionAdmin(admin.ModelAdmin):
    fieldsets = (
            (None, {
                'fields': (
                    'name',
                    'namespace', )}), )


class ActivityAdmin(admin.ModelAdmin):
    fieldsets = (
            (None, {
                'fields': (
                    'name',
                    'namespace',
                    'actions',
                    'activities', )}), )
    filter_horizontal = ('actions', 'activities', )


class AcsObjectAdmin(admin.ModelAdmin):
    fieldsets = (
            (None, {
                'fields': (
                    'name',
                    'namespace', )}), )


class ViewAdmin(admin.ModelAdmin):
    fieldsets = (
            (None, {
                'fields': (
                    'name',
                    'namespace',
                    'acs_objects',
                    'views',
                    'users',
                    'roles',
                    'actions',
                    'activities', )}), )
    filter_horizontal = \
    ('acs_objects', 'views', 'users', 'roles', 'actions', 'activities', )


admin.site.register(Role, RoleAdmin)
admin.site.register(Action, ActionAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(AcsObject, AcsObjectAdmin)
admin.site.register(View, ViewAdmin)
admin.site.register(AcsPermission)
admin.site.register(Namespace)
