'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import settings
from django.conf.urls.defaults import url, include, patterns
from django.contrib import admin

from acs.forms import AuthenticRegistrationForm

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'acs.main_views.index'),

    url(r'^accounts/register',
       'registration.views.register',
       {'form_class': AuthenticRegistrationForm},
       name='registration_register',
       ),
    (r'^accounts/', include('registration.urls')),


    url(r'^login$', 'acs.main_views.login'),
    url(r'^logout$', 'acs.main_views.logout', name='auth_logout'),


    url(r'^ask_decision$', 'acs.views.ask_decision'),
    url(r'^ask_decision_regex$', 'acs.views.ask_decision_regex'),

    url(r'^graph$', 'acs.views.graph'),


    url(r'^create_policy_view$',
        'acs.main_views.create_policy_view'),
    url(r'^delete_policy_view$',
        'acs.main_views.delete_policy_view'),
    url(r'^mod_policy$', 'acs.main_views.mod_policy'),


    url(r'^synchronize_users_in_policy_view$',
        'acs.alias_mgmt_views.synchronize_users_in_policy_view'),
    url(r'^all_users_self_admin$',
        'acs.alias_mgmt_views.all_users_self_admin'),
    url(r'^list_users_for_aliases$',
        'acs.alias_mgmt_views.list_users_for_aliases'),
    url(r'^list_aliases$',
        'acs.alias_mgmt_views.list_aliases'),
    url(r'^switch_self_admin$',
        'acs.alias_mgmt_views.switch_self_admin'),
    url(r'^set_user_in_policy_from_home$',
        'acs.alias_mgmt_views.set_user_in_policy_from_home'),
    url(r'^add_alias_only$',
        'acs.alias_mgmt_views.add_alias_only'),
    url(r'^list_aliases_in_policy$',
        'acs.alias_mgmt_views.list_aliases_in_policy'),
    url(r'^set_user_in_policy$',
        'acs.alias_mgmt_views.set_user_in_policy'),
    url(r'^set_user_in_source$',
        'acs.alias_mgmt_views.set_user_in_source'),


    url(r'^list_accesses$',
        'acs.delegation_views.list_accesses'),
    url(r'^list_delegated_permissions$',
        'acs.delegation_views.list_delegated_permissions'),
    url(r'^delegate_access$',
        'acs.delegation_views.delegate_access'),


    url(r'^add_user$',
        'acs.user_mgmt_views.add_user'),
    url(r'^manage_root_administrators$',
        'acs.user_mgmt_views.manage_root_administrators'),
    url(r'^manage_user_administrators$',
        'acs.user_mgmt_views.manage_user_administrators'),
    url(r'^manage_abac_administrators$',
        'acs.user_mgmt_views.manage_abac_administrators'),
    url(r'^manage_policy_administrators$',
        'acs.user_mgmt_views.manage_policy_administrators'),


    url(r'^list_users$',
        'acs.views.list_users'),

    url(r'^add_role$', 'acs.views.add_role'),
    url(r'^add_object$', 'acs.views.add_object'),
    url(r'^add_view$', 'acs.views.add_view'),
    url(r'^add_action$', 'acs.views.add_action'),
    url(r'^add_activity$', 'acs.views.add_activity'),
    url(r'^add_permission$', 'acs.views.add_permission'),
    url(r'^add_permission_any$', 'acs.views.add_permission_any'),

    url(r'^list_roles$', 'acs.views.list_roles'),
    url(r'^list_objects$', 'acs.views.list_objects'),
    url(r'^list_views$', 'acs.views.list_views'),
    url(r'^list_actions$', 'acs.views.list_actions'),
    url(r'^list_activities$', 'acs.views.list_activities'),
    url(r'^list_permissions$', 'acs.views.list_permissions'),

    url(r'^mod_user$', 'acs.views.mod_user'),
    url(r'^mod_role$', 'acs.views.mod_role'),
    url(r'^mod_object$', 'acs.views.mod_object'),
    url(r'^mod_view$', 'acs.views.mod_view'),
    url(r'^mod_action$', 'acs.views.mod_action'),
    url(r'^mod_activity$', 'acs.views.mod_activity'),

    url(r'^del_any$', 'acs.deletion_view.del_any'),
    url(r'^del_permission$', 'acs.views.del_permission'),

    url(r'^add_abac_permission$', 'acs.abac_views.add_abac_permission'),
    url(r'^list_abac_permissions$', 'acs.abac_views.list_abac_permissions'),
    url(r'^del_abac_permission$', 'acs.abac_views.del_abac_permission'),
    url(r'^add_abac_source$', 'acs.views.add_abac_source'),
    url(r'^add_abac_ldap_source$', 'acs.views.add_abac_ldap_source'),
    url(r'^list_abac_sources$', 'acs.views.list_abac_sources'),
    url(r'^mod_source$', 'acs.views.mod_source'),

    url(r'^add_admin_view$',
        'acs.acs_administration_views.add_admin_view'),
    url(r'^list_admin_views$',
        'acs.acs_administration_views.list_admin_views'),
    url(r'^add_admin_role$',
        'acs.acs_administration_views.add_admin_role'),
    url(r'^list_admin_roles$',
        'acs.acs_administration_views.list_admin_roles'),
    url(r'^add_admin_permission$',
        'acs.acs_administration_views.add_admin_permission'),
    url(r'^list_admin_permissions$',
        'acs.acs_administration_views.list_admin_permissions'),
    url(r'^mod_admin_view$',
        'acs.acs_administration_views.mod_admin_view'),
    url(r'^mod_admin_role$',
        'acs.acs_administration_views.mod_admin_role'),


#    url(r'^delegate_permissions$', 'acs.views.delegate_permissions'),
#    url(r'^remove_delegations$', 'acs.views.remove_delegations'),

    url(r'^incoming$', 'acs.net_interface.incoming'),
    #url(r'^outcoming$', 'acs.net_interface.outcoming'),

    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
)

if settings.STATIC_SERVE:
    urlpatterns += patterns('',
        url(
            regex = r'^media/(?P<path>.*)$',
            view = 'django.views.static.serve',
            kwargs = {'document_root': settings.MEDIA_ROOT}),
    )
