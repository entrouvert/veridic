'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import getpass

from optparse import make_option
from django.db import transaction
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from acs.models import View, AcsPermission, Namespace, Action, \
        UserAlias, Role


class Command(BaseCommand):
    '''
        Initialization script:
            - Initialize Acs.
            - Create a root user with all rights.

        Run with:
            python manage.py initialize-acs <username> [--existing]

            --existing
                The user already exists in the database.
                Else, the user is created.
        Reinitialization:
            python manage.py sqlclear acs | python manage.py dbshell \
            && python manage.py syncdb \
            && python manage.py initialize-acs <username> [--existing]

        If you want to clear all the attribute definitions use
            python manage.py sqlclear abac

        WARNING: The reinitialization, or re-running the initialization
        script, assume that you want to reset ACS and you will loose all
        policies.

        Objects with their names hardly encoded in the script and created in
        the database:

            Namespace: 'Default',
            Action: 'administration',
            Roles: 'root_admin_role', 'root_user_administrator_role',
                'root_abac_administrator_role'
            View: 'root_admin_view'
    '''

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    option_list = BaseCommand.option_list + (
        make_option('--existing',
            action='store_true',
            dest='existing',
            default=False,
            help='Indicate if it is an existing user'),
        )
    args = '<username>'
    help = \
        'Initialize the application with a root user, a new one or existing.'

    @transaction.commit_manually
    def handle(self, *args, **options):

        try:
            if not args:
                raise CommandError('No username on the command line')

            username = args[0]
            user = None

            if options['existing']:
                print 'Look for the existing user %s' %username
                try:
                    user = User.objects.get(username=username)
                    print 'User found: %s' %user
                except:
                    raise CommandError('Unable to get existing user %s' \
                        %username)
            else:
                print 'Creation of user %s' %username
                try:
                    user = User.objects.get(username=username)
                except:
                    pass
                if user:
                    raise CommandError('Already existing user %s' %user)

                MAX_TRIES = 3
                count = 0
                p1, p2 = 1, 2
                while p1 != p2 and count < MAX_TRIES:
                    p1 = getpass.getpass(prompt="Password: ")
                    if not p1:
                        raise CommandError("aborted")
                    p2 = getpass.getpass(prompt="Password (again): ")
                    if not p2:
                        raise CommandError("aborted")
                    if p1 != p2:
                        print "Passwords do not match. Please try again."
                        count = count + 1

                if count == MAX_TRIES:
                    raise CommandError("Aborting creation of user '%s' after \
                        %s attempts" % (username, count))

                try:
                    user = User(username=username)
                    user.set_password(p1)
                    user.save()
                    print 'User created: %s' %user
                except:
                    raise CommandError('Unable to create user %s' %username)

            ns = None
            try:
                ns = Namespace(name='Default')
                ns.save()
                print 'Default namespace created: %s' %ns
            except:
                raise CommandError('Unable to create the default namespace')


            alias = None
            try:
                '''The alias in Default has the username as name'''
                alias = UserAlias(alias=user.username,
                    user=user, namespace=ns)
                alias.save()
                print 'Default alias created: %s' %alias
            except:
                raise CommandError('Unable to create the default alias')

            #view_name = user.username+'_admin_view'
            view_name = 'root_admin_view'
            view = None
            try:
                view = View(name=view_name, namespace=ns)
                view.save()
                print 'Root system view created: %s' %view
            except:
                raise CommandError('Unable to create root system view')

            view.users.add(alias)
            print \
                'Default alias of the root user added to its root system view'

            #role_name = user.username+'_admin_role'
            role_name = 'root_admin_role'
            role=None
            try:
                role = Role(name=role_name, namespace=ns)
                role.save()
                print 'Root system role created: %s' %role
            except:
                raise CommandError('Unable to create root system role')

            view.roles.add(role)
            print 'Default role added to the root system view'

            a = None
            try:
                a = Action(name='administration')
                a.save()
                print 'Administration action created: %s' %a
            except:
                raise \
                    CommandError('Unable to create the administration action')

            p = None
            try:
                p = AcsPermission(who=role, what=view, how=a)
                p.save()
                print 'Root administration permission created: %s' %p
            except:
                raise \
                    CommandError('Unable to create the root administration \
                    permission for the root administration role')

            role.users.add(alias)
            print 'User added to the root role'

            ua = None
            try:
                ua = Role(name='root_user_administrator_role')
                ua.save()
                print 'Special role user administrator created: %s' %ua
            except:
                raise \
                    CommandError('Unable to create special \
                    role user administrator')

            ua = None
            try:
                ua = Role(name='root_abac_administrator_role')
                ua.save()
                print 'Special role abac administrator created: %s' %ua
            except:
                raise \
                    CommandError('Unable to create special \
                    role abac administrator')

        except:
            transaction.rollback()
            raise
        else:
            transaction.commit()
            print '---> Successful main initialization'
