'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import datetime
import time

from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from acs import settings

from acs.core import is_authorized_by_names_with_abac

class Command(BaseCommand):
    '''
        Script to make tests on ABAC
    '''

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    option_list = BaseCommand.option_list + (
        make_option('--view',
            action='store_true',
            dest='view',
            default=False,
            help='Indicate if what is a view'),
        ) + (
        make_option('--activity',
            action='store_true',
            dest='activity',
            default=False,
            help='Indicate if how is an activity'),
        ) + (
        make_option('--no-rule-returned',
            action='store_true',
            dest='no-rule-returned',
            default=False,
            help="Indicate if you don't want a new rule returned if the access is denied"),
        ) + (
        make_option('--no-attribute-signal',
            action='store_true',
            dest='no-attribute-signal',
            default=False,
            help="Indicate if you don't want to send the signal to grab attributes"),
        ) + (
        make_option('--no-print',
            action='store_true',
            dest='no-print',
            default=False,
            help="Indicate if you want to return authorization function return values not formatted"),
        )
    args = '<requestor who what how namespace>'
    help = \
        'No help.'

    @transaction.commit_manually
    def handle(self, *args, **options):

        print '-------- ABAC Tests --------'

        try:

            if not args:
                raise CommandError('Missing arguments')
            try:
                REQUESTOR_NAME, WHO_NAME, WHAT_NAME, HOW_NAME, NAMESPACE_NAME = args
            except:
                raise CommandError('Missing arguments')

            if REQUESTOR_NAME == 'None':
                REQUESTOR_NAME = None
            if REQUESTOR_NAME == 'None':
                WHO_NAME = None
            VIEW = False
            if options['view']:
                VIEW = True
            ACTIVITY = False
            if options['activity']:
                ACTIVITY = True
            '''Not by command line'''
            REQUEST = None
            '''Will support json file in the future'''
            ATTRIBUTES = None
            NO_RULE_RETURNED = False
            if options['no-rule-returned']:
                NO_RULE_RETURNED = True
            NO_ATTRIBUTE_SIGNAL = False
            if options['no-attribute-signal']:
                NO_ATTRIBUTE_SIGNAL = True

            decision, msg, error = \
                is_authorized_by_names_with_abac(\
                    requestor_name=REQUESTOR_NAME,
                    who_name=WHO_NAME,
                    what_name=WHAT_NAME,
                    how_name=HOW_NAME,
                    namespace_name=NAMESPACE_NAME,
                    view=VIEW,
                    activity=ACTIVITY,
                    request=REQUEST,
                    attributes=ATTRIBUTES,
                    no_rule_returned=NO_RULE_RETURNED,
                    no_attribute_signal=NO_ATTRIBUTE_SIGNAL)
            if options['no-print']:
                transaction.rollback()
                return (decision, msg, error)
            if error < 0:
                raise CommandError('is_authorized_by_names_with_abac returned %s' % str(error))
            if decision:
                print "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                print "Access granted by permission %s" % msg
                print "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            else:
                print "------------------------------------------------------"
                if msg:
                    print "Access denied, new rule to satisfy %s" % msg
                else:
                    print "Access denied"
                print "------------------------------------------------------"

        except Exception, err:
            print "Exception: %s" %str(err)
            transaction.rollback()
        else:
            print "Happy end"
            transaction.rollback()

        print '\n-------- DONE --------'
