'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import time
import string
import random

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from acs.models import Role, UserAlias, AcsObject, Action

from acs.core import create_policy, remove_policy, \
    add_role, mod_role, add_object, add_action, add_permission, \
    isAuthorizedRBAC2, isAuthorizedRBAC0


class Command(BaseCommand):
    '''
        Script to make tests on perfs with a hierarchy of roles
    '''

    '''
        A role tree is built to evaluate perf to browse the tree
        The tree of depth n have nodes where each node has n sons
        The number of roles is then (SUM_j=0^n) n^j

        The first thing we test is the time to obtain the decision
        corresponding to a longest tree path, i.e. the user has the root role
        and the permission needed is set on the last node.

        We then test the worst case for a denied request, i.e. returning a
        false decision:
            - the user has the root role, i.e. the user has all the
                permissions
            - the object and action exist
            - no permission exist on them
        All the permissions are tested browsing the whole role tree.

        The results of the two previous tests should be quite similar to have
        an efficient system, i.e. denied request is quite close to the
        permission given on the longest tree path.

        The third test is about giving an average time for a positive
        response.
            - The role tree is perfectly balanced.
            - A permission is set on each role with a different object and
                action.
            - Then user tested has the root role.
        Then we test the response time for obtaining the positive response
        corresponding to the permissions set on each node.
        Finnaly, we compute the average time.

        This is the worst case to compute the average time. The best case
        would be if we had computed the response time when the user is given
        the role on which each permission is set.

        The last test use a more optimistic way to compute this average time.
            - Each role has a user.
            - So we compute the previous average time for each user.
        The first user has the highest average time (it is the previous test)
        Then each following user obtain positive answers with an higher tree
        depth.
        The last user has only the last role which thus gives the smallest
        average time.
        We do not take in account negative request answer.
    '''

    can_import_django_settings = True
    output_transaction = True
    requires_model_validation = True
    option_list = BaseCommand.option_list
    args = '<username>'
    help = \
        'Execute giving the username of a root administrator.'

    def handle(self, *args, **options):

        if not args:
            raise CommandError('No username on the command line')

        root_user = args[0]

        '''
            nb_sons gives the tree depth and the number of sons each role has
            nb_users is equal to the number of roles
            A user is created and added to each role to make tests
        '''
        nb_sons = 3
        nb_users = 40 # = 1 + 3 + 9 + 27
        #-----------------------
        #Test 1:
        #Found in : 0.0790441036224
        #Req/s : 12.6511650354
        #-----------------------
        #Test 2:
        #Search the whole role tree in : 0.183360099792
        #Req/s : 5.45374921333
        #-----------------------
        #Test 2':
        #Search the whole role tree in : 0.0899970531464
        #Req/s : 11.1114749321
        #-----------------------
        #Test 3:
        #Average time is : 0.0418611168861
        #Req/s : 23.8885169433
        #-----------------------
        #Test 4:
        #Average time is : 0.00645741333182
        #Req/s : 154.86076988
        #-----------------------

#        nb_sons = 4
#        nb_users = 341 # = 1 + 4 + 16 + 64 + 256
        #-----------------------
        #Test 1:
        #Found in : 0.617546081543
        #Req/s : 1.61931235561
        #-----------------------
        #Test 2:
        #Search the whole role tree in : 1.574010849
        #Req/s : 0.635319636225
        #-----------------------
        #Test 2':
        #Search the whole role tree in : 0.821752786636
        #Req/s : 1.2169109935
        #-----------------------
        #Test 3:
        #Average time is : 0.337720179488
        #Req/s : 2.96103123455
        #-----------------------
        #Test 4:
        #Average time is : 0.00784453084468
        #Req/s : 127.477349481
        #-----------------------

        '''
            Grab a random string
        '''
        rdm_str = ''.join(random.choice(string.ascii_uppercase + \
            string.digits) for x in range(8))
        print 'All objects of this tests begin with the string %s' % rdm_str

        print '-------- BEGINNING --------'
        print '--> Set some users'
        users = []
        i = 0
        j = 0
        while i < nb_users:
            username = rdm_str + '_user_' + str(i)
            user, created = User.objects.get_or_create(username=username)
            if not user:
                raise CommandError('Error with user %s' %user)
            if created:
                j = j +1
            sys.stdout.write('.')
            sys.stdout.flush()
            user.set_password(username)
            user.save()
            users.append(user)
            i = i + 1
            if not i % 10:
                sys.stdout.write(str(i))
                sys.stdout.flush()
        sys.stdout.write('\n')
        sys.stdout.flush()
        print 'Users created: %s' % str(j)
        print 'already existing users: %s' % str(i-j)
        print '<--\n'

        requester = User.objects.get(username=root_user)

        print '--> Create one policy'
        name = rdm_str + '_policy'
        namespace = name + '_namespace'
        policy, created = create_policy(name, requester,
                    namespace=namespace)
        print '<--\n'

        print '--> Set users in policy'
        for u in users:
            alias, created = UserAlias.objects.get_or_create(alias=u.username,
                namespace=policy.namespace)
            sys.stdout.write('.')
            sys.stdout.flush()
        print '<--\n'

        '''
            Look at with a hierarchy
            * To obtain a negative answer
            * Last object
            * Average time
        '''

        print '--> Create role hierarchy'
        role_name = rdm_str + '_role_' + policy.name + '_' + str(0)
        role = add_role(requester, role_name, policy)
        if not role:
            raise CommandError('Unable to handle a role due to %s' \
                % str(role))

        # Object
        object_name = \
            rdm_str + '_object_' + policy.name + '_' + str(0)
        new_object = add_object(requester, object_name, policy)
        if not new_object:
            raise CommandError(\
                'Unable to handle an object due to %s' \
                % str(new_object))

        #Action
        action_name = \
            rdm_str + '_action_' + policy.name + '_' + str(0)
        new_action = add_action(requester, action_name, policy)
        if not new_action:
            raise CommandError(\
                'Unable to handle an action due to %s' \
                % str(new_action))

        #Permission
        permission = add_permission(requester, policy, role,
            new_object, new_action, delegable=False)
        if not permission:
            raise CommandError(\
                'Unable to handle a permission due to %s' \
                % str(permission))

        #Add user to role
        u = []
        u.append(\
            UserAlias.objects.get(\
                alias=rdm_str + '_user_' + str(0),
                namespace=policy.namespace))
        r = mod_role(requester, role, policy,
        users_added=u, roles_added=[])
        if r < 0:
            raise CommandError(\
                'Unable to handle a role due to %s' % str(r))

        j=0
        total_roles = 1
        mem = 0
        while j < nb_sons:
            existing_roles = Role.objects.filter(namespace=policy.namespace)
            limit = mem
            mem = total_roles
            for role in list(existing_roles)[(limit):]:
                sys.stdout.write('.')
                sys.stdout.flush()
                k=0
                new_roles = []
                while k < nb_sons:
                    #Role
                    role_name = \
                        rdm_str + '_role_' + \
                            policy.name + '_' + str(total_roles)
                    new_role = add_role(requester, role_name, policy)
                    if not new_role:
                        raise CommandError(\
                            'Unable to handle a role due to %s' \
                            % str(new_role))

                    # Object
                    object_name = \
                        rdm_str + '_object_' + \
                            policy.name + '_' + str(total_roles)
                    new_object = add_object(requester, object_name, policy)
                    if not new_object:
                        raise CommandError(\
                            'Unable to handle an object due to %s' \
                            % str(new_object))

                    #Action
                    action_name = \
                        rdm_str + '_action_' + \
                            policy.name + '_' + str(total_roles)
                    new_action = add_action(requester, action_name, policy)
                    if not new_action:
                        raise CommandError(\
                            'Unable to handle an action due to %s' \
                            % str(new_action))

                    #Permission
                    permission = add_permission(requester, policy, new_role,
                        new_object, new_action, delegable=False)
                    if not permission:
                        raise CommandError(\
                            'Unable to handle a permission due to %s' \
                            % str(permission))

                    #Add user to role
                    users = []
                    users.append(\
                        UserAlias.objects.get(\
                            alias=rdm_str + '_user_' + str(total_roles),
                            namespace=policy.namespace))
                    r = mod_role(requester, new_role, policy,
                    users_added=users, roles_added=[])
                    if r < 0:
                        raise CommandError(\
                            'Unable to handle a role due to %s' % str(r))

                    new_roles.append(new_role)
                    total_roles = total_roles + 1
                    k = k +1
                r = mod_role(requester, role, policy,
                users_added=[], roles_added=new_roles)
                if r < 0:
                    raise CommandError('Unable to handle a role due to %s' \
                        % str(r))
            j = j + 1

        last_pos = total_roles - 1

        print '\n\n-------- TESTS --------'

        '''
            Test 1

            The time to obtain the decision corresponding to a longest tree
            path, i.e. the user has the root role and the permission needed is
            set on the last node.
        '''

        print '\n-----------------------'
        print "Test 1: time to obtain the deepest permission"
        u = UserAlias.objects.get(\
            alias=rdm_str + '_user_' + str(0),
            namespace=policy.namespace)
        object_name = \
            rdm_str + '_object_' + policy.name + '_' + str(last_pos)
        o = add_object(requester, object_name, policy)
        action_name = \
            rdm_str + '_action_' + policy.name + '_' + str(last_pos)
        a = add_action(requester, action_name, policy)
        t0 = time.time()
        p = isAuthorizedRBAC2(u, o, a)
        t1 = time.time()
        delta = t1 - t0
        if not p:
            print '### ERROR: missing permission on %s %s %s' % (u, o, a)
        print "Found in : %s" % delta
        print "Req/s : %s" % str(1/delta)
        print '-----------------------'


        '''
            Test 2

            We then test a false decision, on a user having the root role.
            Then the whole tree is tested before returning an access denied.
            Worst case denied request.
        '''

        print '\n-----------------------'
        print \
            "Test 2: time to obtain a denied request browsing the whole role \
            tree - using RBAC2"
        t0 = time.time()
        p = isAuthorizedRBAC2(u, AcsObject(name='dumb'),
            Action(name='dumb'))
        t1 = time.time()
        delta = t1 - t0
        if p:
            print '#### ERROR: found permission %s' % p
        print "Search the whole role tree in : %s" % delta
        print "Req/s : %s" % str(1/delta)
        print '-----------------------'

        print '\n-----------------------'
        print "Test 2': time to obtain a denied request browsing the whole \
            role tree - using RBAC0"
        t0 = time.time()
        p = isAuthorizedRBAC0(u, AcsObject(name='dumb'),
            Action(name='dumb'))
        t1 = time.time()
        delta = t1 - t0
        if p:
            print '#### ERROR: found permission %s' % p
        print "Search the whole role tree in : %s" % delta
        print "Req/s : %s" % str(1/delta)
        print '-----------------------'
        '''
            Note: add administration mode to select RBAC0-2
        '''


        '''
            Test 3

            Average time to obtain a positive response.
            The role tree is perfectly balanced. On each role a permission is
            given on a different object and action. Then user is given the
            root role. Then we test the response for obtaining the response
            corresponding to the permissions set on each node. With record the
            time and compute the average time to obtain a response.
        '''

        def average_time(start_pos, end_pos):
            delta = 0
            u = UserAlias.objects.get(\
                alias=rdm_str + '_user_' + str(start_pos),
                namespace=policy.namespace)
            count = 0
            for i in range(start_pos, end_pos):
                object_name = \
                    rdm_str + '_object_' + policy.name + '_' + str(i)
                o = add_object(requester, object_name, policy)
                action_name = \
                    rdm_str + '_action_' + policy.name + '_' + str(i)
                a = add_action(requester, action_name, policy)
                t0 = time.time()
                p = isAuthorizedRBAC2(u, o, a)
                t1 = time.time()
                if p:
                    d = t1 - t0
                    delta = delta + d
                    count = count + 1
                    sys.stdout.write('.')
                    sys.stdout.flush()
                '''We do not take into account denied requests'''
#                if not p:
#                    print '### ERROR: missing permission on %s %s %s' \
#                        % (u, o, a)
            return delta/count

        print '\n-----------------------'
        print "Test 3: average time to obtain a positive answer \
            (method 1 - worst case)"
        at = average_time(0, total_roles)
        print "\nAverage time is : %s" % at
        print "Req/s : %s" % str(1/at)
        print '-----------------------'


        '''
            Test 4

            The last test use a more balanced way to compute this time. Each
            role has a user. So we compute the previous time for each user.
            The first user has the highest average time because it has the
            root role and has the permission obtained through the longest role
            tree path. The last user has only the last role which is the
            smallest average time.
        '''

        print '\n-----------------------'
        print "Test 4: average time to obtain a positive answer (method 2)"
        delta = 0
        '''Too long
        for i in range(0, total_roles):
            delta = delta + average_time(i, total_roles)'''
        '''Quicker computing: cumpoute only branches and times number of
            branches'''
        total = 0
        for i in range(0, nb_sons+1):
            delta = delta + ((nb_sons**i) * average_time(total, total_roles))
            total = total + nb_sons**i
        at = delta/total_roles
        print "\nAverage time is : %s" % at
        print "Req/s : %s" % str(1/at)
        print '-----------------------'


        print '\n-------- CLEANING --------'
        for u in users:
            u.delete()
        remove_policy(policy.name, requester)

        print '\n-------- DONE --------'
