# Django settings for veridic project.

DEFAULT_CHARSET = 'utf-8'

import os
import sys

DEBUG = True
USE_DEBUG_TOOLBAR = DEBUG
TEMPLATE_DEBUG = DEBUG
STATIC_SERVE = DEBUG
_PROJECT_PATH = os.path.join(os.path.dirname(__file__))
sys.path.append(_PROJECT_PATH)

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

_DATABASE_NAME = os.path.join(_PROJECT_PATH, 'acs.db')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': _DATABASE_NAME,
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(_PROJECT_PATH, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/admin/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '$q7ac62wirb&5-0s64+of)#(d85fz3c48z#nmulwqo*nhfm64k'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'acs.urls'

TEMPLATE_DIRS = (
    os.path.join(_PROJECT_PATH, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'acs',
    'acs.abac',
    'acs.attribute_aggregator',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL='/login'
ROOT_URL='/'

# local_settings.py can be used to override environment-specific settings
# like database and email that differ between development and production.
try:
    from local_settings import *
except ImportError:
    pass

if USE_DEBUG_TOOLBAR:
    MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    INSTALLED_APPS += ('debug_toolbar',)

# 0 is no limit
ROLE_GRAPH_LIMIT = 10000
VIEW_GRAPH_LIMIT = 10000
ACTIVITY_GRAPH_LIMIT = 10000

import logging
# Logging settings
LOG_FILENAME = os.path.join(_PROJECT_PATH, 'log.log')
LOG_FILE_LEVEL = logging.DEBUG
LOG_SYSLOG = True
LOG_SYS_LEVEL = logging.WARNING

from logging.handlers import SysLogHandler
import threading

_LOCAL = threading.local()


def getlogger():

    logger = getattr(_LOCAL, 'logger', None)
    if logger is not None:
        return logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    #Hack to disable too verbose database debug logs
    logging.getLogger('django.db.backends').setLevel(logging.ERROR)

    formatter = \
        logging.Formatter(\
        '[%(asctime)s] %(levelname)-8s %(name)s.%(message)s',
        '%Y-%m-%d %a %H:%M:%S')

    if LOG_FILENAME:
        log_handler = logging.FileHandler(LOG_FILENAME, encoding = "UTF-8")
        log_handler.setFormatter(formatter)
        log_handler.setLevel(LOG_FILE_LEVEL)
        logger.addHandler(log_handler)

    if LOG_SYSLOG:
        syslog_handler = SysLogHandler(address = '/dev/log')
        formatter = \
            logging.Formatter('acs %(levelname)-8s %(name)s.%(message)s',
            '%Y-%m-%d %a %H:%M:%S')
        syslog_handler.setFormatter(formatter)
        syslog_handler.setLevel(LOG_SYS_LEVEL)
        logger.addHandler(syslog_handler)

    setattr(_LOCAL, 'logger', logger)
    return logger

getlogger()
