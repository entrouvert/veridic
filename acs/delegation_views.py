'''
    VERIDIC - Towards a centralized access control system

    Copyright (C) 2011  Mikael Ates

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import logging

from django.contrib import messages
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.template import RequestContext

from core import get_alias_in_policy, stack_of_views_from_view, \
    stack_of_activities_from_activity, \
    add_permission_delegated_by_objects, \
    get_delegated_permissions, \
    return_all_permissions_delegatable_by_objects

from decorators import prevent_access_to_not_self_administrators, \
    check_policy_in_session

from views import return_list_any

from utils_views import get_policy_from_session, \
    get_who_from_one_post_field, \
    get_what_from_one_post_field, \
    get_how_from_one_post_field

from models import UserAlias, Role, AcsObject, Action, AcsPermission

logger = logging.getLogger('acs')

login_url = settings.LOGIN_URL
root_url = settings.ROOT_URL


'''
''''''
    List accesses that can be delegated
''''''
'''


@csrf_exempt
@check_policy_in_session
@prevent_access_to_not_self_administrators
def list_accesses(request):
    policy = get_policy_from_session(request)
    tpl_parameters = {}
    tpl_parameters['backlink'] = 'mod_policy?id=' + str(policy.id)
    list_accesses = []
    alias = get_alias_in_policy(request.user, policy)
    if not alias:
        logger.critical('list_accesses: \
            %s is a self administrator but has not alias in %s' \
                % (request.user, policy))
        messages.add_message(request, messages.ERROR, \
            _('An error happened.'))
        return HttpResponseRedirect(tpl_parameters['backlink'])
    try:
        permissions = return_all_permissions_delegatable_by_objects(alias,
            policy)
        for p in permissions:
            what_list = []
            if isinstance(p.what, AcsObject):
                what_list.append(p.what)
            else:
                views = stack_of_views_from_view(p.what)
                for v in views:
                    what_list.append(v)
                    for o in v.acs_objects.all():
                        what_list.append(o)
            how_list = []
            if isinstance(p.how, Action):
                how_list.append(p.how)
            else:
                activities = stack_of_activities_from_activity(p.how)
                for v in activities:
                    how_list.append(v)
                    for o in v.actions.all():
                        how_list.append(o)
            list_accesses.append((p, what_list, how_list))
    except Exception, err:
        logger.critical('list_accesses: Error due to %s' % err)
        messages.add_message(request, messages.ERROR, \
            _('Error due to %s') % err)
        return HttpResponseRedirect(tpl_parameters['backlink'])
    tpl_parameters['alias'] = alias
    tpl_parameters['back_url'] = 'list_accesses'
    tpl_parameters['list_accesses'] = list_accesses
    tpl_parameters['who_to_display'] = \
        list(UserAlias.objects.filter(namespace=policy.namespace)) + \
        list(Role.objects.filter(namespace=policy.namespace))
    return render_to_response('list_accesses_for_delegation.html',
           tpl_parameters,
           context_instance=RequestContext(request))


@csrf_exempt
@check_policy_in_session
@prevent_access_to_not_self_administrators
def delegate_access(request):
    if request.method == 'POST':
        if not 'who_matches' in request.POST \
                or not request.POST['who_matches'] \
                or not 'what_matches' in request.POST \
                or not request.POST['what_matches'] \
                or not 'how_matches' in request.POST \
                or not request.POST['how_matches'] \
                or not 'alias_id' in request.POST \
                or not request.POST['alias_id'] \
                or not 'permission_id' in request.POST \
                or not request.POST['permission_id']:
            messages.add_message(request, messages.ERROR, \
                _('Missing argument'))
            return HttpResponseRedirect('list_accesses')

        who = None
        what = None
        how = None
        alias = None
        permission = None
        try:
            who = get_who_from_one_post_field(request, 'who_matches')
            what = get_what_from_one_post_field(request, 'what_matches')
            how = get_how_from_one_post_field(request, 'how_matches')
            alias = UserAlias.objects.get(id=request.POST['alias_id'])
            permission = \
                AcsPermission.objects.get(id=request.POST['permission_id'])
        except Exception, err:
            logger.error('delegate_access: \
                Fail to find an object with error %s' % err)
            messages.add_message(request, messages.ERROR,
                _('Fail to find an object due to %s') % err)
            return HttpResponseRedirect('list_accesses')

        delegable = False
        if 'delegable' in request.POST:
            delegable = True

        policy = get_policy_from_session(request)

        try:
            p = add_permission_delegated_by_objects(policy, alias, who,
                permission, what, how, delegable=delegable)
            logger.debug('delegate_access: \
                permission %s added' % p)
            messages.add_message(request, messages.INFO,
                _('Permission %s successfully added') % p)
        except Exception, err:
            logger.error('delegate_access: \
                Fail to add permission due to %s' % err)
            messages.add_message(request, messages.ERROR,
                _('Fail to add permission due to %s') % err)

        return HttpResponseRedirect('list_accesses')

    messages.add_message(request, messages.ERROR,
        _('Unknown request method'))
    return HttpResponseRedirect('list_accesses')


'''
''''''
    List accesses delegated
''''''
'''


@csrf_exempt
@check_policy_in_session
@prevent_access_to_not_self_administrators
def list_delegated_permissions(request):
    policy = get_policy_from_session(request)
    alias = get_alias_in_policy(request.user, policy)
    list_any = get_delegated_permissions(alias)
    title = _('Delete a delegated permission')
    type_entity = 'permission'
    return return_list_any(request, list_any, title, type_entity,
        template_name='list_permissions.html',
        backlink='mod_policy?id=' + str(policy.id),
        back_url='list_delegated_permissions')
