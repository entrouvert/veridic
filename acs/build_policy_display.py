from models import UserAlias, Role, AcsObject, View, Action, Activity, \
    AcsPermission

from core import permission_in_namespace


def build_policy_display(policy):
    '''
    Function called by a policy root administrator, display the whole policy
    '''

    '''Use to register all the nodes and edges of the graph'''
    gg = []

    '''Use to color the nodes of the graph'''
    nodes_users = []
    nodes_roles = []
    nodes_acs_objects = []
    nodes_views = []
    nodes_activities = []
    nodes_actions = []
    nodes_permissions = []

    '''Use to color the edges of the graph'''
    edges_perm_who = []
    edges_perm_what = []
    edges_perm_how = []
    edges_belonging = []
    '''View, Role and Activity inclusion of entity of a same type'''
    edges_inheritance = []

    roles = Role.objects.filter(namespace=policy.namespace)
    for role in roles:
        if not role.name in nodes_roles:
            nodes_roles.append(role.name)
            gg.append((role.name, "", 0))
    users = UserAlias.objects.filter(namespace=policy.namespace)
    for it in users:
        if not it.alias in nodes_users:
            nodes_users.append(it.alias)
            gg.append((it.alias, "", 0))
    for role in roles:
        for it in role.roles.all():
            edges_inheritance.append((role.name, it.name, 0))
            gg.append((role.name, it.name, 0))
        for it in role.users.all():
            edges_belonging.append((role.name, it.alias, 0))
            gg.append((role.name, it.alias, 0))

    activities = Activity.objects.filter(namespace=policy.namespace)
    for activity in activities:
        if not activity.name in nodes_activities:
            nodes_activities.append(activity.name)
            gg.append((activity.name, "", 0))
    actions = Action.objects.filter(namespace=policy.namespace)
    for action in actions:
        if not action.name in nodes_actions:
            nodes_actions.append(action.name)
            gg.append((action.name, "", 0))
    for activity in activities:
        for it in activity.activities.all():
            edges_inheritance.append((activity.name, it.name, 0))
            gg.append((activity.name, it.name, 0))
        actions = activity.actions.all()
        for it in actions:
            edges_belonging.append((activity.name, it.name, 0))
            gg.append((activity.name, it.name, 0))

    views = View.objects.filter(namespace=policy.namespace)
    for view in views:
        if not view.name in nodes_views:
            nodes_views.append(view.name)
            gg.append((view.name, "", 0))
    acs_objects = AcsObject.objects.filter(namespace=policy.namespace)
    for acs_object in acs_objects:
        if not acs_object.name in nodes_acs_objects:
            nodes_acs_objects.append(acs_object.name)
            gg.append((acs_object.name, "", 0))
    for view in views:
        for it in view.views.all():
            edges_inheritance.append((view.name, it.name, 0))
            gg.append((view.name, it.name, 0))
        acs_objects = view.acs_objects.all()
        for it in acs_objects:
            edges_belonging.append((view.name, it.name, 0))
            gg.append((view.name, it.name, 0))
        users = view.users.all()
        for it in users:
            edges_belonging.append((view.name, it.username, 0))
            gg.append((view.name, it.username, 0))
        roles = view.roles.all()
        for it in roles:
            edges_belonging.append((view.name, it.name, 0))
            gg.append((view.name, it.name, 0))
        actions = view.actions.all()
        for it in actions:
            edges_belonging.append((view.name, it.name, 0))
            gg.append((view.name, it.name, 0))
        activities = view.activities.all()
        for it in activities:
            edges_belonging.append((view.name, it.name, 0))
            gg.append((view.name, it.name, 0))

    permissions = AcsPermission.objects.all()
    for permission in permissions:
        if permission_in_namespace(permission, policy.namespace):
            nodes_permissions.append('P' + str(permission.id))
            gg.append(('P' + str(permission.id), "", 0))
            if isinstance(permission.who, UserAlias):
                edges_perm_who.append(('P' + str(permission.id),
                    permission.who.alias, 0))
                gg.append(('P' + str(permission.id), permission.who.alias, 0))
            else:
                edges_perm_who.append(('P' + str(permission.id),
                    permission.who.name, 0))
                gg.append(('P' + str(permission.id), permission.who.name, 0))
            edges_perm_what.append(('P' + str(permission.id),
                permission.what.name, 0))
            gg.append(('P' + str(permission.id), permission.what.name, 0))
            edges_perm_how.append(('P' + str(permission.id),
                permission.how.name, 0))
            gg.append(('P' + str(permission.id), permission.how.name, 0))

    return (gg,
    nodes_users,
    nodes_roles,
    nodes_acs_objects,
    nodes_views,
    nodes_activities,
    nodes_actions,
    nodes_permissions,
    edges_perm_who,
    edges_perm_what,
    edges_perm_how,
    edges_belonging,
    edges_inheritance)


def draw_graph(request, path,
        type_graph='whole_policy', policy=None, display='neato'):
    '''
    Available displays: twopi, fdp, dot, circo, neato
    '''

    try:
        import networkx as nx
        import matplotlib.pyplot as plt
    except ImportError:
        raise ImportError(_('Networkx and Matplotlib are required'))

    graph = None
    if type_graph == 'whole_policy':
        graph = build_policy_display(policy)
    if not graph or len(graph) != 13:
        return False

    G = nx.DiGraph()
    G.add_weighted_edges_from(graph[0])

    '''Size'''
    plt.figure(1, figsize=(9, 9))

    pos = nx.graphviz_layout(G, prog=display)

    '''Color graph'''
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[2], node_color='#1647b5')
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[1], node_color='#2c87d4')
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[4], node_color='#32ab46')
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[3], node_color='#2cd47b')
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[6], node_color='#ab9032')
    nx.draw_networkx_nodes(G, pos, node_size=200,
        nodelist=graph[5], node_color='#bdbc46')
    nx.draw_networkx_nodes(G, pos, node_size=250,
        nodelist=graph[7], node_color='#db533c')

    '''
    For a better display of arrows
    https://networkx.lanl.gov/trac/ticket/78
    '''
    nx.draw_networkx_edges(G, pos, edgelist=graph[8],
        alpha=0.8, width=1.5, edge_color='b')
    nx.draw_networkx_edges(G, pos, edgelist=graph[9],
        alpha=0.8, width=1.5, edge_color='g')
    nx.draw_networkx_edges(G, pos, edgelist=graph[10],
        alpha=0.8, width=1.5, edge_color='y')
    nx.draw_networkx_edges(G, pos, edgelist=graph[11], alpha=0.7)
    nx.draw_networkx_edges(G, pos, edgelist=graph[12], width=1)

    nx.draw_networkx_labels(G, pos, font_size=10, font_weight='bold')

    '''Save graph'''
    plt.axis('off')
    plt.savefig(path, dpi=150)

    return True

'''def draw_graph(request, path, display='neato'):
    logger.info('draw_graph: Beginning...')
    if request.user.is_anonymous():
        logger.info('draw_graph: No anonymous users')
        return redirect('/login')

    administration = Action.objects.get(name='administration')

    # Graph material

    try:
        from networkx import graphviz_layout
    except ImportError:
        raise ImportError(_('Graphviz and either PyGraphviz or Pydot are required'))

    # Create graph
    G=nx.DiGraph()
    gg = []

    # Lists to draw the graph with custom colors, etc.
    user_node = []
    nodes_users = []
    nodes_roles = []
    nodes_acs_objects = []
    nodes_views = []
    nodes_activities = []
    nodes_actions = []
    nodes_permissions = []
    nodes_permissions_administration = []

    edges_perm_who = []
    edges_perm_what = []
    edges_perm_how = []

    edges_belonging = []
    edges_inheritance = []

    view_sys = get_system_view(request.user)

    # Register Nodes and edges
    # Who
    roles = Role.objects.all()
    ''''''We display the roles the user can administrate and its own roles''''''
    for role in roles:
        if not role.name in nodes_roles and (isAuthorizedRBAC2(request.user, role, administration) or role in stack_of_roles_from_user(request.user)):
            nodes_roles.append(role.name)
            gg.append((role.name, "", 0))
    users = User.objects.all()
    for it in users:
        if not it.username in nodes_users and isAuthorizedRBAC2(request.user, it, administration) and it.id != request.user.id:
            nodes_users.append(it.username)
            gg.append((it.username, "", 0))
        elif it.id == request.user.id:
            user_node.append(request.user.username)
            gg.append((request.user.username, "", 0))
    for role in roles:
        if isAuthorizedRBAC2(request.user, role, administration) or role in stack_of_roles_from_user(request.user):
            for it in role.roles.all():
                if (isAuthorizedRBAC2(request.user, it, administration) or role in stack_of_roles_from_user(request.user)):
                    edges_inheritance.append((role.name, it.name,0))
                    gg.append((role.name, it.name,0))
            users = role.users.all()
            for it in users:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((role.name, it.username,0))
                    gg.append((role.name, it.username, 0))
    # How
    activities = Activity.objects.all()
    for activity in activities:
        if not activity.name in nodes_activities and isAuthorizedRBAC2(request.user, activity, administration):
            nodes_activities.append(activity.name)
            gg.append((activity.name, "", 0))
    actions = Action.objects.all()
    for action in actions:
        if not action.name in nodes_actions and \
                isAuthorizedRBAC2(request.user, action, administration):
            nodes_actions.append(action.name)
            gg.append((action.name, "", 0))
    for activity in activities:
        if isAuthorizedRBAC2(request.user, activity, administration):
            for it in activity.activities.all():
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_inheritance.append((activity.name, it.name,0))
                    gg.append((activity.name, it.name,0))
            actions = activity.actions.all()
            for it in actions:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((activity.name, it.name,0))
                    gg.append((activity.name, it.name,0))
    # What
    views = View.objects.all()
    for view in views:
        if not view.name in nodes_views and isAuthorizedRBAC2(request.user, view, administration) and view.id != view_sys.id:
            nodes_views.append(view.name)
            gg.append((view.name, "", 0))
    acs_objects = AcsObject.objects.all()
    for acs_object in acs_objects:
        if not acs_object.name in nodes_acs_objects and isAuthorizedRBAC2(request.user, acs_object, administration):
            nodes_acs_objects.append(acs_object.name)
            gg.append((acs_object.name, "", 0))
    for view in views:
        if isAuthorizedRBAC2(request.user, view, administration) and view.id != view_sys.id:
            for it in view.views.all():
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_inheritance.append((view.name, it.name,0))
                    gg.append((view.name, it.name,0))
            acs_objects = view.acs_objects.all()
            for it in acs_objects:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((view.name, it.name,0))
                    gg.append((view.name, it.name,0))
            users = view.users.all()
            for it in users:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((view.name, it.username,0))
                    gg.append((view.name, it.username,0))
            roles = view.roles.all()
            for it in roles:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((view.name, it.name,0))
                    gg.append((view.name, it.name,0))
            actions = view.actions.all()
            for it in actions:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((view.name, it.name,0))
                    gg.append((view.name, it.name,0))
            activities = view.activities.all()
            for it in activities:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append((view.name, it.name,0))
                    gg.append((view.name, it.name,0))
        elif isAuthorizedRBAC2(request.user, view, administration) and view.id == view_sys.id:
            nodes_permissions_administration.append("System View")
            gg.append(("System View", "", 0))
            for it in view.views.all():
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_inheritance.append(("System View", it.name,0))
                    gg.append(("System View", it.name,0))
            acs_objects = view.acs_objects.all()
            for it in acs_objects:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append(("System View", it.name,0))
                    gg.append(("System View", it.name,0))
            users = view.users.all()
            for it in users:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append(("System View", it.username,0))
                    gg.append(("System View", it.username,0))
            roles = view.roles.all()
            for it in roles:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append(("System View", it.name,0))
                    gg.append(("System View", it.name,0))
            actions = view.actions.all()
            for it in actions:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append(("System View", it.name,0))
                    gg.append(("System View", it.name,0))
            activities = view.activities.all()
            for it in activities:
                if isAuthorizedRBAC2(request.user, it, administration):
                    edges_belonging.append(("System View", it.name,0))
                    gg.append(("System View", it.name,0))

    # Permissions
    permissions = AcsPermission.objects.all()
    for permission in permissions:
        'The permissions the user can administrate, i.e. administration right on wwh
        Or the permissions given to the user or to one of its role'
        if (is_authorized_on_permission(set_default_alias(request.user), permission) or \
           (isinstance(permission.who, User) and permission.who.id == request.user.id) or \
           (isinstance(permission.who, Role) and permission.who in stack_of_roles_from_user(request.user))) \
           and not (isinstance(permission.what, View) and permission.what.id == view_sys.id):
            if permission.how.name == 'administration':
                nodes_permissions_administration.append(permission.id)
            else:
                nodes_permissions.append(permission.id)
            gg.append((permission.id, "", 0))
            if isinstance(permission.who, User):
                edges_perm_who.append((permission.id, permission.who.username, 0))
                gg.append((permission.id, permission.who.username, 0))
            else:
                edges_perm_who.append((permission.id, permission.who.name, 0))
                gg.append((permission.id, permission.who.name, 0))
            if isinstance(permission.what, User):
                edges_perm_what.append((permission.id, permission.what.username, 0))
                gg.append((permission.id, permission.what.username, 0))
            else:
                edges_perm_who.append((permission.id, permission.what.name, 0))
                gg.append((permission.id, permission.what.name, 0))
            if permission.how.name != 'administration':
                edges_perm_how.append((permission.id, permission.how.name, 0))
                gg.append((permission.id, permission.how.name, 0))

    G.add_weighted_edges_from(gg)

    # Choose size
    plt.figure(1,figsize=(9,9))

    # Choose display
    #pos=nx.graphviz_layout(G,prog='twopi')
    #pos=nx.graphviz_layout(G,prog='fdp')
    #pos=nx.graphviz_layout(G,prog='dot')
    #pos=nx.graphviz_layout(G,prog='circo')
    pos=nx.graphviz_layout(G,prog=display)

    # Draw graph
    nx.draw_networkx_nodes(G,pos,node_size=500,nodelist=user_node,node_color='#1647b5')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_roles,node_color='#1647b5')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_users,node_color='#2c87d4')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_views,node_color='#32ab46')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_acs_objects,node_color='#2cd47b')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_activities,node_color='#ab9032')
    nx.draw_networkx_nodes(G,pos,node_size=200,nodelist=nodes_actions,node_color='#bdbc46')
    nx.draw_networkx_nodes(G,pos,node_size=250,nodelist=nodes_permissions,node_color='#db533c')
    nx.draw_networkx_nodes(G,pos,node_size=250,nodelist=nodes_permissions_administration,node_color='#ffffff')
    # XXX: Modify to work with the hack for a better display of arrows
    # https://networkx.lanl.gov/trac/ticket/78
    nx.draw_networkx_edges(G,pos,edgelist=edges_perm_who,alpha=0.8,width=1.5,edge_color='b')
    nx.draw_networkx_edges(G,pos,edgelist=edges_perm_what,alpha=0.8,width=1.5,edge_color='g')
    nx.draw_networkx_edges(G,pos,edgelist=edges_perm_how,alpha=0.8,width=1.5,edge_color='y')
    nx.draw_networkx_edges(G,pos,edgelist=edges_inheritance,width=1)
    nx.draw_networkx_edges(G,pos,edgelist=edges_belonging,alpha=0.7)
    nx.draw_networkx_labels(G, pos, font_size=10, font_weight='bold')

    # Save graph
    plt.axis('off')
    plt.savefig(path,dpi=150)
'''
